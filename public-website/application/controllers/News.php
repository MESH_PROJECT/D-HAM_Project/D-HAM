<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index() {

        $this->load->model("post_model");
        $this->load->model("tags_model");
        $this->load->model("user_model");

        //get last 5 posts
        $posts = $this->post_model->get_last_five_posts();

        foreach($posts as $key => $post) {
            $uid = $post['uid'];
            $posts[$key]['title'] = htmlspecialchars_decode($posts[$key]['title'], ENT_QUOTES);
            $posts[$key]['content'] = htmlspecialchars_decode($posts[$key]['content'], ENT_QUOTES);
            $posts[$key]['user'] = $this->user_model->get($uid);
        }

        $data['posts'] = $posts;

        //TODO later on just display the popular tags
        $data['tags'] = $this->tags_model->get_all();

        if($this->session->userdata("logged_in")) {
            $data['current_user'] = $this->user_model->get($this->session->userdata("uid"));
        }

        $this->load->view("news/news", $data);
    }

    function filter($tid) {
        $this->load->model("post_model");
        $this->load->model("user_model");
        $this->load->model("post_tags_model");
        $this->load->model("tags_model");

        $tagged_posts = $this->post_tags_model->order_by('post_tag_id', 'desc')->get_many_by("tid", $tid);

        if($tagged_posts != NULL) {
            foreach($tagged_posts as $key => $post) {
                $ret = $this->post_model->get_by("post_id", $post['pid']);
                if($ret != NULL) {
                    $tagged_posts[$key] = $ret;
                } else {
                    echo 'An error has occurred';
                    return;
                }
            }

            foreach($tagged_posts as $key => $post) {
                $uid = $post['uid'];
                $tagged_posts[$key]['title'] = htmlspecialchars_decode($tagged_posts[$key]['title'], ENT_QUOTES);
                $tagged_posts[$key]['content'] = htmlspecialchars_decode($tagged_posts[$key]['content'], ENT_QUOTES);
                $tagged_posts[$key]['user'] = $this->user_model->get($uid);
            }

            $data['posts'] = $tagged_posts;

            //TODO later on just display the popular tags
            $data['tags'] = $this->tags_model->get_all();

            $this->load->view("news/news", $data);

        } else {
            echo "search failed";
        }
    }

    function new_post() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules("title", "Title", "required|min_length[10]|max_length[255]");
        $this->form_validation->set_rules("content", "Article text", "required|min_length[75]|max_length[20000]");
        $this->form_validation->set_rules("tags", "Tags", "required");

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view("news/create_post");
        }
        else
        {
            $this->load->model("post_model");

            $data['uid'] = $this->session->userdata('uid');
            $data['title'] = $this->security->xss_clean(htmlspecialchars($this->input->post('title'), ENT_QUOTES));
            $data['content'] = $this->security->xss_clean(htmlspecialchars($this->input->post('content'), ENT_QUOTES));

            $id = $this->post_model->insert($data);

            if($id != NULL) {

                $this->load->model("tags_model");
                $this->load->model("post_tags_model");

                $tags = explode(",", $this->security->xss_clean(htmlspecialchars($this->input->post('tags'), ENT_QUOTES, 'UTF-8')));
                foreach($tags as $tag) {
                    //check to see if it is already in the database.
                    $result = $this->tags_model->get_many_by("name", array("name" => $tag));
                    if(count($result) == 0) {
                        //Not in the database insert a new row
                        $tid = $this->tags_model->insert(array('name' => $tag, 'uid' => $this->session->userdata('uid')));
                        if($tid == NULL) {
                            echo 'could not create a tag';
                            return;
                        } else {
                            //give this article this tag
                            $result = $this->post_tags_model->insert(array('tid' => $tid, 'pid' => $id));
                            if($result == NULL) {
                                echo 'could not tag this article';
                                return;
                            }
                        }
                    } else {
                        $tid = $result[0]['tid'];

                        $result = $this->post_tags_model->insert(array('tid' => $tid, 'pid' => $id));
                        if($result == NULL) {
                            echo 'could not tag this article';
                            return;
                        }
                    }
                }
                //Everything went well redirect to NEWS page
                redirect('/News', 'refresh');
            } else {
                echo "Something went wrong!";
            }
        }
    }

}

