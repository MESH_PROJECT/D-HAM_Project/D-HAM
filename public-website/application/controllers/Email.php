<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    }

    function index() {
        $this->load->model("User_model");
        $this->load->library('form_validation');

        $this->form_validation->set_rules('inputEmail', 'Email Address', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('inputName', 'Name', 'required|max_length[200]|min_length[2]');
        $this->form_validation->set_rules('inputSubject', 'Subject', 'required|max_length[200]|min_length[4]');
        $this->form_validation->set_rules('inputMessage', 'Message', 'required|max_length[500]|min_length[10]');


        if ($this->form_validation->run() == FALSE) {
	    $data = array();
            if($this->session->userdata("logged_in")) {
                $data['current_user'] = $this->User_model->get($this->session->userdata("uid"));
            }

            $this->load->view("contact", $data);
        } else {
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'meshsmarthomes@gmail.com',
                'smtp_pass' => 'shamsham'
            );

            $this->load->library('email', $config);

            $this->email->set_newline("\r\n");

            $this->email->from($this->input->post('inputEmail'), $this->input->post('inputName'));
            $this->email->to('meshsmarthomes@gmail.com');
            $this->email->subject($this->input->post('inputSubject'));
            $this->email->message($this->input->post('inputMessage'));

            if($this->email->send()) {
                $this->load->view('email_confirm');
            } else {
                show_error($this->email->print_debugger());
            }
        }
    }
}