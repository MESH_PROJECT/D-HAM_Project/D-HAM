<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    }


    function index() {
        $this->load->view("admin/logon");
    }

    public function logout() {
        if($this->session->userdata('logged_in')) {
            $this->load->library('form_validation');
            $this->session->unset_userdata('logged_in');
            $this->session->unset_userdata('uid');
            $this->session->unset_userdata('email');
            $this->session->sess_destroy();
            redirect('/Welcome', 'refresh');
        } else {
            //ERROR
        }
    }

    private function session_login($data) {
        $this->session->set_userdata('logged_in', true);
        $this->session->set_userdata('uid', $data['uid']);
        $this->session->set_userdata('email', $data['email']);
    }

    public function save_section() {

        $this->load->model("Editable_model");

        $section_id = htmlspecialchars($this->input->post('sid'), ENT_QUOTES, 'UTF-8');
        $data['heading'] = $this->security->xss_clean(htmlspecialchars($this->input->post('heading'), ENT_QUOTES, 'UTF-8'));
        $data['text'] = $this->security->xss_clean(htmlspecialchars($this->input->post('text'), ENT_QUOTES, 'UTF-8'));
        $data['last_uid'] = htmlspecialchars($this->input->post('uid'), ENT_QUOTES, 'UTF-8');

        if($this->Editable_model->update($section_id, $data) !== NULL) {
            echo json_encode("success");
        }
    }

    /**
     * Callback that checks if the credentials inputted were successful
     */
    public function login_check()
    {
        $this->load->model('User_model');
        $data['uid'] = $this->input->post("uid");
        $data['password'] = $this->input->post('password');
        $data['email'] = $this->input->post('email');

        echo json_encode($this->User_model->authenticate_user($data));

        if($this->User_model->authenticate_user($data) == true) {
            $this->session_login($data);
        } else {

        }
    }

    public function change_password() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $uid = $this->session->userdata("uid");
            $data['password'] = password_hash($this->input->post("pass"), PASSWORD_BCRYPT);

            if(!$this->User_model->update($uid, $data)) {
                echo json_encode("error");
            }

            else {
                echo json_encode("success");
            }
        }
    }

    public function add_user() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $data['fname'] = htmlspecialchars($this->input->post("fname"));
            $data['lname'] = htmlspecialchars($this->input->post("lname"));
            $data['email'] = htmlspecialchars($this->input->post("email"));
            $data['description'] = htmlspecialchars($this->input->post("description"));
            $data['position'] = htmlspecialchars($this->input->post("position"));
            $data['password'] = 'password';

            $all_users = $this->User_model->get_all();
            $data['display_order'] = count($all_users)  + 1;

            $this->User_model->insert($data);
            echo json_encode("success");
        }
    }

    public function update_bio() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $data['position'] = htmlspecialchars($this->input->post("position"));
            $data['description'] = htmlspecialchars($this->input->post("description"));

            $this->User_model->update($this->session->userdata("uid"), $data);

            echo json_encode("success");
        }
    }

    public function remove_user() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $fname = htmlspecialchars($this->input->post("fname"));
            $lname = htmlspecialchars($this->input->post("lname"));

            if($this->User_model->delete_user_by_first_and_last($fname, $lname)) {
                echo json_encode("success");
            } else {
                echo json_encode("no user");
            }

        }
    }

    public function get_user_order_modal(){
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $data['users'] = $this->User_model->get_all();
            $this->load->view("templates/user_order", $data);
        }
    }

    public function save_order_user() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if($this->session->userdata("logged_in")) {
            $this->load->model("User_model");
            $this->User_model->update($this->input->post("uid"), array('display_order' => $this->input->post("display_order") ));
            echo json_encode("success");
        }
    }

    /**
     * Upload a profile picture
     */
    public function save_new_profile_image() {

        //This must be an ajax request
        if (!$this -> input -> is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $this->load->model("User_model");
        $this->load->library("myImageUploader");

        try
        {
            $imageUploader = new ImageUploader(getenv('APP_ROOT_PATH') . "assets/img/profile_pics", "random_salt");
            $imageUploader->setSalt("3141");            // It is used while hashing image names
            $imageUploader->setMinFileSize(0);          // Set minimum file size in bytes
            $imageUploader->setMaxFileSize(4000000);     // Set maximum file size in bytes
            // An optional custom callback to process the uploaded image using GD library
            $custom_callback = function(&$image) {

                $original_w = imagesx($image);
                $original_h = imagesy($image);

                $img_w = intval($this->input->post("img-width"));
                $img_h = intval($this->input->post("img-height"));

                $scale = $original_w /$img_w;

                $crop_x = intval($this->input->post("crop-x"));
                $crop_y = intval($this->input->post("crop-y"));

                $dest = imagecreatetruecolor(256 * $scale, 256 * $scale);
                imagecopy(
                    $dest,
                    $image,
                    0,
                    0,
                    intval($this->input->post("crop-x")) * $scale,
                    intval($this->input->post("crop-y")) * $scale,
                    256 * $scale,
                    256 * $scale
                );

                $image = imagecreatetruecolor(256, 256);
                imagecopyresized($image, $dest, 0, 0, 0, 0, 256, 256, 256 * $scale, 256 * $scale);


                //---------------------PHP >= 5.5 ---------------------------------------
//                $image = imagecrop($image, array(
//                    "x" => intval($this->input->post("crop-x")) * $scale,
//                    "y" => intval($this->input->post("crop-y")) * $scale,
//                    "width" => 200 * $scale,
//                    "height" => 200 * $scale
//                ));
//                $image = imagescale($dest, 256, 256);
            };

            //REMEMBER TO CONFIGURE PERMISSIONS ON THE DIRECTORY.....!!!
            $res = $imageUploader->upload($_FILES["my_image"], $this->session->userdata('uid'), $custom_callback);

            // $w = $this->input->post("ima")


            $url = base_url() . $imageUploader->getImagePath($this->session->userdata('uid'));

            $data['profile_pic_url'] = $url;
            if($this->User_model->update($this->session->userdata('uid'), $data) != NULL) {
                echo json_encode($url);
            }
        }
        catch (Exception $e)
        {
            var_dump($e);
        }
    }

    public function user_exists() {
        //This must be an ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        //load models
        $this->load->model('User_model');

        $email = $this->input->post('email');
        $results = $this->User_model->get_by('email', $email);

        echo json_encode($results);

    }

    public function load_resizer() {
        //This must be an ajax request
        if (!$this -> input -> is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $data['img_path'] = $this->input->post("pathToImg");

        $this->load->view("templates/resizer", $data);
    }
}