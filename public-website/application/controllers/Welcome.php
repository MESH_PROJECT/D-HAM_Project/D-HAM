<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->load->model("Editable_model");
		$this->load->model("User_model");
        $data['sections']= $this->Editable_model->_order_by_primary()->get_many_by('page', 'home');
		$data['users'] = $this->User_model->get_all_order_by_position();
        if( $data['sections'] !== NULL) {

            //Clean all the data from potentially harmful stuff
            for($i = 0; $i < count($data['sections']); $i++) {
                $data['sections'][$i]['heading'] = $this->security->xss_clean($data['sections'][$i]['heading']);
                $data['sections'][$i]['text'] = $this->security->xss_clean($data['sections'][$i]['text']);
            }
			
			if($this->session->userdata("logged_in")) {
				$data['current_user'] = $this->User_model->get($this->session->userdata("uid"));
			}

            $this->load->view('home', $data);
        }
	}

	public function specifics() {
		$this->load->model("User_model");
        $this->load->model("Editable_model");
        $data['sections']= $this->Editable_model->_order_by_primary()->get_many_by('page', 'specifics');
        if( $data['sections'] !== NULL) {

            //Clean all the data from potentially harmful stuff
            for($i = 0; $i < count($data['sections']); $i++) {
                $data['sections'][$i]['heading'] = $this->security->xss_clean($data['sections'][$i]['heading']);
                $data['sections'][$i]['text'] = $this->security->xss_clean($data['sections'][$i]['text']);
            }

			if($this->session->userdata("logged_in")) {
				$data['current_user'] = $this->User_model->get($this->session->userdata("uid"));
			}

            $this->load->view('specifics', $data);
        }
	}

	public function research() {
		$this->load->view('research');
	}

	/**Get involved link**/
	public function contribute() {
		$this->load->model("User_model");
		$data = array();
		if($this->session->userdata("logged_in")) {
			$data['current_user'] = $this->User_model->get($this->session->userdata("uid"));
		}

		$this->load->view('get_involved', $data);
	}

	public function contributors(){
        $this->load->model("Editable_model");
        $this->load->model("User_model");
        $data['sections']= $this->Editable_model->_order_by_primary()->get_many_by('page', 'contributors');
        $data['users'] = $this->User_model->get_inactive();
        if( $data['sections'] !== NULL) {

            //Clean all the data from potentially harmful stuff
            for($i = 0; $i < count($data['sections']); $i++) {
                $data['sections'][$i]['heading'] = $this->security->xss_clean($data['sections'][$i]['heading']);
                $data['sections'][$i]['text'] = $this->security->xss_clean($data['sections'][$i]['text']);
            }

            if($this->session->userdata("logged_in")) {
                $data['current_user'] = $this->User_model->get($this->session->userdata("uid"));
            }

            $this->load->view('contributors', $data);
        }

    }

}
