<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model {

	protected $_table = 'user';
	protected $primary_key = 'uid';
	protected $return_type = 'array';
	
	protected $after_get = array('remove_sensitive_data');
	protected $before_create = array('prep_data');
	
	protected function remove_sensitive_data($user) {
		unset($user['password']);
		return $user;
	}
	
	protected function prep_data($user) {
		$user['password'] = password_hash($user['password'], PASSWORD_BCRYPT);	//hash plaintext password
		return $user;
	}
	
	/**
	 * Checks to see if the credentials inputted will authenticate
	 * @param $data, the array that contains both the email and password.
	 * @return boolean, true if authenticated false otherwise
	 */
	function authenticate_user($data = array()) {
		//query
		$query = $this->db->get_where('user', array('email' => $data['email']));
		if(count($query->row_array()) != 0) {
			$user_row = $query->row_array();
			$h = $user_row['password'];
			
			if(password_verify($data['password'], $h) and $user_row['active']==1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Remove any user with that matches the params. (hopefully there aren't two employees with the same first and last name)
	
	 * @param $fname, the user's first name
	 * @param $lname, the user's last name
	 * @return failure or success
	 */
	function delete_user_by_first_and_last($fname, $lname) {
		$sql  = "DELETE from user WHERE fname='$fname' AND lname='$lname'";

		$query = $this->db->query($sql);
		if($query === NULL) {
			return false;
		}
		return true;
	}

	/**
	 * Search for a user based on a set number of words
	 */
	function searchUsers($terms = array()) {

		foreach($terms as $term) {
			$this->db->like('username', $term);
			$this->db->or_where('email', $term);
		}
		$this->db->order_by("username", "desc");
		$this->db->order_by("email", "asc");
		$query = $this->db->get('user', 5, 0);

		return $query->result_array();
	}

	function get_last_user($uid) {
		return $this->db->select("*")->from("user")->order_by("date", "desc")->limit('1')
			->get()->result_array();
	}

	function get_all_order_by_position() {
		return $this->db->select("*")->from("user")->order_by("display_order")->where("active=1")
			->get()->result_array();
	}
	function get_inactive(){
        return $this->db->select("*")->from("user")->order_by("display_order")->where("active=0")
            ->get()->result_array();
    }

}
