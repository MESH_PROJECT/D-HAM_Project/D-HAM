<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends MY_Model {
    protected $_table = 'post';
    protected $primary_key = 'post_id';
    protected $return_type = 'array';

    function get_last_five_posts() {
        return $this->db->select("*")->from("post")->order_by("date", "desc")->limit('5')
            ->get()->result_array();
    }
}
