<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editable_model extends MY_Model {
    protected $_table = 'editable';
    protected $primary_key = 'section_id';
    protected $return_type = 'array';

    public function _order_by_primary() {
        $this->db->order_by('section_id', 'asc');
        return $this;
    }
}
