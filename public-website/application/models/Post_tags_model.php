<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_tags_model extends MY_Model {
    protected $_table = 'post_tags';
    protected $primary_key = 'post_tag_id';
    protected $return_type = 'array';
}
