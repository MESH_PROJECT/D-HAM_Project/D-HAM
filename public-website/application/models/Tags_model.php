<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_model extends MY_Model {
    protected $_table = 'tags';
    protected $primary_key = 'tid';
    protected $return_type = 'array';
}
