<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH | Contact</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles -->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="background-color: white;">

<!-- Fixed navbar -->
<?php include_once("templates/navbar.php"); ?>

<!-- *****************************************************************************************************************
 color WRAP
 ***************************************************************************************************************** -->
<div id="color-banner">
    <div class="container">
        <div class="row">
            <h3>Research</h3>
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /blue -->


<!-- *****************************************************************************************************************
 CONTENT
 ***************************************************************************************************************** -->

<div class="container mtb">
    <div class="row">
        <div class="alert alert-success" role="alert"><strong>Thank you for contacting us! We will get back to you
        as soon as possible.</strong></div>
    </div>
</div>

<!-- *****************************************************************************************************************
FOOTER
***************************************************************************************************************** -->
<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h4>About</h4>
                <div class="hline-w"></div>
                <p>This is a project directed by the Computer Science Department at New Mexico State University and
                    funded by the National Science Foundation. Although we are still in the early stages of development,
                    we have high hopes that the project will be successful..</p>
            </div>

            <div class="col-lg-4">
                <h4>Get Involved</h4>
                <div class="hline-w"></div>
                <p>
                    <a href="#"><i class="fa fa-github"></i></a>
                </p>
            </div>

            <div class="col-lg-4">
                <h4>Contact</h4>
                <div class="hline-w"></div>
                <p>
                    Science Hall,
                    Room #124<br/>
                    New Mexico State University<br/>
                    Las Cruces, NM 88001<br/>
                </p>
            </div>

        </div>
        <! --/row -->
    </div>
    <! --/container -->
</div>
<! --/footerwrap -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
<script src="<?php echo base_url('js/nav.js'); ?>"></script>


</body>
</html>
