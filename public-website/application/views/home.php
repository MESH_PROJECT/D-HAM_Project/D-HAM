<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles -->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- Video JS CSS -->
    <link href="https://vjs.zencdn.net/5.10.7/video-js.css" rel="stylesheet">

    <link href="<?php echo base_url('/css/jrac.css'); ?>" rel="stylesheet">
    <link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="home">

<!-- Fixed navbar -->
<?php include_once ("templates/navbar.php"); ?>

<!-- *****************************************************************************************************************
 HEADERWRAP
 ***************************************************************************************************************** -->
<div id="headerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Welcome to</h3>
            </div>
            <div class="col-sm-12 himg" style="padding-bottom: 15px;">
                <video id="video1" class="video-js vjs-default-skin vjs-big-play-centered col-sm-12" controls poster="<?php echo base_url('assets/img/logo.jpg'); ?>" style="width: 100%;" data-setup='{"example_option":true}'>
                    <source src="<?php echo base_url('assets/MESH_Movie_Final_Draft.mp4'); ?>" type="video/mp4">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
            </div>
            <div class="col-lg-8 col-lg-offset-2" style="padding-bottom: 15px;">
                <h5>An NMSU iCREDITS project</h5>
                <h5>Funded by the National Science Foundation</h5>
            </div>
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /headerwrap -->

<!-- *****************************************************************************************************************
 INFO LOGOS
 ***************************************************************************************************************** -->
<div id="service">
    <div class="container">
        <div class="row centered">
            <div class="col-md-4 logo-contain">
                <i class="info-icon fa fa-home"></i>
                <h4 data-section-heading="<?php echo $sections[0]['section_id']; ?>"><?php echo $sections[0]['heading']; ?></h4>
                <p class="reg-text" data-section="<?php echo $sections[0]['section_id']; ?>"><?php echo $sections[0]['text']; ?></p>
                <?php if($this->session->userdata("logged_in")): ?>
                    <div class="edit-contain">
                        <a href="#" class="editable" data-edit="<?php echo $sections[0]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-4 logo-contain">
                <i class="info-icon fa fa-lightbulb-o"></i>
                <h4 data-section-heading="<?php echo $sections[1]['section_id']; ?>"><?php echo $sections[1]['heading']; ?></h4>
                <p class="reg-text" data-section="<?php echo $sections[1]['section_id']; ?>"><?php echo $sections[1]['text']; ?></p>

                <?php if($this->session->userdata("logged_in")): ?>
                    <div class="edit-contain">
                        <a href="#" class="editable" data-edit="<?php echo $sections[1]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-4 logo-contain">
                <i class="info-icon fa fa-pagelines"></i>
                <h4 data-section-heading="<?php echo $sections[2]['section_id']; ?>"><?php echo $sections[2]['heading']; ?></h4>
                <p class="reg-text" data-section="<?php echo $sections[2]['section_id']; ?>"><?php echo $sections[2]['text']; ?></p>

                <?php if($this->session->userdata("logged_in")): ?>
                    <div class="edit-contain">
                        <a href="#" class="editable" data-edit="<?php echo $sections[2]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <! --/container -->
</div>
<! --/service -->

<!-- *****************************************************************************************************************
 TESTIMONIALS
 ***************************************************************************************************************** -->
<div id="twrap">
    <div class="container centered">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <i class="fa fa-comment-o"></i>
                <p>[T]he Internet will disappear. There will be so many IP addresses, so many devices, sensors, things that you are wearing, things that you are interacting with, that you won't even sense it. It will be part of your presence all the time. Imagine you walk into a room, and the room is dynamic. And with your permission and all of that, you are interacting with the things going on in the room.</p>
                <h4><br/>Eric Schmidt</h4>
                <p>Former CEO of Google</p>
            </div>
        </div><! --/row -->
    </div><! --/container -->
</div><! --/twrap -->

<!-- *****************************************************************************************************************
 TEAM SECTION
 ***************************************************************************************************************** -->
<div class="container mtb">
    <h3 class="text-center" style="padding-bottom: 15px;">MEET OUR TEAM</h3>

    <?php for($i = 0; $i < count($users); $i++): ?>
        <?php if($i % 4 == 0): ?>
        <div class="row centered">
        <?php endif; ?>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="he-wrap tpl6">
                    <img src="<?php echo $users[$i]['profile_pic_url']; ?>" alt="" style="width: 256px; height: 256px;">
                    <div class="he-view">
                        <div class="bg a0" data-animate="fadeIn">
                            <div data-user="<?php echo $users[$i]['uid']; ?>" ><?php echo $users[$i]['description']; ?></div>
                        </div><!-- he bg -->
                    </div><!-- he view -->
                </div><!-- he wrap -->
                <h4 data-user="<?php echo $users[$i]['uid']; ?>"><?php echo $users[$i]['fname'] . ' ' . $users[$i]['lname']; ?></h4>
                <h5 class="ctitle"><?php echo $users[$i]['position']; ?></h5>
                <?php if($this->session->userdata("logged_in")): ?>
<!--                    <div class="edit-contain">-->
<!--                        <a href="#" class="editable" data-edit="--><?php //echo $sections[3]['section_id']; ?><!--"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>-->
<!--                    </div>-->
                <?php endif; ?>
            </div>
            <! --/col-lg-3 -->
        <?php if($i % 4 == 3): ?>
        </div>
        <?php endif; ?>
    <?php endfor; ?>
</div>
</div><! --/container -->

<!-- *****************************************************************************************************************
 OUR SPONSORS
 ***************************************************************************************************************** -->
<div id="cwrap">
    <div class="container">
        <div class="row centered">
            <h3 style="color: black;">OUR SPONSORS</h3>

            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="https://www.nmsu.edu/">
                    <img src="<?php echo base_url('assets/img/sponsors/nmsu_logo.png'); ?>" class="img-responsive">
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="https://icredits.nmsu.edu/">
                    <img src="<?php echo base_url('assets/img/sponsors/crest_logo.png'); ?>" class="img-responsive">
                </a>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=1345232">
                    <img src="<?php echo base_url('assets/img/sponsors/nsf1.png'); ?>" class="img-responsive">
                </a>
            </div>
        </div>
        <! --/row -->
    </div>
    <! --/container -->
</div>
<! --/cwrap -->

<!-- *****************************************************************************************************************
 FOOTER
 ***************************************************************************************************************** -->
<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4>About</h4>
                <div class="hline-w"></div>
                <p>This is a project conducted within the iCREDITS smart grid research center at New Mexico State University. It is funded by the National Science Foundation. Although we are still in the early stages of development, we have high hopes that the project will be successful!</p>
            </div>

            <div class="col-md-4">
                <h4>Get Involved</h4>
                <div class="hline-w"></div>
                <p>
                    Are you interested in Home Automation? Want to learn more about the Internet of Things? Or are you looking for some practical experience with real systems? We are always looking for enthusiastic contributors. <a href="<?php echo base_url(); ?>index.php/Welcome/contribute/">Find out how</a>!
                </p>
            </div>

            <div class="col-md-4">
                <h4>Contact</h4>
                <div class="hline-w"></div>
                <p>
                    Science Hall,
                    Room #134<br/>
                    New Mexico State University<br/>
                    Las Cruces, NM 88001<br/>
                </p>
            </div>

        </div>
        <! --/row -->
    </div>
    <! --/container -->
</div>
<! --/footerwrap -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
    var Constants = {
        base_url: '<?= base_url() ?>'
        <?php if($this->session->userdata('logged_in')): ?>
        , uid: <?php echo $this->session->userdata('uid'); ?>
        <?php endif; ?>
    };
</script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="https://vjs.zencdn.net/5.10.7/video.js"></script>
<script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>



<script type="text/javascript" src="<?php echo base_url('js/home.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>
<script src="<?php echo base_url('js/nav.js'); ?>"></script>


<script>
    // Portfolio
    (function ($) {
        "use strict";
        var $container = $('.portfolio'),
                $items = $container.find('.portfolio-item'),
                portfolioLayout = 'fitRows';

        if ($container.hasClass('portfolio-centered')) {
            portfolioLayout = 'masonry';
        }

        $container.isotope({
            filter: '*',
            animationEngine: 'best-available',
            layoutMode: portfolioLayout,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            },
            masonry: {}
        }, refreshWaypoints());

        function refreshWaypoints() {
            setTimeout(function () {
            }, 1000);
        }

        $('nav.portfolio-filter ul a').on('click', function () {
            var selector = $(this).attr('data-filter');
            $container.isotope({filter: selector}, refreshWaypoints());
            $('nav.portfolio-filter ul a').removeClass('active');
            $(this).addClass('active');
            return false;
        });

        function getColumnNumber() {
            var winWidth = $(window).width(),
                    columnNumber = 1;

            if (winWidth > 1200) {
                columnNumber = 5;
            } else if (winWidth > 950) {
                columnNumber = 4;
            } else if (winWidth > 600) {
                columnNumber = 3;
            } else if (winWidth > 400) {
                columnNumber = 2;
            } else if (winWidth > 250) {
                columnNumber = 1;
            }
            return columnNumber;
        }

        function setColumns() {
            var winWidth = $(window).width(),
                    columnNumber = getColumnNumber(),
                    itemWidth = Math.floor(winWidth / columnNumber);

            $container.find('.portfolio-item').each(function () {
                $(this).css({
                    width: itemWidth + 'px'
                });
            });
        }

        function setPortfolio() {
            setColumns();
            $container.isotope('reLayout');
        }

        $container.imagesLoaded(function () {
            setPortfolio();
        });

        $(window).on('resize', function () {
            setPortfolio();
        });
    })(jQuery);
</script>
</body>
</html>
