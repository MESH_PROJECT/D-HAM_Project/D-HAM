<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH | Specifics</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

      <link href="<?php echo base_url('/css/jrac.css'); ?>" rel="stylesheet">
      <link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body id="about-section">

  <!-- Fixed navbar -->
  <?php include_once ("templates/navbar.php"); ?>

	<!-- *****************************************************************************************************************
	 COLOR WRAP
	 ***************************************************************************************************************** -->
	<div id="color-banner">
	    <div class="container">
			<div class="row">
				<h3>Specifics</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /color -->

	 
	<!-- *****************************************************************************************************************
	 SPECIFICS
	 ***************************************************************************************************************** -->
		<div class="container mtb">
			<div class="row">
				<div id="left-interactive-content" class="col-sm-6">
                    <img id="load-img" class="img-responsive" src="<?php echo base_url('assets/img/raspberry_pi.jpg'); ?>"/>

				</div>
				<div class="col-sm-6">
					<h3 style="color: black;" data-section-heading="<?php echo $sections[0]['section_id']; ?>">Under the Hood</h3>
                    <p style="color: black;" data-section="<?php echo $sections[0]['section_id']; ?>">
                        <?php echo $sections[0]['text']; ?>
                    </p>

                    <?php if($this->session->userdata("logged_in")): ?>
                        <div class="edit-contain" style="padding-bottom: 15px; margin: 0px; padding-top: 0px;">
                            <a href="#" class="editable" data-edit="<?php echo $sections[0]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                        </div>
                    <?php endif; ?>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-section-heading="<?php echo $sections[1]['section_id']; ?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <?php echo $sections[1]['heading']; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body" style="color: black;">
                                    <p data-section="<?php echo $sections[1]['section_id']; ?>" style="color: black;">
                                        <?php  echo $sections[1]['text']; ?>
                                    </p>
                                </div>
                                <?php if($this->session->userdata("logged_in")): ?>
                                    <div class="edit-contain pull-right" style="margin: 0; padding: 0;">
                                        <a href="#" class="editable" data-edit="<?php echo $sections[1]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a data-section-heading="<?php echo $sections[2]['section_id']; ?>" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <?php echo $sections[2]['heading']; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <p data-section="<?php echo $sections[2]['section_id']; ?>" style="color: black;">
                                        <?php  echo $sections[2]['text']; ?>
                                    </p>
                                </div>
                                <?php if($this->session->userdata("logged_in")): ?>
                                    <div class="edit-contain pull-right" style="margin: 0; padding: 0;">
                                        <a href="#" class="editable" data-edit="<?php echo $sections[2]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a data-section-heading="<?php echo $sections[3]['section_id']; ?>" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <?php echo $sections[3]['heading']; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p data-section="<?php echo $sections[3]['section_id']; ?>" style="color: black;">
                                        <?php  echo $sections[3]['text']; ?>
                                    </p>
                                </div>
                                <?php if($this->session->userdata("logged_in")): ?>
                                    <div class="edit-contain pull-right" style="margin: 0; padding: 0;">
                                        <a href="#" class="editable" data-edit="<?php echo $sections[3]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                    <a data-section-heading="<?php echo $sections[4]['section_id']; ?>" class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <?php echo $sections[4]['heading']; ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                <div class="panel-body">
                                    <p data-section="<?php echo $sections[4]['section_id']; ?>" style="color: black;">
                                        <?php  echo $sections[4]['text']; ?>
                                    </p>
                                </div>
                                <?php if($this->session->userdata("logged_in")): ?>
                                    <div class="edit-contain pull-right" style="margin: 0; padding: 0;">
                                        <a href="#" class="editable" data-edit="<?php echo $sections[4]['section_id']; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
					<p><br/><a href="<?php echo site_url("Email/"); ?>" class="btn btn-theme">Contact Us</a></p>
				</div>
			</div>
			<! --/row -->
		</div>
		<! --/container -->

	<!-- *****************************************************************************************************************
     FOOTER
     ***************************************************************************************************************** -->
	<div id="footerwrap">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4>About</h4>
					<div class="hline-w"></div>
					<p>
                        This is a project conducted within the iCREDITS smart grid research center at New Mexico State University. It is funded by the National Science Foundation. Although we are still in the early stages of development, we have high hopes that the project will be successful!</p>
				</div>

                <div class="col-md-4">
                    <h4>Get Involved</h4>
                    <div class="hline-w"></div>
                    <p>
                        Are you interested in Home Automation? Want to learn more about the Internet of Things? Or are you looking for some practical experience with real systems? We are always looking for enthusiastic contributors. <a href="<?php echo base_url(); ?>index.php/Welcome/contribute/">Find out how</a>!
                    </p>
                </div>

				<div class="col-md-4">
					<h4>Contact</h4>
					<div class="hline-w"></div>
					<p>
						Science Hall,
						Room #134<br/>
						New Mexico State University<br/>
						Las Cruces, NM 88001<br/>
					</p>
				</div>
			</div>
			<! --/row -->
		</div>
		<! --/container -->
	</div>
	<! --/footerwrap -->
	 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript">
        var Constants = {
            base_url: '<?= base_url() ?>'
        }
    </script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>

    <script src="<?php echo base_url('js/specifics.js'); ?>"></script>
    <script src="<?php echo base_url('js/nav.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>


  </body>
</html>
