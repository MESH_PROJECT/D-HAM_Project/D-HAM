
<?php if($this->session->userdata('logged_in')): ?>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: black;">Change Password</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <input id="pass1" type="password" class="form-control" placeholder="new password"/>
                            <input id="pass2" type="password" class="form-control" placeholder="verify password"/>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="change-password" data-url="<?php echo base_url(); ?>" data-user="<?php echo $this->session->userdata('uid'); ?>" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id="addUser" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: black;">Add User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">
                            <input id="fname" type="text" class="form-control" placeholder="Their first name"/>
                            <input id="lname" type="text" class="form-control" placeholder="Their last name"/>
                            <input id="position" type="text" class="form-control" placeholder="Their position"/>
                            <input id="email" type="text" class="form-control" placeholder="Their email"/>
                        </div>
                        <div class="form-group">
                            <label for="description">User description:</label>
                            <textarea id="description" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="add-user" data-url="<?php echo base_url(); ?>" data-user="<?php echo $this->session->userdata('uid'); ?>" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id="removeUser" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: black;">Remove User</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center">Will delete any users that match both the first name and last name</div>
                    <div class="form-group">
                        <div class="form-group">
                            <input id="fname-del" type="text" class="form-control" placeholder="Their first name"/>
                            <input id="lname-del" type="text" class="form-control" placeholder="Their last name"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="remove-user" data-url="<?php echo base_url(); ?>" data-user="<?php echo $this->session->userdata('uid'); ?>" type="submit" class="btn btn-primary">Remove User</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Modal to change your description and/or title -->
    <div id="modifyBio" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: black;">Change Description/Job Title</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="new-jobtitle">Job Title:</label>
                        <input id="new-jobtitle" type="text" class="form-control" value="<?php echo $current_user['position']; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="new-description">User description:</label>
                        <textarea id="new-description" class="form-control" rows="5"><?php echo $current_user['description']; ?></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button id="change-bio" data-url="<?php echo base_url(); ?>" data-user="<?php echo $this->session->userdata('uid'); ?>" type="submit" class="btn btn-primary">Update Bio</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->g -->
    </div><!-- /.modal -->

    <!-- Modal -->
    <div class="modal" id="uploadProfilePicModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Choose a new picture</h4>
                </div>
                <form method="POST" id="imageUploadForm"
                      action="<?php echo base_url() . 'index.php/Admin/save_new_profile_image'; ?>"
                      enctype="multipart/form-data">
                    <div class="modal-body">
                        <input id="ImageBrowse" type="file" name="my_image" data-url="<?php echo base_url(); ?>"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button id="upload-profile-pic" type="submit" class="btn btn-warning">Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endif; ?>


<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() . 'index.php/Welcome/'; ?>">MESH</a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li id="home-menu"><a href="<?php echo base_url() . 'index.php/Welcome/'; ?>">HOME</a></li>
                <li id="specifics-menu"><a href="<?php echo base_url() . 'index.php/Welcome/specifics/'; ?>">SPECIFICS</a></li>
                <li id="contact-menu"><a href="<?php echo base_url() . 'index.php/Email/'; ?>">CONTACT</a></li>
                <li id="news-menu"><a href="<?php echo base_url() . 'index.php/News/'; ?>">NEWS</a></li>
                <?php if($this->session->userdata('logged_in')): ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">ADMIN <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Change password</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#uploadProfilePicModal">Change your picture</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#modifyBio">Change your description/jobtitle</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#addUser">Add User</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#removeUser">Remove User</a></li>
                        <li><a id="changeUserOrder" href="#" data-url="<?php echo base_url(); ?>">Change user display order</a></li>
                        <li><a href="<?php echo base_url(). 'index.php/Admin/logout'; ?>">Logout</a></li>
                    </ul>
                </li>
                <?php endif; ?>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>