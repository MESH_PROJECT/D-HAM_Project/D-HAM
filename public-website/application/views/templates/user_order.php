<div id="orderModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color: black;">User display order</h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>user id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>display order (lower = higher in page)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users as $user): ?>
                        <tr>

                            <th scope="row"><?php echo $user['uid']; ?></th>
                            <td><?php echo $user['fname']; ?></td>
                            <td><?php echo $user['lname']; ?></td>
                            <td>
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control" placeholder="<?php echo $user['display_order']; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary save-order" data-url="<?php echo base_url(); ?>" data-user="<?php echo $user['uid']; ?>"
                                                                        type="button">Save</button>
                                    </span>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->