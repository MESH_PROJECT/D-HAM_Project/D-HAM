<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH | News</title>

	  <!-- Bootstrap core CSS -->
	  <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

	  <!-- Custom styles -->
	  <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
	  <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

      <link href="<?php echo base_url('/css/jrac.css'); ?>" rel="stylesheet">
      <link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

      <style>
          h1, h2, h3, h4, h5, h6 {
              color: black;
          }
      </style>
  </head>

  <body style="background-color: white; color: black;">

  <!-- Fixed navbar -->
  <?php include_once ("/home/http/mesh/application/views/templates/navbar.php"); ?>

  <!-- *****************************************************************************************************************
   COLOR WRAP
   ***************************************************************************************************************** -->
  <div id="color-banner">
	  <div class="container">
		  <div class="row">
			  <h3 style="color: white;">News</h3>
		  </div><!-- /row -->
	  </div> <!-- /container -->
  </div><!-- /color -->

	 
	<!-- *****************************************************************************************************************
	 BLOG CONTENT
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
		 <?php if($this->session->userdata("logged_in")): ?>
		 <div class="pull-right" style="padding-bottom: 20px;">
			 <a href="<?php echo base_url() . 'index.php/News/new_post/'; ?>" class="btn btn-default btn-lg">Create a Post</a>
		 </div>
		 <?php endif; ?>
	 	<div class="row">
	 	
	 		<! -- BLOG POSTS LIST -->
	 		<div class="col-sm-8" style="min-height: 50vh;">

                <?php if(count($posts) === 0): ?>
                    <h3 class="ctitle">There are no articles</h3>
                <?php endif; ?>

                <?php foreach($posts as $post): ?>
                    <div class="post-container" style="color: black;">
                        <! -- Blog Post 1 -->
                        <a href="single-post.html"><h3 class="ctitle"><?php echo $post['title']; ?></h3></a>
                        <p><csmall>Posted: <?php echo $post['date']; ?>.</csmall> | <csmall2>By: <?php echo $post['user']['fname']; ?> <?php echo $post['user']['lname']; ?></csmall2></p>
                        <div class="post-content" style="overflow: hidden;"><?php echo $post['content']; ?></div>
                        <div class="hline"></div>
                    </div>
                <?php endforeach; ?>
		 		
			</div><! --/col-lg-8 -->
	 		
	 		
	 		<! -- SIDEBAR -->
	 		<div class="col-sm-4">
		 		<h4 style="color: black;">Search</h4>
		 		<div class="hline"></div>
		 			<p>
		 				<br/>
		 				<input type="text" class="form-control" placeholder="Search something">
		 			</p>
		            
		 		<div class="spacing"></div>
		 		
		 		<h4 style="color: black;">Popular Tags</h4>
		 		<div class="hline"></div>
		 			<p>
                        <?php if(count($tags) === 0) { echo "There are no tags"; } ?>
                        <?php foreach($tags as $tag): ?>
		            	    <a class="btn btn-theme" href="<?php echo base_url(). 'index.php/News/filter/' . $tag['tid']; ?>" role="button">
                                <?php echo $tag['name']; ?>
                            </a>
                        <?php endforeach; ?>
		 			</p>
	 		</div>
	 	</div><! --/row -->
	 </div><! --/container -->


  <!-- *****************************************************************************************************************
   FOOTER
   ***************************************************************************************************************** -->
  <div id="footerwrap">
      <div class="container">
          <div class="row">
              <div class="col-md-4">
                  <h4>About</h4>
                  <div class="hline-w"></div>
                  <p>
                      This is a project conducted within the iCREDITS smart grid research center at New Mexico State University. It is funded by the National Science Foundation. Although we are still in the early stages of development, we have high hopes that the project will be successful!</p>
              </div>

              <div class="col-md-4">
                  <h4>Get Involved</h4>
                  <div class="hline-w"></div>
                  <p>
                      Are you interested in Home Automation? Want to learn more about the Internet of Things? Or are you looking for some practical experience with real systems? We are always looking for enthusiastic contributors. <a href="<?php echo base_url(); ?>index.php/Welcome/contribute/">Find out how</a>!
                  </p>
              </div>

              <div class="col-md-4">
                  <h4>Contact</h4>
                  <div class="hline-w"></div>
                  <p>
                      Science Hall,
                      Room #134<br/>
                      New Mexico State University<br/>
                      Las Cruces, NM 88001<br/>
                  </p>
              </div>

          </div>
          <! --/row -->
      </div>
      <! --/container -->
  </div>
  <! --/footerwrap -->
	 
    <!-- Bootstrap core JavaScript
    ================================================== -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script type="text/javascript">
	  var Constants = {
		  base_url: '<?= base_url() ?>'
		  <?php if($this->session->userdata('logged_in')): ?>
		  , uid: <?php echo $this->session->userdata('uid'); ?>
		  <?php endif; ?>
	  };
  </script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>

  <script type="text/javascript" src="<?php echo base_url('js/news.js'); ?>"></script>
  <script src="<?php echo base_url('js/nav.js'); ?>"></script>

  <script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>


  </body>
</html>
