<?php if ($this->session->userdata("logged_in")): ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>MESH | News</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

        <!-- Custom styles -->
        <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('/css/jrac.css'); ?>" rel="stylesheet">
        <link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            h1, h2, h3, h4, h5, h6 {
                color: black;
            }
        </style>
    </head>

    <body style="background-color: white;">

    <!-- Fixed navbar -->
    <?php include_once("/home/http/mesh/application/views/templates/navbar.php"); ?>

    <!-- *****************************************************************************************************************
     COLOR WRAP
     ***************************************************************************************************************** -->
    <div id="color-banner">
        <div class="container">
            <div class="row">
                <h3>Create Post</h3>
            </div><!-- /row -->
        </div> <!-- /container -->
    </div><!-- /color -->

    <div class="container mtb">

        <div class="col-sm-12">
            <?php $attributes = array('id' => 'form-create'); ?>
            <?php echo form_open(base_url() . 'index.php/News/new_post/'); ?>
                <p id="feedback" style="color: red;"><?php echo validation_errors(); ?></p>
                <div class="form-group">
                    <label style="color: black;" for="title">Title</label>
                    <input type="text" class="form-control"  value="<?php echo set_value('title'); ?>" name="title" id="title" placeholder="The title of your article">
                </div>
                <div class="form-group" style="color: black;">
                    <label style="color: black;" for="title">
                        Post content <span style="color: red;">(Note: Images uploaded using the editor will not be stored on our server; upload with care.)</span>
                    </label>
                    <textarea type="text" style="color: black;" name="content" class="form-control" id="content"><?php echo set_value('content'); ?></textarea>
                </div>
                <div class="form-group">
                    <label style="color: black;" for="tags">Tags</label>
                    <input type="text" name="tags" value="<?php echo set_value('tags'); ?>" class="form-control" id="tags" placeholder="A comma seperated list; no spaces">
                </div>
                <button id="submit-post" type="submit" class="btn btn-default pull-right" onclick="nicEditors.findEditor('content').saveContent();">Submit</button>
            <?php echo form_close(); ?>
        </div>

    </div>

    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
    <script src="<?php echo base_url('js/nav.js'); ?>"></script>

    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
    <script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>

    <script type="text/javascript">

        $(document).ready(function() {


            $("#submit-post").click(function(e) {
                if(nicEditors.findEditor('content').getContent() == "<br>")
                {
                    $("#feedback").text("please input text for the article");
                    e.preventDefault();
                }
            });
        });

    </script>

    </html>

<?php endif; ?>

