<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH | Research</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles -->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="background-color: white;">

<!-- Fixed navbar -->
<?php include_once("templates/navbar.php"); ?>

<!-- *****************************************************************************************************************
 color WRAP
 ***************************************************************************************************************** -->
<div id="color-banner">
    <div class="container">
        <div class="row">
            <h3>Research</h3>
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /blue -->


<!-- *****************************************************************************************************************
 CONTENT
 ***************************************************************************************************************** -->
<div class="container-fluid">
    <div class="col-md-4 col-md-offset-3">
        <img src="<?php echo base_url('assets/img/construction.png'); ?>" class="img-responsive">
    </div>
</div>

<!-- *****************************************************************************************************************
FOOTER
***************************************************************************************************************** -->
<div id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h4>About</h4>
                <div class="hline-w"></div>
                <p>This is a project conducted within the iCREDITS smart grid research center at New Mexico State University. It is funded by the National Science Foundation. Although we are still in the early stages of development, we have high hopes that the project will be successful!</p>
            </div>

            <div class="col-md-4">
                <h4>Get Involved</h4>
                <div class="hline-w"></div>
                <p>
                    Are you interested in Home Automation? Want to learn more about the Internet of Things? Or are you looking for some practical experience with real systems? We are always looking for enthusiastic contributors. <a href="<?php echo base_url(); ?>index.php/Welcome/contribute/">Find out how</a>!
                </p>
            </div>

            <div class="col-md-4">
                <h4>Contact</h4>
                <div class="hline-w"></div>
                <p>
                    Science Hall,
                    Room #134<br/>
                    New Mexico State University<br/>
                    Las Cruces, NM 88001<br/>
                </p>
            </div>

        </div>
        <! --/row -->
    </div>
    <! --/container -->
</div>
<! --/footerwrap -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
<script src="<?php echo base_url('js/research.js'); ?>"></script>
<script src="<?php echo base_url('js/nav.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>


</body>
</html>
