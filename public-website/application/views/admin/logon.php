<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Logon</title>
    <link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>">
</head>

<body>
<div class="login">
    <div class="login-screen">
        <div class="app-title">
            <h1 style="color: black;">Admin Login</h1>
    </div>

    <div class="login-form">
        <div class="control-group">
            <input type="email" class="login-field" value="" placeholder="email" id="login-email">
            <label class="login-field-icon fui-user" for="login-name"></label>
        </div>

        <div id="feedback" style="color: red";></div>

        <a id="next" class="btn btn-primary btn-large btn-block" href="#">Next</a>
        <a class="login-link" href="#">Lost your password?</a>
    </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script type="text/javascript">
        var Constants = {
            base_url: '<?= base_url() ?>'
        }
    </script>
    <script type="text/javascript" src="<?php echo base_url('js/logon.js'); ?>"></script>
</html>