<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>MESH | Contact</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">

    <!-- Custom styles -->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    <link href="<?php echo base_url('/css/jrac.css'); ?>" rel="stylesheet">
    <link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body style="background-color: white;">

  <!-- Fixed navbar -->
  <?php include_once ("templates/navbar.php"); ?>

	<!-- *****************************************************************************************************************
	 color WRAP
	 ***************************************************************************************************************** -->
	<div id="color-banner">
	    <div class="container">
			<div class="row">
				<h3>Contact</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->
	 
	<!-- *****************************************************************************************************************
	 CONTACT FORMS
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row">
	 		<div class="col-sm-8" style="color: black;">
	 			<h4 style="color: #5A0000;">Just Get In Touch!</h4>
	 			<div class="hline"></div>
		 			<p style="color: black">Do you want to know more? Feel free to contact us using the form below. Thank you for your interest!</p>
					<?php echo form_open(base_url() . 'index.php/Email/'); ?>
					  <div class="form-group">
					    <label for="inputName">Your Name</label>
					    <input style="text-align: left;" type="text" class="form-control" id="inputName" name="inputName" value="<?php echo set_value('inputName'); ?>">
                          <div class="registration-errors"><?php echo form_error('inputName'); ?></div>
					  </div>
					  <div class="form-group">
					    <label for="inputEmail">Email address</label>
					    <input style="text-align: left;" type="email" class="form-control" id="inputEmail" name="inputEmail" value="<?php echo set_value('inputEmail'); ?>">
                          <div class="registration-errors"><?php echo form_error('inputEmail'); ?></div>
					  </div>
					  <div class="form-group">
					    <label for="inputSubject">Subject</label>
					    <input style="text-align: left;" type="text" class="form-control" id="inputSubject" name="inputSubject" value="<?php echo set_value('inputSubject'); ?>">
                        <div class="registration-errors"><?php echo form_error('inputSubject'); ?></div>
					  </div>
					  <div class="form-group">
					  	<label for="inputMessage">Message</label>
					  	<textarea class="form-control" id="inputMessage" name="inputMessage" rows="3"></textarea>
                          <div class="registration-errors"><?php echo form_error('inputMessage'); ?></div>
					  </div>
					  <button type="submit" class="btn btn-theme">Submit</button>
					<?php echo form_close(); ?>
			</div><! --/col-lg-8 -->
	 		
	 		<div class="col-sm-4">
		 		<h4 style="color: #5A0000;">Our Address</h4>
		 		<div class="hline"></div>
		 			<p style="color: black;">
						New Mexico State University<br/>
		 				Science Hall, Room 124<br/>
		 				Las Cruces NM, 88001<br/>
		 				United States<br/>
		 			</p>
	 		</div>
	 	</div><! --/row -->
	 </div><! --/container -->


	<!-- *****************************************************************************************************************
    FOOTER
    ***************************************************************************************************************** -->
	<div id="footerwrap">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h4>About</h4>
					<div class="hline-w"></div>
					<p>
						This is a project conducted within the iCREDITS smart grid research center at New Mexico State University. It is funded by the National Science Foundation. Although we are still in the early stages of development, we have high hopes that the project will be successful!</p>
				</div>

				<div class="col-md-4">
					<h4>Get Involved</h4>
					<div class="hline-w"></div>
					<p>
						Are you interested in Home Automation? Want to learn more about the Internet of Things? Or are you looking for some practical experience with real systems? We are always looking for enthusiastic contributors. <a href="<?php echo base_url(); ?>index.php/Welcome/contribute/">Find out how</a>!
					</p>
				</div>

				<div class="col-md-4">
					<h4>Contact</h4>
					<div class="hline-w"></div>
					<p>
						Science Hall,
						Room #134<br/>
						New Mexico State University<br/>
						Las Cruces, NM 88001<br/>
					</p>
				</div>

			</div>
			<! --/row -->
		</div>
		<! --/container -->
	</div>
	<! --/footerwrap -->
	 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/retina-1.1.0.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.hoverdir.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.hoverex.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.prettyPhoto.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/jquery.isotope.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
  <script src="<?php echo base_url('js/contact.js'); ?>"></script>
  <script src="<?php echo base_url('js/nav.js'); ?>"></script>

  <script type="text/javascript" src="<?php echo base_url('js/jquery.jrac.js'); ?>"></script>


  </body>
</html>
