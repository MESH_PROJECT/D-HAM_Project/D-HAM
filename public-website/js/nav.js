/**
 * Created by Dr. Boyd on 6/30/2016.
 */
$(document).ready(function() {
    $("#change-password").click(function() {
        var pass1  = $("#pass1").val();
        var pass2 =  $("#pass2").val();
        var url = $(this).attr("data-url");

        if(pass1.length < 8) {
            alert("Password must be atleast 8 characters in length");
            return;
        }

        if(pass1 !== pass2) {
            alert("the passwords do not match!");
        } else {
            $.ajax({
                type: "POST",
                url: url + "index.php/Admin/change_password",
                // The key needs to match your method's input parameter (case-sensitive).
                data: {
                    uid: $(this).attr("data-user"),
                    pass: pass1
                },
                dataType: "json",
                success: function(data){
                    if(data === "success") {
                        $("#myModal").modal("hide");
                        alert("password changed");
                    } else {
                        alert("error");
                    }
                },
                failure: function(errMsg) {
                    alert(errMsg);
                }
            });
        }
    });


    //The user has selected a file
    $("#ImageBrowse").change(function() {
        $("#imageUploadForm").before('<div id="img-spinner" class="text-center"><i class="fa fa-refresh fa-spin fa-fw"></i>' +
            '<span class="sr-only">Loading...</span> Loading</div>');
        $("#jrac-contain").remove();
        var file = this.files[0];
        var imagefile = file.type;
        var match = ["image/jpeg","image/png","image/jpg"];
        if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
        {
            // $('#previewing').attr('src','noimage.png');
            alert("Please Select A valid Image File, Only jpeg, jpg and png Images types allowed.");
            return false;
        }
        else {
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
        }
    });

    function imageIsLoaded(e) {
        $("#ImageBrowse").css("color","green");
        loadImgResizer(e.target.result);
    };
    $("#upload-profile-pic").prop("disabled", true);

    $('#imageUploadForm').on('submit',(function(e) {
        e.preventDefault();

        var formData = new FormData(this);
        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data: formData,
            cache:false,
            dataType: 'html',
            contentType: false,
            processData: false,
            success:function(data){
                $("#uploadProfilePicModal").modal("hide");
                // var d = new Date();
                // $("#profile-img").attr("src", "");
                // $("#profile-img").attr("src", data + "?" + d.getTime());
                location.reload();
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
    }));

    function loadImgResizer(imgPath) {

        var url = $("#ImageBrowse").attr("data-url");
        $.ajax({
            url: url + "index.php/Admin/load_resizer",
            data: {pathToImg: imgPath},
            dataType: 'html',
            type: 'post',
            success: function(data) {
                $("#ImageBrowse").before(data);

                $("#img-spinner").remove();
                //run resize javascript
                resizeJS();
                $("#upload-profile-pic").prop("disabled", false);
            },
            error: function() {
            }
        });
    }

    function resizeJS() {
        // Apply jrac on some image.
        $('#img-to-be-modified').jrac({
            'crop_width': 256,
            'crop_height': 256,
            'crop_x': 100,
            'crop_y': 100,
            'crop_resize': false,
            'width': 400,
            'viewport_onload': function() {
                var $viewport = this;
                var inputs = $('#jrac-contain').find('input:text');
                var events = ['jrac_crop_x','jrac_crop_y','jrac_crop_width','jrac_crop_height','jrac_image_width','jrac_image_height'];
                for (var i = 0; i < events.length; i++) {
                    var event_name = events[i];
                    // Register an event with an element.
                    $viewport.observator.register(event_name, inputs.eq(i));
                    // Attach a handler to that event for the element.
                    inputs.eq(i).bind(event_name, function(event, $viewport, value) {
                        $(this).val(value);
                    })
                    // Attach a handler for the built-in jQuery change event, handler
                    // which read user input and apply it to relevent viewport object.
                        .change(event_name, function(event) {
                            var event_name = event.data;
                            $viewport.observator.set_property(event_name,$(this).val());
                        })
                }
            }
        })// React on all viewport events.
            .bind('jrac_events', function(event, $viewport) {
                var inputs = $('#jrac-contain').find('input:text');
                inputs.css('background-color',($viewport.observator.crop_consistent())?'chartreuse':'salmon');
            });
    }

    $("#change-bio").click(function() {
        if($("#new-jobtitle").val() == "" || $("#new-description").val() == "") {
            alert("Don't leave any fields empty please");
        }

        var url = $(this).attr("data-url");
        $.ajax({
            type: "POST",
            url: url + "index.php/Admin/update_bio",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                position: $("#new-jobtitle").val(),
                description: $("#new-description").val()
            },
            dataType: "json",
            success: function(data){
                if(data === "success") {
                    $("#modifyBio").modal("hide");
                    alert("your info has been changed");
                    location.reload();
                } else {
                    alert("error");
                }
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });


    $("#add-user").click(function() {
        if($("#fname").val() == "" || $("#lname").val() == "" || $("#email").val() == "" || $("#position").val() == "" || $("#description").val() == "") {
            alert("fill out all the required fields please");
            return;
        }

        var url = $(this).attr("data-url");
        $.ajax({
            type: "POST",
            url: url + "index.php/Admin/add_user",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                fname: $("#fname").val(),
                lname: $("#lname").val(),
                email: $("#email").val(),
                position: $("#position").val(),
                description: $("#description").val()
            },
            dataType: "json",
            success: function(data){
                if(data === "success") {
                    $("#addUser").modal("hide");
                    alert("user has been added");
                    location.reload();
                } else {
                    alert("error");
                }
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });

    $("#remove-user").click(function() {
        if($("#fname-del").val() == "" || $("#lname-del").val() == "") {
            alert("fill out all the required fields please");
            return;
        }

        var url = $(this).attr("data-url");
        $.ajax({
            type: "POST",
            url: url + "index.php/Admin/remove_user",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                fname: $("#fname-del").val(),
                lname: $("#lname-del").val()
            },
            dataType: "json",
            success: function(data){
                if(data === "success") {
                    $("#addUser").modal("hide");
                    alert("user has been removed");
                    location.reload();
                } else {
                    alert("error");
                }
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });

    $("#changeUserOrder").click(function() {
        var url = $(this).attr("data-url");
        $.ajax({
            type: "POST",
            url: url + "index.php/Admin/get_user_order_modal",
            dataType: "html",
            success: function(data){
                $("#myModal").before(data);
                $("#orderModal").modal("show");
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });

    $(document).on("click", ".save-order", function(){
        var url = $(this).attr("data-url");
        $.ajax({
            type: "POST",
            url: url + "index.php/Admin/save_order_user",
            // The key needs to match your method's input parameter (case-sensitive).
            data: {
                uid: $(this).attr("data-user"),
                display_order: $(this).parent().parent().find("input").val()
            },
            dataType: "json",
            success: function(data){
                if(data === "success") {
                    alert("saved");
                } else {
                    alert("error");
                }
            },
            failure: function(errMsg) {
                alert(errMsg);
            }
        });
    });
     
});
