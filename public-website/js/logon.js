$(document).ready(function() {
    var email = "";
    $("#next").click(function(e) {
        email = $("#login-email").val();
        if(email === "") {
            return;
        }

        $.ajax({
            type: "post",
            url: "user_exists",
            dataType: 'json',
            data: {email: email },
            success: function(response) {
                console.log(response);
                if(response.uid) {
                    $("#next").before('<h5 style="color: black; padding-bottom: 10px;">Your account has been found</h5><div class="control-group">' +
                        '<input type="password" class="login-field" value="" placeholder="Password" id="login-password">' +
                        '<label class="login-field-icon" for="login-password"></label>' +
                        '</div>'
                    );

                    $("#login-email").remove();
                    $("#next").text("login");
                    $("#next").unbind("click");
                    $("#next").attr("id", "log-on-button");
                    $("#log-on-button").attr("data-uid", response.uid);
                }
            }
        });
    });

    $(document).on("click", "#log-on-button", function() {
        var pass = $("#login-password").val();

        $.ajax({
            type: "post",
            url: "login_check",
            dataType: 'json',
            data: {email: email, password: pass, uid: $(this).attr("data-uid")},
            success: function(response) {
                if(response == true) {
                    window.location.href = Constants.base_url;
                } else {
                    $("#login-password").val("");
                    $("#feedback").html("Bad combination");
                }
            }
        });
    });
});
