/**
 * Created by Ventura on 6/14/2016.
 */

$(document).ready(function () {
    $("#specifics-menu").addClass("active");

    $(".editable").click(function(e){
        e.preventDefault();
        $(this).hide();
        var section = $(this).attr("data-edit");
        var headingEle = $(document).find("[data-section-heading='" + section + "']");
        var pEle = $(document).find("[data-section='" + section + "']");

        var oldSecHeading = headingEle.text();
        var oldSecText = pEle.text();


        pEle.replaceWith('<textarea data-section="' + section +'" style="color: black; width:' + pEle.css("width") + '; height: ' + pEle.css("height") + '">' +
            oldSecText.trim() +'</textarea>');

        headingEle.replaceWith('<textarea data-section-heading="' + section +'" style="color: black; margin-top: 20px; width:' + headingEle.css("width") + '; height: ' +
            headingEle.css("height") + '">' + oldSecHeading.trim() +'</textarea>');

        $(this).after('<button class="save-editable btn btn-default" data-save-section=' + section + '>Save</button>');
    });

    $(document).on("click", ".save-editable", function() {
        var section = $(this).attr("data-save-section");

        var headingText = $(document).find("[data-section-heading='" + section + "']").val().trim();
        var pText = $(document).find("[data-section='" + section + "']").val().trim();

        console.log(pText);

        //save the section
        $.ajax({
            url: Constants.base_url + "index.php/Admin/save_section",
            data: {
                sid: section,
                heading: headingText,
                text: pText,
                uid: Constants.uid
            },
            dataType: 'json',
            type: 'post',
            success: function(data) {
                location.reload();
            },
            error: function() {
            }
        });

    });


    $('#collapseOne').on('show.bs.collapse', function () {
        $("#load-img").attr("src", Constants.base_url + "assets/img/website.png");
    });
    $('#collapseTwo').on('show.bs.collapse', function () {
        $("#load-img").attr("src", Constants.base_url + "assets/img/face.jpg");
    });
    $('#collapseThree').on('show.bs.collapse', function () {
        $("#load-img").attr("src", Constants.base_url + "assets/img/communication.png");
    });
    $('#collapseFour').on('show.bs.collapse', function () {
        $("#load-img").attr("src", Constants.base_url + "/assets/img/time.jpg");
    });
});


