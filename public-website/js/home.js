$(window).ready(function() {
    $("#home-menu").addClass("active");

    $(window).load(function() {
        $(".a0").each(function(i, l) {
            var parentHeight = $(this).height();
            var descriptionHeight = $(this).find("div").height();
            var padding = (parentHeight - descriptionHeight / 2) - 100;
            console.log(padding);
            $(this).css("padding-top", padding );
        });
    });

    // var padding = Math.floor((parentHeight -descriptionHeight)/2);
    // $(".bg").css("padding-top", padding);
    
    $(".editable").click(function(e){
        e.preventDefault();
        $(this).hide();
        var section = $(this).attr("data-edit");
        var headingEle = $(document).find("[data-section-heading='" + section + "']");
        var pEle = $(document).find("[data-section='" + section + "']");

        var oldSecHeading = headingEle.text();
        var oldSecText = pEle.text();


        pEle.replaceWith('<textarea data-section="' + section +'" style="color: black; width:' + pEle.css("width") + '; height: ' + pEle.css("height") + '">' +
            oldSecText +'</textarea>');

        headingEle.replaceWith('<textarea data-section-heading="' + section +'" style="color: black; margin-top: 20px; width:' + headingEle.css("width") + '; height: ' +
            headingEle.css("height") + '">' + oldSecHeading +'</textarea>');

        $(this).after('<button class="save-editable btn btn-default" data-save-section=' + section + '>Save</button>');
    });

    $(document).on("click", ".save-editable", function() {
        var section = $(this).attr("data-save-section");

        var headingText = $(document).find("[data-section-heading='" + section + "']").val();
        var pText = $(document).find("[data-section='" + section + "']").val();

        console.log(pText);

        //save the section
        $.ajax({
            url: Constants.base_url + "index.php/Admin/save_section",
            data: {
                sid: section,
                heading: headingText,
                text: pText,
                uid: Constants.uid
            },
            dataType: 'json',
            type: 'post',
            success: function(data) {
                location.reload();
            },
            error: function() {
            }
        });

    });
});
