$(document).ready(function() {

    //Iterate through each post
    $(".post-content").each(function() {
        var height = $(this).height();
        if(height > 200) {
            $(this).css("height", "100");
            $(this).after('<p>...</p>' + '<p><a class="read-more" href="#">[Read More]</a></p>');
        }
    });

    $(".post-container").on("click", ".read-more", function(e) {
        e.preventDefault();
        $(this).parent().prev().remove();
        $(this).parent().prev().css("height", 'auto');
        $(this).replaceWith('<a class="hide-post" href="#">[Show Less]</a>');
    });

    $(".post-container").on("click", ".hide-post", function(e) {
        e.preventDefault();
        $(this).parent().before("<p>...</p>");
        $(this).parent().prev().prev().css("height", "100px");
        $(this).replaceWith('<a class="read-more" href="#">[Read More]</a>');
    });
});