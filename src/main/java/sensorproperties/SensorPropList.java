package sensorproperties;

import java.util.ArrayList;

/**
 * Created by jeremiah on 11/11/16.
 */
public class SensorPropList {

    private ArrayList<SensorProperty> sensorProperties;

    public SensorPropList(ArrayList<SensorProperty> sensorProperties) {
        this.sensorProperties = sensorProperties;
    }

    public ArrayList<SensorProperty> getSensorProperties() {
        return sensorProperties;
    }

    public void setSensorProperties(ArrayList<SensorProperty> sensorProperties) {
        this.sensorProperties = sensorProperties;
    }

    public boolean containsSensorProperty(SensorProperty sensorProperty) {
        for ( SensorProperty sp : sensorProperties) {
            if (sp.equals(sensorProperty)) {
                return true;
            }
        }

        return false;
    }


}
