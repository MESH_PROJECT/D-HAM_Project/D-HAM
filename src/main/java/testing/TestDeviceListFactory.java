package testing;


import devices.Actuator;
import devices.Device;
import devices.DeviceList;
import devices.Sensor;
import factories.DeviceListFactory;
import factories.PredictiveModelFactory;
import factories.RuleFactory;
import models.PredictiveModel;
import rules.SchedulingRule;
import schedule.Schedule;
import sensorproperties.SensorProperty;
import solver.Solver;
import utilities.Database;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 12/14/16.
 */
public class TestDeviceListFactory {

    public static void main(String[] args) {
        Database database =  new Database();
        DeviceList testDeviceList = DeviceListFactory.constructDeviceList();
        Solver solver;
        Schedule schedule;
        double[] bgLoads = new double[LoadProjectProperties.getInstance().getTimeSteps()];
        double[] neighborPower = new double[LoadProjectProperties.getInstance().getTimeSteps()];
        for(int i =0; i < LoadProjectProperties.getInstance().getTimeSteps(); i++) {
            bgLoads[i] = 0.0;
            neighborPower[i] = 0.0;
        }

        ArrayList<Actuator> actuators = new ArrayList<>();
        ArrayList<Sensor> sensors = new ArrayList<>();
        for(Device device : testDeviceList.getDevices()) {
            if (device instanceof Sensor) {
                sensors.add((Sensor)device);
            }
            else {
                actuators.add((Actuator) device);
            }
        }

        for( Sensor sensor : sensors) {

                PredictiveModel m = PredictiveModelFactory.constructPredictiveModel(sensor, actuators);
                System.out.println(m.toString());
        }

        ArrayList<HashMap<String, String>> rulesHMAL = database.selectRules();
        ArrayList<SchedulingRule> rules = new ArrayList<>();

        for(HashMap<String, String> ruleHM : rulesHMAL) {
            rules.add(RuleFactory.constructScheduleRule(ruleHM));
        }

        for (SchedulingRule sr : rules) {
            System.out.println(sr.toString());
        }

        solver = new Solver(rules, bgLoads);

        schedule = solver.getFirstSchedule();


        testDeviceList.printDevices();

        System.out.println("Schedule toString:");
        System.out.println(schedule.toString());
        System.out.println("Schedule to JSON auto:");
        System.out.println(schedule.getJSONSchedule());
        System.out.println("Schedule to JSON manual:");
        System.out.println(schedule.getJSONScheduleObject());
    }
}
