package testing;

import devices.Actuator;
import devices.BinarySwitch;
import actions.DeviceAction;
import devices.Device;
import devices.DeviceList;
import factories.DeviceListFactory;

/**
 * Created by ryanread on 1/30/17.
 */
public class BinSwitchTest {

    public static void main(String[] args){
        DeviceList testDeviceList = DeviceListFactory.constructDeviceList();
        for(Device device: testDeviceList.getDevices()){
            if(device instanceof BinarySwitch) {
                BinarySwitch bs = (BinarySwitch) device;
                for(DeviceAction action : bs.getActionList().getActions()) {
                    bs.activate(action);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
