package testing;


import factories.RuleFactory;
import rules.SchedulingRule;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah on 1/15/2017.
 */
public class RuleTest {

    public static void main(String [] args) {
        Database database =  new Database();

        ArrayList<HashMap<String, String>> rulesHMAL = database.selectRules();
        ArrayList<SchedulingRule> rules = new ArrayList<>();

        for(HashMap<String, String> ruleHM : rulesHMAL) {
            rules.add(RuleFactory.constructScheduleRule(ruleHM));
        }

        for (SchedulingRule sr : rules) {
            System.out.println(sr.toString());
        }
    }
}
