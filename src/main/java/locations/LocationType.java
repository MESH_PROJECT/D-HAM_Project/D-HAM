package locations;

/**
 * Created by jeremiah on 10/21/16.
 */
public enum LocationType {

    NONE(0),
    ROOM(1),
    ACTUATOR(2);

    private int value;

    LocationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static LocationType assignLocationType(int locationType) {
        for (LocationType lt : LocationType.values()) {
            if (lt.value == locationType) {
                return lt;
            }
        }

        return null;
    }

}
