package behaviours.testing;

import agents.MessageTestingAgent;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import utilities.LoadProjectProperties;

import java.util.Random;


/**
 * Created by jeremiah on 2/8/17.
 */
public class MGMInfoMessage extends Behaviour {

    private int cycleNumber;
    private MessageTestingAgent myAgent;
    private Random random = new Random();
    private int debugLevel = 0;

    public MGMInfoMessage(MessageTestingAgent myAgent) {
        this.myAgent = myAgent;
        this.cycleNumber = 0;
    }

    @Override
    public void action() {


        //generate message information
        double gain = random.nextDouble();
        double [] ep = new double[LoadProjectProperties.getInstance().getTimeSteps()];

        for (int i = 0; i < ep.length; i++) {
            ep[i] = random.nextDouble();
        }

        //construct json object
        JsonObject gainContent =  new JsonObject();
        JsonArray epArray = new JsonArray();

        for(int i = 0; i < LoadProjectProperties.getInstance().getTimeSteps(); i++) {
            epArray.add(random.nextDouble());
        }

        gainContent.addProperty("Cycle", cycleNumber);
        gainContent.addProperty("Gain", gain);
        gainContent.add("EP", epArray);

        if(debugLevel > 2) {
            System.out.println("Values in JSON object:");
            System.out.println(gainContent.toString());
        }

        //get house agent
        DFAgentDescription dfAgentDescription = new DFAgentDescription();
        ServiceDescription serviceDescription = new ServiceDescription();

        serviceDescription.setType("house-agent");
        dfAgentDescription.addServices(serviceDescription);

        try {
            DFAgentDescription[] result = DFService.search(myAgent, dfAgentDescription);


            if(result.length > 0) {

                if(debugLevel > 2) {
                    System.out.println("Following Agent Found: " + result[0].getName());
                }
                //create message
                ACLMessage gainMessage = new ACLMessage(ACLMessage.INFORM);
                gainMessage.addReceiver(result[0].getName());
                gainMessage.setContent(gainContent.toString());
                gainMessage.setConversationId("MGM-Info");
                myAgent.send(gainMessage);

            }
        } catch (FIPAException e) {
            e.printStackTrace();
        }




        //increment cycle number
        cycleNumber++;
    }

    @Override
    public boolean done() {
        if(cycleNumber == LoadProjectProperties.getInstance().getMgmCycles())
            return true;

        return false;
    }
}
