package behaviours.testing;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by jeremiah on 2/9/17.
 */
public class PrintDebugMessage extends OneShotBehaviour {

    private Agent myAgent;
    private String message;
    private int separatorLength;
    private int spaceBufferLength;
    private String separatorCharacter;

    public PrintDebugMessage(Agent myAgent, String message) {
        this.separatorCharacter = "+";
        this.myAgent = myAgent;
        this.message = message;
        this.separatorLength = 100;
        this.spaceBufferLength = separatorLength - message.length() - 6;



    }

    @Override
    public void action() {
        System.out.println();
        this.printSeparator();
        System.out.println();
        System.out.print("++ " + message);
        for(int i = 0; i < spaceBufferLength; i++) {
            System.out.print(" ");
        }
        System.out.println(" ++");
        this.printSeparator();
        System.out.println();
        System.out.println();

    }

    private void printSeparator() {
        for(int i = 0; i < separatorLength; i++) {
            System.out.print(separatorCharacter);
        }

    }
}
