package behaviours.testing;

import agents.HouseAgent;
import devices.Actuator;
import devices.Device;
import jade.core.behaviours.TickerBehaviour;
import schedule.ScheduleEntry;
import utilities.LoadProjectProperties;

/**
 * Created by jeremiah on 1/31/17.
 */
public class DaySimulation extends TickerBehaviour {

    int timeSteps = LoadProjectProperties.getInstance().getTimeSteps();
    int currentTimeStep;

    private HouseAgent myAgent;

    public DaySimulation(HouseAgent myAgent, long period) {
        super(myAgent, period);
        this.myAgent = myAgent;
        currentTimeStep = 0;
    }

    @Override
    protected void onTick() {
        if(currentTimeStep >= timeSteps) {
            currentTimeStep = 0;
        }

        System.out.println("Current TS: " + currentTimeStep);

        if(myAgent.getCurrentSchedule() != null) {
            for(int deviceID : myAgent.getCurrentSchedule().getScheduleEntryMap().keySet()) {
                Device device = myAgent.getDeviceList().getDevice(deviceID);
                if( device instanceof Actuator) {
                    Actuator actuator = (Actuator) device;
                    actuator.activate(myAgent.getCurrentSchedule().getScheduleEntryMap().get(deviceID).getScheduledAction(currentTimeStep));
                }
            }
        }


        currentTimeStep++;
    }
}
