package behaviours.testing;

import agents.HouseAgent;
import devices.DeviceList;
import schedule.Schedule;
import utilities.LoadProjectProperties;
import jade.core.behaviours.OneShotBehaviour;
import java.io.*;
import java.time.Instant;
import java.util.Random;

/**
 * Created by jeremiah on 6/30/16.
 */
public class GenerateSchedule extends OneShotBehaviour {

    private HouseAgent myAgent;
    private Schedule currentSchedule;

    public GenerateSchedule(HouseAgent myAgent) {

        this.myAgent = myAgent;
    }

    @Override
    public void action() {

        try {

            printStarSeparator(100);

            if(myAgent.isInDebug()) System.out.println("Attempting to get schedule!");
            currentSchedule = myAgent.getSolver().getFirstSchedule();
            if(myAgent.isInDebug()) {
                System.out.println("Schedule complete!");
                System.out.println(currentSchedule.toString());
            }
            printStarSeparator(100);
            System.out.println();

            myAgent.setCurrentSchedule(currentSchedule);

        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to print star separator
     * @param numberToPrint number of starts to print defaults to 100
     */
    private void printStarSeparator(int numberToPrint) {

        if (numberToPrint == 0) {
            numberToPrint = 100;
        }

        for(int i = 0; i < numberToPrint; i++) {
            System.out.print("*");
        }

        System.out.println();
    }


}
