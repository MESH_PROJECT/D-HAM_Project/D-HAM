package behaviours.device;

import agents.HouseAgent;

import devices.Actuator;
import actions.DeviceAction;
import jade.core.behaviours.*;

public class ActivateAction extends OneShotBehaviour {

    private HouseAgent myAgent;
    private DeviceAction actionToActivate;
    private Actuator device;

    public ActivateAction(HouseAgent myAgent, Actuator device, DeviceAction actionToActivate) {
        this.myAgent = myAgent;
        this.actionToActivate = actionToActivate;
        this.device = device;
    }
    
    public void action() {

        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }

        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }
        System.out.println();

    }
}