package behaviours.general;

import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import utilities.LoadProjectProperties;

/**
 * Created by jeremiah on 2/8/17.
 */
public class RegisterWithDF extends OneShotBehaviour {
    
    private Agent myAgent;
    private String agentType;
    private String agentName;
    private int debugLevel = LoadProjectProperties.getInstance().getAgentDebugLevel();

    public RegisterWithDF(Agent myAgent, String agentType, String agentName) {
        this.myAgent = myAgent;
        this.agentName = agentName;
        this.agentType = agentType;
    }

    @Override
    public void action() {


        if(debugLevel > 0) {
            System.out.println("Registering Agent with DF.");
        }

        //register agent with DF
        DFAgentDescription dfd =  new DFAgentDescription();
        dfd.setName(myAgent.getAID());

        ServiceDescription sd = new ServiceDescription();
        sd.setType(agentType);
        sd.setName(agentName);

        dfd.addServices(sd);


        if(debugLevel > 0) {
            System.out.println("Agent registered with DF.");
            System.out.println("Registered Name: " + sd.getName());
            System.out.println("Registered Type: " + sd.getType());
        }

        try {
            DFService.register(myAgent, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }
}
