package behaviours.general;

import agents.HouseAgent;
import behaviours.device.ActivateAction;
import behaviours.schedule.MGM;
import behaviours.testing.GenerateSchedule;
import devices.Actuator;
import utilities.LoadProjectProperties;
import actions.DeviceAction;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import utilities.Database;

/**
 * Created by jeremiah on 7/21/16.
 */
public class GeneralMessageChecker extends CyclicBehaviour {

    private HouseAgent myAgent;
    private Database database;
    private final LoadProjectProperties LPP = LoadProjectProperties.getInstance();

    public GeneralMessageChecker(HouseAgent myAgent) {
        this.myAgent = myAgent;
        this.database = new Database();
    }

    @Override
    public void action() {

        MessageTemplate generalMT;
        MessageTemplate conIDGenSchedule;
        MessageTemplate conIDMGM;
        MessageTemplate conIDActivateAction;
        MessageTemplate orMT;
        MessageTemplate conIDMGMCheck;
        MessageTemplate conIDStartExp;

        //setup conversation id message templates
        conIDGenSchedule = MessageTemplate.MatchConversationId("Generate-Schedule");
        conIDMGM = MessageTemplate.MatchConversationId("MGM");
        conIDMGMCheck = MessageTemplate.MatchConversationId("MGMCheck");
        conIDStartExp = MessageTemplate.MatchConversationId("StartExp");
        conIDActivateAction = MessageTemplate.MatchConversationId("ActivateAction");
        //setup or message template
        orMT = MessageTemplate.or(conIDGenSchedule, conIDMGM);
        orMT = MessageTemplate.or(orMT, conIDMGMCheck);
        orMT = MessageTemplate.or(orMT, conIDStartExp);
        orMT = MessageTemplate.or(orMT, conIDActivateAction);

        //add all MT for general message checking
        generalMT = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST), orMT);


        ACLMessage generalMessage = myAgent.receive(generalMT);

        if(generalMessage != null) {

            if(generalMessage.getPerformative() == ACLMessage.REQUEST) {
                if (generalMessage.getConversationId().equals("Generate-Schedule")) {
                    //todo add handler for Generate Schedule
                    System.out.println("Will generate schedule");
                    myAgent.addBehaviour(new GenerateSchedule(myAgent));
                }

                if (generalMessage.getConversationId().equals("MGM")) {
                    //todo add handler for MGM
                    System.out.println("Will start MGM");
                    int experimentNumber = Integer.parseInt(generalMessage.getContent());
                    myAgent.addBehaviour(new MGM(myAgent, Integer.parseInt(generalMessage.getContent())));
                }

                if(generalMessage.getConversationId().equals("ActivateAction")) {
                    System.out.println("Will active action");
                    String[] messageContent = generalMessage.getContent().split(",");
                    int deviceID = Integer.parseInt(messageContent[0]);
                    int actionID = Integer.parseInt((messageContent[1]));
                    Actuator actuator = (myAgent.getDeviceList().getDevice(deviceID) instanceof Actuator)? (Actuator) myAgent.getDeviceList().getDevice(deviceID): null;
                    DeviceAction action = actuator.getActionList().getAction(actionID);

                    if((!(actuator == null)) && (!(action == null))) {
                        myAgent.addBehaviour(new ActivateAction(myAgent, actuator, action));
                    }


                }

            }
        }
        else {
            block();
        }


    }

}
