package behaviours.general;

import agents.HouseAgent;
import factories.DeviceListFactory;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by jeremiah on 1/29/17.
 */
public class InitlizeAgent extends OneShotBehaviour {

    private HouseAgent myAgent;

    public InitlizeAgent(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {

        myAgent.setDeviceList(DeviceListFactory.constructDeviceList());
    }
}
