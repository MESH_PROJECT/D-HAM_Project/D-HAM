package behaviours.schedule;

import agents.HouseAgent;
import jade.core.AID;
import mgm.MGMInfo;
import neighbor.Neighbor;
import schedule.Schedule;
import utilities.LoadProjectProperties;
import jade.core.behaviours.Behaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by jeremiah on 7/21/16.
 */
public class MGM extends Behaviour {

    private HouseAgent myAgent;
    private int cycleNumber;
    private int maxCycles;
    private Schedule currentSchedule;
    private Schedule newSchedule;
    private final LoadProjectProperties LPP;
    private double gain;
    ArrayList<Double> gainsRecieved = new ArrayList<>();
    private Random random = new Random();
    private Instant mgmStart;
    private Instant mgmEnd;
    private Instant cycleStart;
    private Instant cycleEnd;
    private Instant solverStart;
    private Instant solverEnd;


    public MGM(HouseAgent myAgent, int maxCycles) {

        mgmStart = Instant.now();
        this.LPP = LoadProjectProperties.getInstance();
        this.myAgent = myAgent;
        this.maxCycles = maxCycles;
        this.gain = 0.0;

    }

    @Override
    public void action() {
        double [] testEP =  new double[LPP.getTimeSteps()];
        int numberOfStars = 50;
        for(int i = 0; i < testEP.length; i++) {
            testEP[i] = random.nextDouble() * 10;
        }

        try {

            if(myAgent.getMgmInformation().get(cycleNumber -1) != null || cycleNumber == 0) {
                if (cycleNumber == 0) {

                    double[] initialNPower = new double[LPP.getTimeSteps()];
                    for (int i = 0; i < initialNPower.length; i++) {
                        initialNPower[i] = 0.0;
                    }
                    this.printSeparator(numberOfStars);
                    cycleStart = Instant.now();
                    System.out.println("Starting MGM Behavior Cycle: " + cycleNumber);
                    //initialize schedule for cycle 0

                    System.out.println(currentSchedule.toString());
                    //end cycle
                    cycleEnd = Instant.now();
                    System.out.println("Total Cycle Time: " + Duration.between(cycleStart, cycleEnd).getNano()*1000 + " ms");
                    System.out.println("Ending Cycle: " + cycleNumber);


                    this.printSeparator(numberOfStars);
                }
                else {
                    this.printSeparator(numberOfStars);

                    cycleStart = Instant.now();
                    System.out.println("Starting MGM Behavior Cycle: " + cycleNumber);
                    System.out.println("Generating New Schedule");
                    solverStart = Instant.now();
                    double newScheduleCost = newSchedule.getUtility();
                    solverEnd = Instant.now();
                    System.out.println("New Schedules Utility: " + newSchedule.getUtility());
                    System.out.println("Calculating gain of new schedule vs current schedule");

                    gain = this.calculateGain(newSchedule.getUtility(), currentSchedule.getUtility());
                    System.out.println("Gain form calculation: " + gain);



                    if(myAgent.getNeighbors().size() > 0) {

                        if(!gainsRecieved.isEmpty()) {
                            gainsRecieved.clear();
                        }

                        ACLMessage gainMessage = new ACLMessage(ACLMessage.INFORM);
                        gainMessage.setContentObject(gain);
                        gainMessage.setConversationId("Gain-Message");

                        for (AID aid : myAgent.getNeighbors().values() ) {
                            gainMessage.addReceiver(aid);
                        }

                        myAgent.send(gainMessage);


                        //wait for gain message
                        for(Neighbor n: myAgent.getNeighborList().getNeighbors()) {
                            MessageTemplate gainMessageCID = MessageTemplate.MatchConversationId("Gain-Message");
                            MessageTemplate gainMessageN = MessageTemplate.MatchSender(n.generateAID());
                            MessageTemplate gainMessagePerformative = MessageTemplate.MatchPerformative(ACLMessage.INFORM);

                            MessageTemplate gainMessageAll = MessageTemplate.and(gainMessageCID, gainMessageN);
                            gainMessageAll = MessageTemplate.and(gainMessagePerformative, gainMessageAll);
                            

                            ACLMessage gainReceived = myAgent.blockingReceive(gainMessageAll);
                            if (gainReceived != null) {


                                gainsRecieved.add((Double) gainReceived.getContentObject());
                            }
                        }
                    }


                    boolean bestGain = true;

                    //checking for best gain among connected agents
                    for(Double g:gainsRecieved) {
                        System.out.println("Comparing my gain to :" + g);
                        if(gain > g) {
                            System.out.println("I don't have the best gain");
                            bestGain = false;
                        }
                    }
                    if(bestGain){
                        System.out.println("Taking new schedule");
                        currentSchedule = newSchedule;
                        newSchedule = null;

                    }

                    cycleEnd = Instant.now();
                    System.out.println("Total Cycle Time: " + Duration.between(cycleStart, cycleEnd).getSeconds() + " secs");
                    System.out.println("Ending Cycle: " + cycleNumber);
                    this.printSeparator(numberOfStars);
                }

                MGMInfo currentCycleInfo = new MGMInfo(cycleNumber, gain, testEP);

                myAgent.addBehaviour(new SendMGMInfo(myAgent, currentCycleInfo));

                cycleNumber++;

            }
            else {
                this.block(5000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean done() {

        if(cycleNumber == maxCycles) {
            mgmEnd = Instant.now();
            Duration mgmTotalDuration  = Duration.between(mgmStart, mgmEnd);
            System.out.println("Total MGM Duration: " + mgmTotalDuration.getSeconds());
            return true;
        }
        return false;
    }

    private void printSeparator(int numberOfStars) {
        System.out.println();
        for (int i = 0; i < numberOfStars; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    private double calculateGain(double newUtility, double currentUtility) {
        return newUtility - currentUtility;
    }
}
