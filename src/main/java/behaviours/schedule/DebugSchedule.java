package behaviours.schedule;

/**
 * Begrudgingly created by William Kluegel on 10/1/2016.
 */
/**
 *  Crappy methods for storing whether to debug each variable...
 *  Being used to remove warnings related to if(true) always true compile warning..
 */

public class DebugSchedule {
    public boolean intro()          { return  true; }
    public boolean schedule()       { return  true; }
    public boolean power()          { return false; }
    public boolean delta()          { return false; }

    public boolean predictiveModel(){ return false; }
    public boolean rules()          { return false; }
    public boolean objPower()       { return false; }
    public boolean price()          { return false; }


}
