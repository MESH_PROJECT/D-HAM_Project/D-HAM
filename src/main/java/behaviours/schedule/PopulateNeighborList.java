package behaviours.schedule;

import agents.HouseAgent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import utilities.Database;

import java.util.HashMap;

/**
 * Created by jeremiah on 7/21/16.
 */
public class PopulateNeighborList extends OneShotBehaviour {

    private HouseAgent myAgent;
    private Database database;

    public PopulateNeighborList (HouseAgent myAgent) {
        this.myAgent = myAgent;
        this.database = new Database();
    }

    @Override
    public void action() {

        //myAgent.setNeighborList(database.selectNeighbors());
        HashMap<Integer, AID> neighbors = new HashMap<>();

        for(HashMap<String, String> hm: database.selectNeighborsNew()) {
            AID aid =  new AID(hm.get("Neighbor_Agent_Name") + "@" + hm.get("Neighbor_Platform_Name"), true);
            aid.addAddresses(hm.get("Neighbor_Address"));

            neighbors.put(Integer.parseInt(hm.get("Neighbor_ID")), aid);
        }

        if(neighbors.size() > 0) {
            myAgent.setNeighbors(neighbors);
        }
        else {
            myAgent.setNeighbors(new HashMap<>());
        }

    }
}
