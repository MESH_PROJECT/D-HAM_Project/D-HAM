package behaviours.schedule;

import agents.HouseAgent;
import factories.DeviceListFactory;
import jade.core.behaviours.OneShotBehaviour;

/**
 * Created by jeremiah on 7/22/16.
 */
public class PopulateDeviceList extends OneShotBehaviour {

    private HouseAgent myAgent;

    public PopulateDeviceList(HouseAgent myAgent) {
        this.myAgent = myAgent;
    }

    @Override
    public void action() {

        myAgent.setDeviceList(DeviceListFactory.constructDeviceList());


    }
}
