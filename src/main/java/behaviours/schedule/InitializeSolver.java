package behaviours.schedule;

import agents.HouseAgent;
import devices.Actuator;
import devices.Device;
import devices.Sensor;
import factories.PredictiveModelFactory;
import factories.RuleFactory;
import jade.core.behaviours.OneShotBehaviour;
import models.PredictiveModel;
import rules.SchedulingRule;
import solver.Solver;
import utilities.Database;
import utilities.LoadProjectProperties;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 1/29/17.
 */
public class InitializeSolver extends OneShotBehaviour {


    private HouseAgent myAgent;
    private double[] bgLoads;


    public InitializeSolver(HouseAgent myAgent) {
        this.myAgent = myAgent;

        bgLoads = new double[LoadProjectProperties.getInstance().getTimeSteps()];

        for(int i = 0; i < bgLoads.length; i++) {
            bgLoads[i]= 0.0;
        }
    }

    public InitializeSolver(HouseAgent myAgent, double[] bgLoads) {
        this.myAgent = myAgent;
        this.bgLoads = bgLoads;
    }

    @Override
    public void action() {
        if(myAgent.isInDebug()) {
            printSeparator();
            System.out.println("Starting initialize solver behavior");
        }
        Database database =  new Database();
        ArrayList<Actuator> actuators = new ArrayList<>();
        ArrayList<Sensor> sensors = new ArrayList<>();

        if(myAgent.isInDebug()) {
            System.out.println("Spiting Device List into actuators and sensors");
        }

        for(Device device : myAgent.getDeviceList().getDevices()) {
            if (device instanceof Sensor) {
                sensors.add((Sensor)device);
            }
            else {
                actuators.add((Actuator) device);
            }
        }

        for( Sensor sensor : sensors) {
            PredictiveModel m = PredictiveModelFactory.constructPredictiveModel(sensor, actuators);
            System.out.println(m.toString());
        }

        ArrayList<HashMap<String, String>> rulesHMAL = database.selectRules();
        ArrayList<SchedulingRule> rules = new ArrayList<>();

        if(myAgent.isInDebug()) {
            System.out.println("Getting rules from database");
        }

        for(HashMap<String, String> ruleHM : rulesHMAL) {
            rules.add(RuleFactory.constructScheduleRule(ruleHM));
        }

        if(myAgent.isInDebug()) {
            for (SchedulingRule sr : rules) {
                System.out.println(sr.toString());
            }
        }
        myAgent.setSolver(new Solver(rules, bgLoads));
    }

    private void printSeparator() {
        for(int i = 0; i < 100; i++) {
            System.out.print("*");
        }
        System.out.println();
    }
}
