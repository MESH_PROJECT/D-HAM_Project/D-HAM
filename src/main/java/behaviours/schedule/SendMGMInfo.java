package behaviours.schedule;

import agents.HouseAgent;
import mgm.MGMInfo;
import neighbor.Neighbor;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.io.IOException;

/**
 * Created by jeremiah on 7/27/16.
 */
public class SendMGMInfo extends OneShotBehaviour {

    private HouseAgent myAgent;
    private MGMInfo mgmInfo;

    public SendMGMInfo(HouseAgent myAgent, MGMInfo mgmInfo) {
        this.myAgent = myAgent;
        this.mgmInfo = mgmInfo;
    }

    @Override
    public void action() {

        //create message
        ACLMessage mgmInfoMessage = new ACLMessage(ACLMessage.INFORM);

        //set conversation id
        mgmInfoMessage.setConversationId("MGM-Info");

        //add receivers to message
        for(Neighbor n: myAgent.getNeighborList().getNeighbors()) {
            mgmInfoMessage.addReceiver(n.generateAID());
        }

        //set content object for message to the provided MGMInfo object
        try {
            mgmInfoMessage.setContentObject(mgmInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //send message
        myAgent.send(mgmInfoMessage);



    }
}
