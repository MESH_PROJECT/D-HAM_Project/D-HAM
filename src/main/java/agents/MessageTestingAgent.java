package agents;


import behaviours.general.RegisterWithDF;
import behaviours.testing.MGMInfoMessage;
import jade.core.Agent;
import utilities.LoadProjectProperties;

/**
 * Created by jeremiah on 2/8/17.
 */
public class MessageTestingAgent extends Agent {

    private boolean inDebug = LoadProjectProperties.getInstance().isAgentDebug();
    private int debugLevel = LoadProjectProperties.getInstance().getAgentDebugLevel();

    public void setup() {

        this.addBehaviour(new RegisterWithDF(this, "message-testing-agent", "MessageTestingAgent"));
        this.addBehaviour(new MGMInfoMessage(this));
    }

    public void takeDown() {

    }

}
