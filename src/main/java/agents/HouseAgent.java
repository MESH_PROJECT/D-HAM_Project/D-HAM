package agents;

import behaviours.general.GeneralMessageChecker;
import behaviours.general.RegisterWithDF;
import behaviours.schedule.InitializeSolver;
import behaviours.schedule.MGMInfoMessageHandler;
import behaviours.schedule.PopulateDeviceList;
import behaviours.schedule.PopulateNeighborList;
import behaviours.testing.DaySimulation;
import behaviours.testing.PrintDebugMessage;
import behaviours.testing.PrintDeviceList;
import behaviours.testing.PrintNeighborList;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import devices.Actuator;
import devices.Device;
import io.socket.client.IO;
import jade.core.AID;
import jade.core.behaviours.SequentialBehaviour;
import mgm.MGMInfo;
import neighbor.Neighbor;
import neighbor.NeighborList;
import schedule.Schedule;
import solver.Solver;
import utilities.LoadProjectProperties;
import devices.DeviceList;
import io.socket.client.Socket;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jeremiah on 7/20/16.
 */
public class HouseAgent extends Agent {

    //member variables
    private Gson gson = new Gson();
    private NeighborList neighborList;
    private HashMap<Integer, AID> neighbors;
    private DeviceList deviceList;
    private Solver solver;
    private Schedule currentSchedule;
    private Socket socket;
    private final boolean inDebug = LoadProjectProperties.getInstance().isAgentDebug();
    private int debugLevel = LoadProjectProperties.getInstance().getAgentDebugLevel();
    private HashMap<Integer, HashMap<AID, MGMInfo>> mgmInformation = new HashMap<>();

    public NeighborList getNeighborList() { return neighborList; }
    public DeviceList getDeviceList() { return deviceList; }
    public void setNeighborList(NeighborList neighborList) {
        this.neighborList = neighborList;
    }
    public void setDeviceList(DeviceList deviceList) { this.deviceList = deviceList; }

    public HashMap<Integer, AID> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(HashMap<Integer, AID> neighbors) {
        this.neighbors = neighbors;
    }

    public Solver getSolver() {
        return solver;
    }

    public void setSolver(Solver solver) {
        this.solver = solver;
    }

    public boolean isInDebug() {
        return inDebug;
    }

    public int getDebugLevel() { return debugLevel; }

    public HashMap<Integer, HashMap<AID, MGMInfo>> getMgmInformation() {
        return mgmInformation;
    }

    public Schedule getCurrentSchedule() {
        return currentSchedule;
    }

    public void setCurrentSchedule(Schedule currentSchedule) {
        this.currentSchedule = currentSchedule;
    }

    public void setup() {



        this.setupSocketIO();

        //adding startup behaviors
        SequentialBehaviour sequentialBehaviour = new SequentialBehaviour(this);
        if(inDebug) sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "Starting Agent Setup"));
        sequentialBehaviour.addSubBehaviour(new RegisterWithDF(this, "house-agent", "HouseAgent"));
        sequentialBehaviour.addSubBehaviour(new PopulateNeighborList(this));
        sequentialBehaviour.addSubBehaviour(new PopulateDeviceList(this));
        sequentialBehaviour.addSubBehaviour(new InitializeSolver(this));

        if(inDebug){
            sequentialBehaviour.addSubBehaviour(new PrintDeviceList(this));
            sequentialBehaviour.addSubBehaviour(new PrintNeighborList(this));
            sequentialBehaviour.addSubBehaviour(new PrintDebugMessage(this, "End Agent Setup"));
        }

        this.addBehaviour(sequentialBehaviour);
        this.addBehaviour(new GeneralMessageChecker(this));
        this.addBehaviour(new MGMInfoMessageHandler(this));
        if(LoadProjectProperties.getInstance().isSimulation()) this.addBehaviour(new DaySimulation(this, 10000));



    }

    public void takeDown() {
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
    }

    private void setupSocketIO() {

        try {
            socket = IO.socket(LoadProjectProperties.getInstance().getSocketIOAddress());

            socket.on(Socket.EVENT_CONNECT, args -> {
                System.out.println("Connected to server");
            });

            //message to activate an action
            socket.on("device.action", args -> {
                JsonElement element = gson.fromJson(args[0].toString(), JsonElement.class);
                JsonObject actionInfo = element.getAsJsonObject();

                int deviceID = Integer.parseInt(actionInfo.get("Device_ID").getAsString());
                int actionID = Integer.parseInt(actionInfo.get("Action_ID").getAsString());
                if(inDebug) {
                    System.out.println("Attempting to activate action id : " + actionID + " for deviceID: " + deviceID);
                }

                Device device = deviceList.getDevice(deviceID);
                if( device instanceof Actuator) {
                    Actuator actuator = (Actuator) device;
                    actuator.activate(actuator.getActionList().getAction(actionID));
                }

            });

            //message to get current schedule
            socket.on("device.getschedule", args -> {
                //todo after demo take out schedule generation
                double[] np = new double[LoadProjectProperties.getInstance().getTimeSteps()];
                for(int i = 0; i < np.length; i++){
                    np[i] = 0.0;
                }
                System.out.println("device.getSchedule");
                //currentSchedule = solver.getFirstSchedule();
                currentSchedule = solver.getSchedule(np);
                System.out.println(currentSchedule.getJSONScheduleObject());
                socket.emit("device.schedule", currentSchedule.getJSONScheduleObject());

            });

            socket.connect();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }


}
