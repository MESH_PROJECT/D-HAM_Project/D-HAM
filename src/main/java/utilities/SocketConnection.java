package utilities;


import io.socket.client.IO;
import io.socket.client.Socket;
import java.net.URISyntaxException;

/**
 * Created by William Kluegel on 5/24/2016.
 */
public class SocketConnection
{
    private String address;
    private Socket socket;
    private static final SocketConnection INSTANCE = new SocketConnection();

    /*default constructor*/
    private SocketConnection()
    {
        address = "http://127.0.0.1:3000";
        //address = "https://meshversion2-cloned-johnnyvf24.c9users.io:8080";
        // LOCAL:"http://127.0.0.1:3141";
        // MINE DHAM:"http://192.168.1.108:3141";
        // JOHN DHAM:"http://192.168.1.141:3141";
    }

    /*returns a specific instance of socketConnection object*/
    public static SocketConnection getConnection()
    {
        return INSTANCE;
    }

    /*sets address, setting the port to 3141*/
    public void setAddress(String a)
    {
        address = a + ":3000";
    }

    /*creates a socket connection*/
    public Socket createSocketConnection()
    {
        try {
            socket = IO.socket(address);
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return socket;
    }
}