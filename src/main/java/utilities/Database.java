package utilities;

//this is a comment
import neighbor.Neighbor;
import neighbor.NeighborList;
import communication.CommunicationProtocol;
import devices.*;
import locations.Location;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 8/5/16.
 * edited by ryan 10/23/16
 */
public class Database {

    //member variables
    private final LoadProjectProperties LPP;
    private final String CONNECTION_STRING;
    private final String DEFAULT_SCHEMA;
    private Connection conn;

    //constructor
    public Database() {
        LPP = LoadProjectProperties.getInstance();
        CONNECTION_STRING = LPP.getDBConnectionString();
        DEFAULT_SCHEMA = LPP.getDBSchema();
    }

    //public methods

    /**
     * Method to create and return a NeighborList of the House Agents neighbors in the data
     * @return NeighborList of current neighbors in the data
     */
    @Deprecated
    public NeighborList selectNeighbors() {
        NeighborList neighborList = new NeighborList();

        String sql = "SELECT * FROM Neighbors";

        createConnection();
        try {
            Statement neighborSTMT = conn.createStatement();

            ResultSet neighborRS = neighborSTMT.executeQuery(sql);

            while(neighborRS.next()) {
                neighborList.addNeighbor(new Neighbor(neighborRS.getInt("Neighbor_ID"), neighborRS.getString("Neighbor_Name"), neighborRS.getString("Neighbor_Address"), neighborRS.getString("Platform_Name")));
            }

            neighborRS.close();
            neighborSTMT.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        destroyConnection();

        return neighborList;

    }
    @Deprecated
    public ArrayList<String> selectImageIcons() {
        ArrayList<String> imageIcons = new ArrayList<>();

        String sql = "SELECT Icon_URL FROM Device_Icon";

        createConnection();

        try{
            Statement imageIconSTMT = conn.createStatement();
            ResultSet imageIconRS = imageIconSTMT.executeQuery(sql);

            while(imageIconRS.next()) {
                imageIcons.add(imageIconRS.getString("Icon_URL"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        destroyConnection();


        return imageIcons;
    }

    /**
     * Method to select all current java classes for device from the data
     * @return ArrayList of string class names
     */
    @Deprecated
    public ArrayList<String> selectJavaClasses() {
        ArrayList<String> classNames = new ArrayList<>();

        String sql = "SELECT Class_Name FROM Java_Class_Name";

        createConnection();

        try{
            Statement classNameSTMT = conn.createStatement();
            ResultSet classNameRS = classNameSTMT.executeQuery(sql);

            while(classNameRS.next()) {
                classNames.add(classNameRS.getString("Class_Name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        destroyConnection();



        return classNames;
    }

    /**
     * Method to select all communication protocols in the data
     * @return ArrayList of CommunicationProtocol objects
     */
    @Deprecated
    public ArrayList<CommunicationProtocol> selectCommunicationProtocols(){
        ArrayList<CommunicationProtocol> commProtocol = new ArrayList<>();
        String sql = "SELECT * FROM Communication_Protocols";

        createConnection();
        try {
            Statement commProtocolSTMT = conn.createStatement();

            ResultSet deviceTypeRS = commProtocolSTMT.executeQuery(sql);

            while(deviceTypeRS.next()) {
                //commProtocol.add(new CommunicationProtocol(deviceTypeRS.getInt("Protocol_ID"), deviceTypeRS.getString("Protocol_Name")));
            }

            deviceTypeRS.close();
            commProtocolSTMT.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        destroyConnection();

        return commProtocol;
    }

    /**
     * Method to select all devices that are Actuators in the data
     * @return ArrayList of device that implement the Actuator interface
     */
    @Deprecated
    public ArrayList<Actuator> selectActuators() {

        ArrayList<Actuator> actuators = new ArrayList<>();


        createConnection();
        try{
            Statement act = conn.createStatement();
            String sql = "SELECT * FROM Device_Table WHERE Device_Type_ID = 2;";

            ResultSet actuatorRS = act.executeQuery(sql);

            while(actuatorRS.next()) {
                //TODO: implment code to create a class based on data Class_Name value

                //use class forname to create correct class based on db field Class_Name

                //add to actuator array list


            }

            destroyConnection();
        }catch(SQLException e){
            System.out.println("Error executing statements");
        }

        return actuators;
    }

    public ArrayList<HashMap<String, String>> selectAffectedSensorProperties(int actionID) {
        String sql = "SELECT Sensor_Property_ID, Sensor_Property_Name FROM Sensor_Properties NATURAL JOIN Device_Action_Relation WHERE Action_ID = " + actionID;
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectActionSPRelations(int actionID) {
        String sql = "SELECT * FROM Device_Action_SP_Relation WHERE Action_ID = " + actionID;
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectDeviceActions(int deviceID) {
        String sql = "SELECT Action_ID, Action_Name, Action_KWH FROM Device_Actions NATURAL JOIN Device_Action_Relation WHERE Device_ID = " + deviceID;
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectDevices() {
        String sql = "SELECT * FROM Device_Table";
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectRules() {
        String sql = "SELECT * FROM Rule_Table";
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectSensorProperties() {
        String sql = "SELECT * FROM Sensor_Properties";
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectDeviceIcons() {
        String sql = "SELECT * FROM Device_Icon";
        return this.buildArrayListHashMap(sql);
    }

    public ArrayList<HashMap<String, String>> selectNeighborsNew() {
        String sql = "SELECT * FROM Neighbors";
        return this.buildArrayListHashMap(sql);
    }

    public HashMap<String, String> selectDeviceIcon(int iconID) {
        String sql = "SELECT * FROM Device_Icon WHERE Icon_ID =" + iconID;
        return this.buildHashMap(sql);
    }

    public HashMap<String, String> selectCommProtocol(int deviceID) {
        String sql = "SELECT * FROM Comm_Pro_Device_Relation INNER JOIN Communication_Protocols ON Comm_Pro_ID = Protocol_ID WHERE Device_ID = " + deviceID;
        return this.buildHashMap(sql);
    }

    public HashMap<String, String> selectLocation(int locationID) {
        String sql = "SELECT Location_ID, Location_Name, Locations.Location_Type_ID, Location_Type_Name FROM Locations INNER JOIN Location_Types ON Locations.Location_Type_ID = Location_Types.Location_Type_ID WHERE Location_ID = " + locationID;
        return this.buildHashMap(sql);
    }

    public HashMap<String, String> selectSensorProperty(int sensorPropID) {
        String sql = "SELECT * FROM Sensor_Properties WHERE Sensor_Property_ID =" + sensorPropID;
        return this.buildHashMap(sql);
    }


    /**
     * Method to select all locations in the data
     * @return ArrayList of Locations
     */
    @Deprecated
    public ArrayList<Location> selectLocationsOld() {

        ArrayList<Location> locations = new ArrayList<>();

        createConnection();
        try {
            Statement locationSTMT = conn.createStatement();

            //select device types
            String sql = "SELECT * FROM Locations NATURAL JOIN Location_Types";

            ResultSet locationRS = locationSTMT.executeQuery(sql);

            while(locationRS.next()) {
                //locations.add(new Location(locationRS.getInt("Location_ID"), locationRS.getString("Location_Name"), new LocationType(locationRS.getInt("Location_Type_ID"), locationRS.getString("Location_Type_Name"))));
            }

            locationRS.close();
            locationSTMT.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        destroyConnection();

        return locations;


    }


    /**
     * Method to insert into new java class name into the data
     * @param className name of the class to be inserted
     */
    @Deprecated
    public void insertClassName(String className) {

        createConnection();
        String sql = "INSERT INTO Java_Class_Name(Class_Name) VALUES ('" + className +"')";

        try{
            Statement insertClassSTMT = conn.createStatement();
            insertClassSTMT.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();

    }

    /*
    Things we need
    add Device ID
    Device Type ID (foreign key)
    Device Name
    communication protocol
    Icon_ID
    Location ID
     */
    public boolean insertDevice(String device_name, String device_location, int Device_Type_ID, int comm_proto, int icon_id){
        //get location from name.
        int loc_id = 0;

        try {
            createConnection();
            String get_locale = "SELECT Location_ID FROM Locations WHERE Location_Name = \"" + device_location + "\";";
            Statement loc = conn.createStatement();
            System.out.println("we have reached made connection");
            ResultSet rs = loc.executeQuery(get_locale);
            System.out.println("We have executed query");

            loc_id =  (int) rs.getObject(1);
            System.out.println(loc_id);


            destroyConnection();
        }catch(SQLException e){
            System.out.println("Error getting device location");
            return false;
        }


        String sql = "INSERT INTO Device_Table VALUES (7, " + Device_Type_ID + ", \""+
                device_name + "\", " + comm_proto + ", " + icon_id + ", " + loc_id + ");";
        System.out.println(sql);
        try{
            createConnection();
            Statement insertDevice = conn.createStatement();
            insertDevice.executeUpdate(sql);
            destroyConnection();
        } catch(SQLException e){
            System.out.println("Error inserting device." + e.toString());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /*public boolean insertDevice(int deviceID, int deviceType, String deviceName, int locationID, int iconID, int communicationProtocol) {

        //insert device into data
        //INSERT INTO Device_Table (Device_Type_ID, Device_Name, Communication_Protocol_ID, Icon_ID, Location_ID)  VALUES (1, 'Test', 2, 1, 1)
        //return
        return true;
    }*/



    //private methods
    /**
     * Method to create a data connection
     */
    private void createConnection() {
        try {

            File dbfile = new File(this.getClass().getResource(CONNECTION_STRING).getFile());

            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbfile.getAbsolutePath() );
            conn.setSchema(DEFAULT_SCHEMA);
        }
        catch (Exception e) {
            System.err.println("Error Creating Connection:" + e.getMessage());
        }

    }

    /**
     * Method to destroy the connection to the data
     */
    private void destroyConnection() {
        try {

            if (this.conn != null){
                this.conn.close();
            }

        }
        catch (Exception e) {
            System.err.println("Error Shutting Down Connection: " + e);
        }
    }

    private ArrayList<HashMap<String, String>> buildArrayListHashMap(String sql) {
        ArrayList<HashMap<String, String>> hml = new ArrayList<>();
        createConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                HashMap<String,String> hm = new HashMap();
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    hm.put(resultSet.getMetaData().getColumnName(i+1), resultSet.getString(i+1));
                }
                hml.add(hm);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();
        return hml;
    }

    private HashMap<String, String> buildHashMap(String sql) {
        HashMap<String, String> hm = new HashMap<>();
        createConnection();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    hm.put(resultSet.getMetaData().getColumnName(i+1), resultSet.getString(i+1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        destroyConnection();
        return hm;
    }

}
