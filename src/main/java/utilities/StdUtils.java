package utilities;

import java.util.List;

/**
 * Created by ffiorett on 8/22/16.
 */
public class StdUtils {

    public StdUtils() {
    }

    public static double getMax (double[] array) {
        assert (array.length > 0);

        double max = array[0];
        for (int i=1; i<array.length; i++)
            if (max < array[i])
                max = array[i];
        return max;
    }

    public static int getMax (int[] array) {
        assert (array.length > 0);

        int max = array[0];
        for (int i=1; i<array.length; i++)
            if (max < array[i])
                max = array[i];
        return max;
    }

    public static Integer getMax (List<Integer> array) {
        assert (array.size() > 0);

        int max = array.get(0);
        for (int i=1; i<array.size(); i++)
            if (max < array.get(i))
                max = array.get(i);
        return max;
    }


    public static double getMin (double[] array) {
        assert (array.length > 0);

        double min = array[0];
        for (int i=1; i<array.length; i++)
            if (array[i] < min)
                min = array[i];
        return min;
    }

    public static int getMin (int[] array) {
        assert (array.length > 0);

        int min = array[0];
        for (int i=1; i<array.length; i++)
            if (array[i] < min)
                min = array[i];
        return min;
    }

    public static Integer getMin (List<Integer> array) {
        assert (array.size() > 0);

        int min = array.get(0);
        for (int i=1; i<array.size(); i++)
            if (min< array.get(i))
                min = array.get(i);
        return min;
    }


//    public static <T1, T2> T2[] mult(T1[] array, T2 m) {
//        T2[] ret = new T2[array.length];
//
//        for (int i=0; i<array.length; i++) {
//            ret[i] = (T2)(array[i]) * m;
//        }
//        return ret;
//    }

//    public static <T> T getMax(List<T> array) {
//        T max = array.get(0);
//        for (T t : array) {
//            if (max < t) {
//                max = t;
//            }
//        }
//        return max;
//    }
}
