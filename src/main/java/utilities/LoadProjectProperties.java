package utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by jeremiah on 7/21/16.
 */
public class LoadProjectProperties {

    private Properties properties;
    private InputStream propertiesFile;
    private int timeSteps;
    private double[] priceSchema;
    private long schedulerTimeout;
    private int mgmCycles;
    private int alphaPrice;
    private int alphaPower;
    private String socketIOAddress;
    private String zwaveConfig;
    private boolean debug;
    private boolean simulation;
    private boolean agentDebug;
    private boolean solverDebug;
    private int agentDebugLevel;
    private int solverDebugLevel;

    private static LoadProjectProperties ourInstance = new LoadProjectProperties();

    public static LoadProjectProperties getInstance() {
        return ourInstance;
    }

    private LoadProjectProperties() {

        properties = new Properties();

        try {
            propertiesFile = getClass().getClassLoader().getResourceAsStream("config/MESH.properties");
            properties.load(propertiesFile);
            timeSteps = Integer.parseInt(properties.getProperty("TimeSteps"));
            alphaPower = Integer.parseInt(properties.getProperty("AlphaPower"));
            alphaPrice = Integer.parseInt(properties.getProperty("AlphaPrice"));
            priceSchema = new double[timeSteps];
            String[] prices = properties.getProperty("PriceSchema").split(" ");
            for (int i = 0; i < timeSteps; i++) {
                priceSchema[i] = Double.valueOf(prices[i]);
            }
            schedulerTimeout = Long.parseLong(properties.getProperty("SchedulerTimeout"));
            mgmCycles = Integer.parseInt(properties.getProperty("MGMCycles"));
            zwaveConfig = properties.getProperty("ZwaveConfig");
            socketIOAddress = properties.getProperty("socketIOAddress");

            if(properties.getProperty("DebugMode").equalsIgnoreCase("true")) {
                debug = true;
            }
            else {
                debug = false;
            }

            if(properties.getProperty("SimulationMode").equalsIgnoreCase("true")) {
                simulation = true;
            }
            else {
                simulation = false;
            }

            if(properties.getProperty("AgentDebug").equalsIgnoreCase("true")) {
                agentDebug = true;
                agentDebugLevel = Integer.parseInt(properties.getProperty("AgentDebugLevel"));
            }
            else {
                agentDebug = false;
                agentDebugLevel = 0;
            }

            if(properties.getProperty("SolverDebug").equalsIgnoreCase("true")) {
                solverDebug = true;
                agentDebugLevel = Integer.parseInt(properties.getProperty("SolverDebugLevel"));
            }
            else {
                solverDebug = false;
                solverDebugLevel = 0;
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to load the number of Time Steps the project uses
     * @return int time steps
     */
    public int getTimeSteps() {
        return timeSteps;
    }

    public String getDBConnectionString() {
        return properties.getProperty("ConnectionString");
    }

    public String getDBSchema() {
        return properties.getProperty("Schema");
    }

    public String getDBUserName() {
        return properties.getProperty("UserName");
    }

    public String getDBPassword() {
        return properties.getProperty("Password");
    }

    public String getDeviceDictionaryPath() {
        return properties.getProperty("DeviceDictionaryPath");
    }

    public String getRuleDictionaryPath() {
        return properties.getProperty("RuleDictionaryPath");
    }

    public String getRuleFilePath() {
        return properties.getProperty("RulesPath");
    }

    public int getNumberOfSimulatedDevices() {
        return Integer.valueOf(properties.getProperty("NumberOfSimulatedDevices"));
    }
    public String getSocketIOAddress() {
        return socketIOAddress;
    }

    public int getAlphaPrice() {
        return alphaPrice;
    }

    public int getAlphaPower() {
        return alphaPower;
    }

    /**
     * It returns the time in ms allotted to the scheduler to search for a solution.
     */
    public long getSchedulerTimeoutMs() {
        return schedulerTimeout;
    }

    public double[] getPriceSchema() {
        return priceSchema;
    }

    public int getMgmCycles() {return mgmCycles;}

    public String getZwaveConfig(){ return zwaveConfig;}

    public boolean isDebug() {return debug;}

    public boolean isSimulation() {
        return simulation;
    }

    public boolean isAgentDebug() {
        return agentDebug;
    }

    public boolean isSolverDebug() {
        return solverDebug;
    }

    public int getAgentDebugLevel() {
        return agentDebugLevel;
    }

    public int getSolverDebugLevel() {
        return solverDebugLevel;
    }
}
