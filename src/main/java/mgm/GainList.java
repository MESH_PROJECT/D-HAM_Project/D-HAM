package mgm;

import java.util.ArrayList;

/**
 * Created by jeremiah on 7/11/16.
 */
public class GainList {

    private ArrayList<Gain> gains;

    public GainList() {
        gains = new ArrayList<>();
    }

    public GainList(ArrayList<Gain> gains) {
        this.gains = gains;
    }

    public ArrayList<Gain> getGains() { return gains; }

    public void addGain(Gain gainToAdd) {
        gains.add(gainToAdd);
    }

    public void removeGain(Gain gainToRemove) {
        for(Gain g: gains) {
            if(g.equals(gainToRemove)) {
                gains.remove(g);
            }
        }
    }

    public boolean hasGainBeenRecieved(int cyclenumber) {
        for (Gain g : gains) {
            if(g.getCycleNumber() == cyclenumber) {
                return true;
            }
        }

        return false;
    }
}
