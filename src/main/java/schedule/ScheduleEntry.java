package schedule;

import actions.DeviceAction;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import devices.Device;
import utilities.LoadProjectProperties;

import java.util.Arrays;

/**
 * Created by Jeremiah on 1/16/2017.
 */
public class ScheduleEntry {

    private int deviceID;
    private DeviceAction [] scheduledActions;
    private Gson gson;

    public ScheduleEntry(Device device) {
        this.gson = new Gson();
        this.deviceID = device.getID();
        scheduledActions = new DeviceAction[LoadProjectProperties.getInstance().getTimeSteps()];

    }

    public int getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(int deviceID) {
        this.deviceID = deviceID;
    }

    public DeviceAction[] getScheduledActions() {
        return scheduledActions;
    }

    public void setScheduledActions(DeviceAction[] scheduledActions) {
        this.scheduledActions = scheduledActions;
    }

    public void setScheduledAction(int scheduledTime, DeviceAction action) {
        assert(scheduledTime > 0 && scheduledTime < LoadProjectProperties.getInstance().getTimeSteps());

        scheduledActions[scheduledTime] = action;
    }

    public DeviceAction getScheduledAction(int timestep) {
        return scheduledActions[timestep];
    }

    //todo add json support

    public String getJSONScheduleEntry() {
        String scheduleEntry =  gson.toJson(this);

        return scheduleEntry;
    }

    public JsonObject getJSONScheduleEntryObject() {
        JsonObject scheduleEntryJSONObject = new JsonObject();
        JsonArray scheduleEntryJSONArray = new JsonArray();

        scheduleEntryJSONObject.addProperty("Device_ID", this.deviceID);

        for(int i = 0; i < scheduledActions.length; i++) {
            JsonObject entry = new JsonObject();
            entry.addProperty("Action_ID", scheduledActions[i].getActionID());
            entry.addProperty("Timestep", i);
            scheduleEntryJSONArray.add(entry);
        }
        scheduleEntryJSONObject.add("Actions", scheduleEntryJSONArray);

        return scheduleEntryJSONObject;
    }
    @Override
    public String toString() {
        return "ScheduleEntry{" +
                "deviceID=" + deviceID +
                ", scheduledActions=" + Arrays.toString(scheduledActions) +
                '}';
    }
}
