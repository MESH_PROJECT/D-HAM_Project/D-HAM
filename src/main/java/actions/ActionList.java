package actions;

import java.util.ArrayList;

/**
 * Created by jeremiah on 11/11/16.
 */
public class ActionList {

    private ArrayList<DeviceAction> actions;

    public ActionList(ArrayList<DeviceAction> actions) {
        this.actions = actions;
    }

    public ArrayList<DeviceAction> getActions() {
        return actions;
    }

    public void setActions(ArrayList<DeviceAction> actions) {
        this.actions = actions;
    }

    public void addAction(DeviceAction actionToAdd) {
        this.actions.add(actionToAdd);
    }

    public DeviceAction getAction(int actionID) {
        for(DeviceAction a : actions) {
            if(a.getActionID() == actionID)
                return a;
        }
        return null;
    }

    @Override
    public String toString() {
        return "ActionList{" +
                "actions=" + actions +
                '}';
    }
}
