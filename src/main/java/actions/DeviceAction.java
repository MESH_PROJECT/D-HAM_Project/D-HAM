package actions;


import devices.Sensor;
import sensorproperties.SensorPropList;
import sensorproperties.SensorProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeremiah on 10/21/16.
 */
public class DeviceAction {

    private int actionID;
    private String actionName;
    private double actionKWH;
    private SensorPropList affectedSensorProperties;
    private Map<SensorProperty, Double> actionDelta;

    public DeviceAction(int actionID, String actionName, double actionKWH,ArrayList<SensorProperty> affectedSensorProperties, HashMap<SensorProperty, Double> actionDelta) {
        this.actionID = actionID;
        this.actionName = actionName;
        this.actionKWH = actionKWH;
        this.affectedSensorProperties = new SensorPropList(affectedSensorProperties);
        this.actionDelta = actionDelta;

    }

    public int getActionID() {
        return actionID;
    }

    public void setActionID(int actionID) {
        this.actionID = actionID;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public double getActionKWH() {
        return actionKWH;
    }

    public void setActionKWH(double actionKWH) {
        this.actionKWH = actionKWH;
    }

    public SensorPropList getAffectedSensorProperties() {
        return affectedSensorProperties;
    }

    public void setAffectedSensorProperties(SensorPropList affectedSensorProperties) {
        this.affectedSensorProperties = affectedSensorProperties;
    }

    public double getActionDelta(SensorProperty sensorProperty) {

        return actionDelta.get(sensorProperty);
    }

    public Map<SensorProperty, Double> getActionDelta() {
        return actionDelta;
    }

    public void setActionDelta(Map<SensorProperty, Double> actionDelta) {
        this.actionDelta = actionDelta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceAction)) return false;

        DeviceAction that = (DeviceAction) o;

        if (actionID != that.actionID) return false;
        if (Double.compare(that.actionKWH, actionKWH) != 0) return false;
        if (!actionName.equals(that.actionName)) return false;
        return affectedSensorProperties.equals(that.affectedSensorProperties);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = actionID;
        result = 31 * result + actionName.hashCode();
        temp = Double.doubleToLongBits(actionKWH);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + affectedSensorProperties.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DeviceAction{" +
                "actionID=" + actionID +
                ", actionName='" + actionName + '\'' +
                ", actionKWH=" + actionKWH +
                ", affectedSensorProperties=" + affectedSensorProperties +
                '}';
    }
}
