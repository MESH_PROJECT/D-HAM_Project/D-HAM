package meshfx.model;

import devices.Actuator;
import communication.CommunicationProtocol;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import locations.Location;
import utilities.Database;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by jeremiah on 10/21/16.
 */
public class MeshModel {

    private Database database;

    //private members needed for inserts into data
    private String newClassName;

    public void setNewClassName(String newClassName) {
        this.newClassName = newClassName;
    }

    public MeshModel()
    {
        database = new Database();
    }

    public ObservableList<CommunicationProtocol> getProtocolInfo()
    {
        return FXCollections.observableArrayList(database.selectCommunicationProtocols());
    }

    public ObservableList<Location> getLocationList()
    {
        return FXCollections.observableArrayList(database.selectLocationsOld());
    }

    public ObservableList<Actuator> getActuatorList()
    {
        return FXCollections.observableList(database.selectActuators());
    }

    public ObservableList<String> getClassNames() {
        return FXCollections.observableArrayList(database.selectJavaClasses());
    }
    public ObservableList<String> getImageIcons() {
        return FXCollections.observableArrayList(database.selectImageIcons());
    }

    public ArrayList<Image> getImages() {
        ArrayList<Image> images = new ArrayList<>();

        ArrayList<String> imageLocations = database.selectImageIcons();

        images.addAll(imageLocations.stream().map(Image::new).collect(Collectors.toList()));
        return images;
    }


    public void addNewClassName() {
        database.insertClassName(newClassName);
    }
}