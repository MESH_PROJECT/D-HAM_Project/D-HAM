package meshfx.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jorda on 1/4/2017.
 */
public class DeviceOrganizer implements Initializable
{
    @FXML ListView<String> deviceList = new ListView<String>();
    ObservableList<String> items = FXCollections.observableArrayList("Light", "Thermostat");

    @FXML TextField nameChange;

    //Click and highlight a device and then everything else shows up

    //Change icon button (submit a new picture for the icon of that particular device)

    @FXML
    private void handleNameChange(ActionEvent event) throws Exception
    {
        System.out.println(nameChange.getText());
        //System.out.println("Test");
        items.add(nameChange.getText());
        //deviceList.setItems();nameChange.getText();
        deviceList.setItems(items);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        deviceList.setItems(items);
    }
}
