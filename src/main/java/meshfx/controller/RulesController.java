package meshfx.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jorda on 11/11/2016.
 */
public class RulesController implements Initializable
{
    @FXML private Button submitButton;
    @FXML private ComboBox ruleType, affectedSP, relation, timePrefix;
    @FXML private TextField goalState;

    @FXML private void handleSubmitButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) submitButton.getScene().getWindow();
        stage.setTitle("DeviceInformation");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/addDevice.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ruleType.getItems().addAll("Passive", "Active");
        affectedSP.getItems().addAll("Charging", "Temperature", "Dust", "Water Temp");
        relation.getItems().addAll("GT", "LT", "EQ", "GEQ", "LEQ");
        timePrefix.getItems().addAll("N/A");
    }
}
