package meshfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import meshfx.model.MeshModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jerem on 12/10/2016.
 */
public class DeviceInformationController implements Initializable {

    private MeshModel model = new MeshModel();

    @FXML
    private ImageView deviceIcon;
    @FXML
    private ToggleButton deviceToggle;
    @FXML
    private Label commPro;
    @FXML
    private Label loation;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
