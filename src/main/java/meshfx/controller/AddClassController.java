package meshfx.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import meshfx.model.MeshModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 12/9/16.
 */
public class AddClassController implements Initializable {

    private MeshModel model = new MeshModel();

    @FXML
    private Button addClass;
    @FXML
    private Button back;
    @FXML
    private ListView<String> currentClasses;
    @FXML
    private TextField newClassName;



    @FXML
    private void handleBackButton(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage) back.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/settings.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleAddClassButton(ActionEvent actionEvent) throws Exception {
        //todo add verification to this to insure data
        model.setNewClassName(newClassName.getText());
        model.addNewClassName();
        currentClasses.setItems(model.getClassNames());

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentClasses.setItems(model.getClassNames());
    }
}
