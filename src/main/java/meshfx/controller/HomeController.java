package meshfx.controller;

import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import meshfx.model.MeshModel;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable
{
    @FXML private Button roomsButton, settingsButton, deviceButton, agentButton, energyButton;
    @FXML private AnchorPane homeAnchor;
    @FXML private AnchorPane screenSaver;
    @FXML private ImageView deviceImage, energyImage, agentImage, roomsImage;

    private MeshModel meshModel;

    @FXML
    private void handleDeviceButton(ActionEvent event) throws Exception
    {
        /*Stage stage = new Stage();
        stage.setTitle("Devices");
        AnchorPane pane = new AnchorPane();
        Pane pane1 = new Pane();
        pane1.setPrefSize(200, 200);
        pane.setPrefSize(300, 300);
        VBox deviceVbox = new VBox(3);
        //deviceVbox.setPadding(new Insets(1));
        Label L1 = new Label("Device1");
        Label L2 = new Label("Device2");
        Label L3 = new Label("Device3");
        deviceVbox.getChildren().addAll(pane1, L2, L3);
        pane.getChildren().add(deviceVbox);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();*/

        Stage stage = (Stage) deviceButton.getScene().getWindow();
        stage.setTitle("Devices");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/deviceMan.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleRoomsButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) roomsButton.getScene().getWindow();
        stage.setTitle("Rooms");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/roomspage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleEnergyButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) energyButton.getScene().getWindow();
        stage.setTitle("Device Information");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/addDevice.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        //TODO add method that loads device data from data
    }

    @FXML
    private void handleSettingsButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) settingsButton.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/settings.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        //Motioning the buttons for effect on the home page
        TranslateTransition devTT = new TranslateTransition(Duration.seconds(2), deviceButton);
        TranslateTransition ageTT = new TranslateTransition(Duration.seconds(2), agentButton);
        TranslateTransition rooTT = new TranslateTransition(Duration.seconds(2), roomsButton);
        TranslateTransition eneTT = new TranslateTransition(Duration.seconds(2), energyButton);
        devTT.setByX(-36);
        ageTT.setByX(-13);
        rooTT.setByX(13);
        eneTT.setByX(36);
        devTT.play();
        ageTT.play();
        rooTT.play();
        eneTT.play();
    }//end initialize
}//end HomeController