package meshfx.controller;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by Jorda on 11/11/2016.
 */
public class DeviceInfo
{
    private final SimpleStringProperty id;
    private final SimpleStringProperty name;
    private final SimpleStringProperty loc;

    public DeviceInfo(String newID, String newName, String newLoc)
    {
        this.id = new SimpleStringProperty(newID);
        this.name = new SimpleStringProperty(newName);
        this.loc = new SimpleStringProperty(newLoc);
    }

    public String getId() {
        return id.get();
    }

    public SimpleStringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getLoc() {
        return loc.get();
    }

    public SimpleStringProperty locProperty() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc.set(loc);
    }
}//End DeviceInfo
