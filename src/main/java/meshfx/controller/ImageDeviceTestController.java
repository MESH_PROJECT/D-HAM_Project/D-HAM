package meshfx.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.shape.MeshView;
import javafx.util.Callback;
import meshfx.model.MeshModel;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 12/9/16.
 */
public class ImageDeviceTestController implements Initializable {

    private MeshModel model = new MeshModel();
    private Image[] listImages;

    @FXML
    private ListView<String> deviceView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        deviceView.setItems(model.getImageIcons());

        deviceView.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {


            @Override
            public ListCell<String> call(ListView<String> param) {
                return null;
            }
        } );



    }
}
