package meshfx.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by jeremiah on 12/9/16.
 */
public class SettingsController implements Initializable {


    @FXML private Button classEditor;
    @FXML
    private Button deviceInfo;


    @FXML
    private void handleClassEditorButton(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage) classEditor.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/addClass.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleDeviceInfoButton(ActionEvent actionEvent) throws Exception {
        Stage stage = (Stage) classEditor.getScene().getWindow();
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/deviceInformation.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
