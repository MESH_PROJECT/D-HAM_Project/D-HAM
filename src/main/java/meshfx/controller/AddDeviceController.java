package meshfx.controller;

import meshfx.model.MeshModel;
import utilities.Database;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jorda on 11/11/2016.
 */
public class AddDeviceController implements Initializable
{
    @FXML private TextField deviceName;
    @FXML private ListView<String> deviceView, rulesView;
    @FXML private Button addButton, addRuleButton, exitButton;
    @FXML private TableView<DeviceInfo> deviceTable;
    @FXML private TableColumn idCol, nameCol, locCol;
    @FXML private ComboBox deviceLoc, comProtocol;

    @FXML private void handleExitButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.setTitle("MESH");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/homepage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML private void handleRulesButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) addRuleButton.getScene().getWindow();
        stage.setTitle("Rules");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/rulespage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void handleAddButton(ActionEvent event) throws Exception
    {
        Database meshDatabase = new Database();

        //Defining Observable List
        ObservableList<DeviceInfo> deviceData = FXCollections.observableArrayList(
                new DeviceInfo(comProtocol.getValue().toString(), deviceName.getText(), deviceLoc.getValue().toString()));
                //new CommunicationProtocol(, deviceLoc.getValue().toString()));

        //Columns
        idCol.setCellValueFactory(new PropertyValueFactory<DeviceInfo, String>("id"));
        nameCol.setCellValueFactory(new PropertyValueFactory<DeviceInfo, String>("name"));
        locCol.setCellValueFactory(new PropertyValueFactory<DeviceInfo, String>("loc"));

        deviceTable.setItems(deviceData);
        deviceTable.setEditable(true);

        //TODO add method that loads device data from data
        meshDatabase.insertDevice(deviceName.getText(), deviceLoc.getValue().toString() , 1, 1, 1);
        System.out.println(deviceLoc.getValue().toString());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        MeshModel model = new MeshModel();

        deviceLoc.setItems(model.getLocationList());
        comProtocol.setItems(model.getProtocolInfo());

        //deviceTable.setEditable(true);
    }
}
