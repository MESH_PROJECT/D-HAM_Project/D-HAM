package meshfx.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.ColorInput;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Jordan Michael on 8/18/2016.
 */
public class RoomController implements Initializable
{
    @FXML private Button homeButton;
    @FXML private ImageView lamp;
    @FXML private Button onButton;
    @FXML private AnchorPane roomPane;

    @FXML
    private void handleHomeButton(ActionEvent event) throws Exception
    {
        Stage stage = (Stage) homeButton.getScene().getWindow();
        stage.setTitle("MESH");
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("interfaceFXML/homepage.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void handleCategoryItem(ActionEvent event) throws Exception
    {
        File file = new File("C:\\Users\\Jordan Michael\\Documents\\Work\\MESH\\Interface\\src\\meshfx\\Lamp Icon.png");
        Image image = new Image(file.toURI().toString());
        lamp.setImage(image);
    }

    @FXML
    private void handleOnButton(ActionEvent event) throws Exception
    {
        ColorAdjust green = new ColorAdjust();
        green.setSaturation(-1.0);

        Blend greenBlend = new Blend(
                BlendMode.MULTIPLY,
                green,
                new ColorInput(
                        0, 0,
                        lamp.getImage().getWidth(),
                        lamp.getImage().getHeight(),
                        Color.GREEN
                )
        );

        lamp.setEffect(greenBlend);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
