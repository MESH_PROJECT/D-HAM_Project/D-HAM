package models;

import devices.Actuator;
import devices.Device;
import devices.Sensor;
import locations.Location;
import sensorproperties.SensorProperty;
import utilities.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jeremiah on 1/15/2017.
 */
public class PredictiveModel {
    private Location location;
    private SensorProperty sensorProperty;
    private Sensor sensor;
    private ArrayList<Actuator> actuators;
    private static Map<Pair<Location, SensorProperty>, PredictiveModel> predictiveModelMap;

    public PredictiveModel(Sensor sensor, ArrayList<Actuator> actuators) {
        this.location = sensor.getLocation();
        this.sensorProperty = sensor.getAffectedProperty();
        this.sensor = sensor;
        this.actuators = actuators;
        this.predictiveModelMap = new HashMap<>();

        for(Actuator a : actuators) {
            assert (a.getLocation().equals(location));
        }

        predictiveModelMap.put(new Pair<>(location, sensorProperty), this);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public SensorProperty getSensorProperty() {
        return sensorProperty;
    }

    public void setSensorProperty(SensorProperty sensorProperty) {
        this.sensorProperty = sensorProperty;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public ArrayList<Actuator> getActuators() {
        return actuators;
    }

    public void setActuators(ArrayList<Actuator> actuators) {
        this.actuators = actuators;
    }

    public static PredictiveModel valueOf(Location location, SensorProperty sensorProperty) {
        return predictiveModelMap.get(new Pair<>(location, sensorProperty));
    }

    public void clearModelMap() {
        if(predictiveModelMap.isEmpty())
            return;

        predictiveModelMap.clear();
    }

    @Override
    public String toString() {
        return "PredictiveModel{" +
                "location=" + location +
                ", sensorProperty=" + sensorProperty +
                ", sensor=" + sensor +
                ", actuators=" + actuators +
                '}';
    }
}
