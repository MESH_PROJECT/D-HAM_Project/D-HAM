$(document).ready(function() {
    
    //The user is viewing all the device actions available
    $(".device-actions").click(function() {
        var deviceID = $(this).data("device-id");
        $("#" + deviceID + "-action-list").empty();
        
        $.get("/getdevactions/" + deviceID, function(data) {
            $.each(data, function(i, val) {
                
                var html = "";
                //On/Off Switch
                if(val.Action_Name == "on" || val.Action_Name == "off") {
                   html = '<li>' +
                            '<a data-action-id="' + val.Action_ID + '" data-device-id="' + val.Device_ID +
                                '" class="action-partake" href="#">' + 
                                '<i class="fa fa-power-off aria-hidden="true"></i> ' +
                                val.Action_Name + " " +
                            '</a>' +
                            
                            '<hr />'+
                        '</li>';
                    $("#" + deviceID + "-action-list").append(html);
                }
                
                //A device has the HEAT action
                else if(val.Action_Name == "heat") {
                   html = '<li>' +
                            '<a data-action-id="' + val.Action_ID + '" data-device-id="' + val.Device_ID +
                                '" class="action-partake" href="#">' +
                                '<i class="fa fa-chevron-up" aria-hidden="true"></i> ' +
                                val.Action_Name + " " +
                            '</a>' +
                            
                            '<hr />'+
                        '</li>';
                    $("#" + deviceID + "-action-list").append(html);
                } 
                
                //A device has the cool action
                else if(val.Action_Name == "cool") {
                    html = '<li>' +
                            '<a data-action-id="' + val.Action_ID + '" data-device-id="' + val.Device_ID +
                                '" class="action-partake" href="#">' +
                                '<i class="fa fa-chevron-down" aria-hidden="true"></i> ' +
                                val.Action_Name + " " +
                            '</a>' +
                            
                            '<hr />'+
                        '</li>';
                    $("#" + deviceID + "-action-list").append(html);
                } 
                
                //A device has an action that is not yet personalized UI wise
                else {
                    html = '<li>' +
                            '<a data-action-id="' + val.Action_ID + '" data-device-id="' + val.Device_ID +
                                '" class="action-partake" href="#">' + 
                                '<i class="fa fa-power-off aria-hidden="true"></i> ' +
                                val.Action_Name + " " +
                            '</a>' +
                            
                            '<hr />'+
                        '</li>';
                    $("#" + deviceID + "-action-list").append(html);
                }
                
            }); 
        });
    });
    
    //The user has toggled the on/off state of a device
    $(document).on("click", ".action-partake", function(e) {
        var deviceID = $(this).data("device-id");
        var actionID = $(this).data("action-id");
        
        //Tell the agent to start an action
        socket.emit("device.action", {Device_ID: deviceID, Action_ID: actionID});
    });
    
    //Make a PATCH request to update a device
    function patchDevice(deviceID, patchObj, callback) {
        if (typeof callback !== "function") {
            return;
        }
        
        var url = "devices/" + deviceID;
        //send request
        $.ajax({
            url : url,
            data : JSON.stringify(patchObj),
            method : 'PATCH',
            contentType : 'application/json',
            success: callback,
            error: function(req, status, err) {
                
                console.log(status);
            }
        });
    }
    
    //the user is renaming the device
    $(".pref-form").on("keyup", ".rename-input", function(e) {
        if (e.keyCode == 13) {
            var deviceID = $(this).data("device-id");
            var deviceName = $(this).val();
            
            if(!deviceName) {
                alert("Device Name must not be empty");
                return;
            }
            var deviceObj = {
                Device_Name: deviceName
            };
            
            patchDevice(deviceID, deviceObj, function() {
                $("#" + deviceID +"-title").text(deviceObj.Device_Name); 
            });
            e.stopPropagation();
        }
    });
    
    //The user initiated choosing an icon for a device, show all icons
    $(".choose-icon").click( function(e) {
        var deviceID = $(this).attr("data-device-id");
        $("#all-icons").empty();
        $.get("icons/", function(data) {
            for(var i = 0; i < data.length; i++) {
                var html = '<li><a class="select-icon" href="#" data-icon-id="' + data[i].Icon_ID + '">' +
                                '<img src=' + data[i].Icon_URL + ' />' + 
                            '</li>'
                $("#all-icons").append(html)
                    //assign the device_id to #all-icons for selection purposes
                    .attr("data-device-id", deviceID);
            }
        });
    });
    
    //The user is selecting which icon to replace the old icon with
    $("#all-icons").on("click", ".select-icon", function() {
        var deviceID = $("#all-icons").attr("data-device-id") ;
        var iconID = $(this).data("icon-id");
        var src = $(this).children("img").attr("src");

        //the selected device patch details
        var obj = {
            Icon_ID: iconID    
        };
        
        //save to DB
        patchDevice(deviceID, obj, function(data) {
            $("#icon-" + deviceID).attr("src", src);
            //close the icons popup
            $(".close-reveal-modal").click();
        });
    });
    
    //Get current device icon when expanding device info section.
    $(".device-info").click(function(e) {
        var iconID = $(this).attr("data-icon-id");
        var deviceID = $(this).attr("data-device-id");
        $.get("icons/" + iconID, function(data) {
            $("#icon-" + deviceID).attr("src", data.Icon_URL);
        });
    });
    
    //Toggle twice, for demo purposes
    $(".device-info").click();
    $(".device-info").click();
});