$(document).ready(function() {
    
    var NUM_TIMESTEPS = 12;
    var NUM_HOURS = 24;
    var TIME_STEP_SIZE = NUM_HOURS / NUM_TIMESTEPS;
    
    function convertToTimeStep(militaryTime) {
        console.log("in conversion ", militaryTime);
        if(militaryTime == 0) {
            return 0;
        }
        
        var time  = Math.round(militaryTime / TIME_STEP_SIZE);
        return time;
    }
    
    function addHoverEffects() {
        $(".trash-rule").hide();
        $(document).on("mouseover", ".search-result", function() {
            $(this).find(".trash-rule").show();
        });
        
        $(document).on("mouseleave", ".search-result", function () {
            $(this).find(".trash-rule").hide();
        });
    }
    
    function saveRuleToDom(rule) {
        
        var ruleHtml = 'Set the <strong>' + rule.Sensor_Property_Name + '</strong> in the '
            + '<strong>' + rule.Location_Name + '</strong> to be '
            + '<strong>' + rule.Rule_Relation + '</strong> '
            + '<strong>' + rule.Goal_State + '</strong> <strong>'
            + rule.Time_Prefix + '</strong> AT timestep ' +
            +'<strong>' + rule.Time_Text + '</strong>';
        
        var $ele = $("<div />", {
            class: "well search-result",
        });
        
        var $row = $("<div />", {
            class: "row"
        });
        
        var $trashLink = $("<a />", {
            class: "trash-rule",
        }).data("rule-id", rule.Rule_ID);
        
        var $trashIcon = $("<i />", {
            class: "fa fa-trash fa-lg",
            style: "float: right"
        });
        
        var $center = $("<center />");
        var $p = $("<p />").append(ruleHtml);
        
        $trashLink.append($trashIcon).hide();
        $row.append($trashLink);
        $center.append($p);
        $row.append($center);
        $ele.append($row);
        
        $("#rule-list").append($ele);
    }
    
    function resetLocationsDropdown() {
        var html = "<option selected disabled>Locations:</option>";
        
        //get all the sensor properties
        $.get("/locations", function(data) {
            for(var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].Location_ID 
                        + '">' + data[i].Location_Name 
                        + '</option>';
            }
            
            //populate dropdown list
            $(".locations-dropdown").empty();
            $(".locations-dropdown").append(html);
            $(".locations-dropdown").show();
        });
    }
    
    //Reset the modal to it's original state so as to not impede further 
    //rule additions
    function resetAddRuleModal() {
        resetLocationsDropdown();
        $(".sensor-property-dropdown").hide();
        $(".relation-dropdown").hide();
        $(".relation-dropdown").val("-1")
        $(".locations-dropdown").hide();
        $(".goal-state-input").hide();
        $(".goal-state-input").val("");
        $(".time-prefix-dropdown").hide();
        $(".time").hide();
        $(".time").val("");
        $("#save-rule-btn").hide();
        $(".start-time").hide();
        $(".start-time").val("");
        $(".end-time").hide();
        $(".end-time").val("");
        
        $("#rule-modal-alert").hide();
        $(".start-time").hide();
        $(".end-time").hide();
    }
    
    //Make a POST request to insert a new rule
    function postRule(data, callback) {
        if (typeof callback !== "function") {
            return;
        }
        $.ajax({
            type: "POST",
            url: "/rules",
            data: JSON.stringify(data),
            contentType : 'application/json',
            success: callback
        });
    };
    
    //Make a DELETE request to delete a rule
    function deleteRule(id, callback) {
        if (typeof callback !== "function") {
            return;
        }
        $.ajax({
            type: "DELETE",
            url: "/rules/" + id,
            contentType : 'application/json',
            success: callback
        });
    };
    
    //User has clicked on add rule button
    $(".set-rules").click(function() {
        $(".rule-type-dropdown").val("-1");
        resetAddRuleModal();
    });
    
    
    // //The user is adding a rule and selecting a rule type
    // $(".rule-type-dropdown").change(function() {
    //     resetAddRuleModal();
        
    // });
    
    //A location has been selected, get affected sensor properties
    $(".locations-dropdown").change(function() {
        var locationID = $(this).find(":selected").val();
        var html = "<option selected disabled>Sensor Properties:</option>";
        $.get("/sensorpropsbyloc/" + locationID, function(data) {
            //For every sensor property add it as a dropdown option
            for(var i = 0; i < data.length; i++) {
                html += '<option value="' + data[i].Sensor_Property_ID 
                        + '">' + data[i].Sensor_Property_Name 
                        + '</option>';
            }
            
            //populate dropdown list
            $(".sensor-property-dropdown").empty();
            $(".sensor-property-dropdown").append(html);
            $(".sensor-property-dropdown").show();
        });
    });
    
    //A Affected sensor property has been selected
    $(".sensor-property-dropdown").change(function() {
        $(".relation-dropdown").show();
    });
    
    //A relation has been selected
    $(".relation-dropdown").change(function() {
        $(".goal-state-input").show();
    });
    
    //A goal state has been inputted
    $("input.goal-state-input").keyup(function() {
        // $(".time-prefix-dropdown").prepend('');
        
        var ruleType = $(".rule-type-dropdown").find(":selected").val();
        
        //Passive
        if(ruleType == "0") {
            //show submit button
            $("#save-rule-btn").show();
        } else {
            $(".time-prefix-dropdown").show();
        }
        
    });
    
    //A Time prefix has been selected
    $(".time-prefix-dropdown").change(function() {
        var timePrefix = $(this).find(":selected").val();
        
        switch(timePrefix) {
            case "0":
                $(".start-time").hide();
                $(".end-time").hide();
                $(".time").show();
                break;
            case "1":
                $(".start-time").hide();
                $(".end-time").hide();
                $(".time").show();
                break;
            case "2":
                $(".start-time").hide();
                $(".end-time").hide();
                $(".time").show();
                break;
            case "3":
                $(".time").hide();
                $(".start-time").show();
                $(".end-time").show();
                break;
            default:
                break;
        }
    });
    
    //The start time field is being typed in
    $(".start-time").keyup(function() {
        $("#save-rule-btn").show();
    });
    
    //The end time field is being typed in 
    $(".end-time").keyup(function() {
        $("#save-rule-btn").show();
    });
    
    //The time field is being typed in
    $(".time").keyup(function() {
        $("#save-rule-btn").show();
    });
    
    //The user is attempting to add a new rule
    $("#save-rule-btn").click(function() {
        //rule Type e.g. passive or active
        var ruleType = 1;
        
        //The location 
        var locationID = $(".locations-dropdown").find(":selected").val();
         var locName = $(".locations-dropdown").find(":selected").text();
        
        //The affected sensor property
        var sensorPropID = $(".sensor-property-dropdown").find(":selected").val();
        var spName = $(".sensor-property-dropdown").find(":selected").text();
        
        var relationType = $(".relation-dropdown").find(":selected").val();
        var ruleRel = $(".relation-dropdown").find(":selected").text();
        
        var goalState = $(".goal-state-input").val();
        var timePrefix = $(".time-prefix-dropdown").find(":selected").val();
        var timePrefixName = $(".time-prefix-dropdown").find(":selected").text()
        
        var ruleObj = {};
        
        if(ruleType) {
            ruleObj.Rule_Type = parseInt(ruleType, 10);
        }
        
        if(locationID) {
            ruleObj.Location_ID = parseInt(locationID, 10);
        }
        
        if(sensorPropID) {
            ruleObj.Sensor_Prop_ID = parseInt(sensorPropID, 10);
        }
        
        if(relationType) {
            ruleObj.Rule_Relation = parseInt(relationType, 10);
        }
        
        if(goalState) {
            ruleObj.Goal_State = parseInt(goalState, 10);
        }
        
        if(timePrefix) {
            var startTime = parseInt($(".start-time").val(), 10);
            var endTime = parseInt($(".end-time").val(), 10);
            var time = parseInt($(".time").val(), 10);
            console.log("retrieved from select ", time);
            
            startTime = convertToTimeStep(startTime);
            endTime = convertToTimeStep(endTime);
            time = convertToTimeStep(time);
            
            console.log("after converstion ", time);
            
            timePrefix = parseInt(timePrefix, 10);
            var timeText = '';
            
            switch(timePrefix) {
                case 0:
                        ruleObj.Time_Value = time;
                        ruleObj.Time_Modifier = 0;
                        timeText = time;
                    break;
                case 1:
                        ruleObj.Time_Value = time;
                        ruleObj.Time_Modifier = 0;
                        timeText = time;

                    break;
                case 2:
                    ruleObj.Time_Value = time;
                    ruleObj.Time_Modifier = 0;
                    timeText = time;
                    break;
                case 3:

                    ruleObj.Time_Value = startTime;
                    
                    ruleObj.Time_Modifier = endTime - startTime;
                    timeText = startTime + ' - ' + endTime;
                    
                    break;
                default:
                    break;
            }
            
                
            ruleObj.Time_Prefix = timePrefix;
            
            postRule(ruleObj, function(data) {
                //success
                $("#rule-modal-alert").show();
                
                ruleObj.Rule_ID = data.Rule_ID;
                ruleObj.Sensor_Property_Name = spName;
                ruleObj.Location_Name = locName;
                ruleObj.Rule_Relation = ruleRel;
                ruleObj.Time_Prefix = timePrefixName;
                ruleObj.Time_Text = timeText;
                saveRuleToDom(ruleObj);
                
                setTimeout(function() {
                    resetAddRuleModal();
                }, 1500);
            });
                
            
            
        }
    });
    
    
    $("body").on("click", ".trash-rule", function() {
         var ruleID = $(this).data('rule-id');
         var $self = $(this)
         
         deleteRule(ruleID, function(data) {
            $self.parent().parent().remove(); 
         });
    });
    
    
    function init() {
        addHoverEffects();
    }
    
    init();
})