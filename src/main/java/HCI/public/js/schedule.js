$(document).ready(function() {
	var NUM_TIMESTEPS = 12;
    var NUM_HOURS = 24;
    var TIME_STEP_SIZE = NUM_HOURS / NUM_TIMESTEPS;

	function convertToTimeStep(time) {
	    var militaryTime = Math.round(time * TIME_STEP_SIZE);
	    return militaryTime;
	}

	socket.emit('device.getschedule');

	socket.on('device.schedule', function(data) {

		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();

		var schedule = JSON.parse(data);
		// console.log(schedule.Schedule);

		var devices = {},
		actions = [];

		var deviceReqs = [];

		schedule.Schedule.map(function(devSchedule, index) {

			//get all the devices
			var deviceID = devSchedule.Device_ID;
			devices[deviceID] = {};
			devices[deviceID].actions = devSchedule.Actions;
			deviceReqs.push($.get("/devices/" + deviceID, function(device) {
				devices[device.Device_ID].device = device;
			}));
		});

		var requests = [];

		$.when.apply(undefined, deviceReqs).then(function(results) {
			var requests = [];
			$.map(devices, function(v, i) {
				//get all the actions
				v.actions.map(function(action, index) {
					var TimeStep = action.Timestep;
					requests.push($.get("/deviceactions/" + action.Action_ID, function(data) {
						data.TimeStep = TimeStep;
						devices[i].actions[index] = data;
					}));
				});
			});

			$.when.apply(undefined, requests).then(function(results) {

				var date = new Date();
				var d = date.getDate();
				var m = date.getMonth();
				var y = date.getFullYear();

				console.log(devices);

				var resources = [], events = [];
				$.map(devices, function(v, i) {
					resources.push({
						id: v.device.Device_ID,
						name: v.device.Device_Name
					});

					var lookingForOff = false;
					var startTime = null;
					var endTime = null;

					v.actions.map(function(action) {
						var sTime = parseInt(action.TimeStep, 10);
						var eTime = sTime + 1;
						if(action.Action_Name === "On") {
							lookingForOff = true;
							startTime = convertToTimeStep(sTime);
						} else if(action.Action_Name === "Off" && lookingForOff)  {
							lookingForOff = false;
							endTime = convertToTimeStep(sTime);
							console.log(startTime);
							console.log(endTime);
							events.push({
								title: "ON",
								start: new Date(y, m, d, startTime),
								end: new Date(y, m, d, endTime),
								resources: v.device.Device_ID
							});
						}
						// else {
						// 	events.push({
						// 		title: action.Action_Name,
						// 		start: new Date(y, m, d, sTime, eTime),
						// 		resources: v.device.Device_ID
						// 	});
						// }

					});
				});

				$('#calendar').fullCalendar({
					header: {
						left: 'prev,next today',
						center: 'title',
						right: 'month,agendaWeek,resourceDay'
					},
					timeFormat: '',
					defaultView: 'resourceDay',
					editable: true,
					droppable: true,
					resources: resources,
					events: events
				});

			});
		});
	});
});
