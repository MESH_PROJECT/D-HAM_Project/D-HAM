$(document).ready(function() {
    
    function initTrashIcons() {
        $(".trash-location").hide()
        
        $(document).on("mouseover", ".location-list-item", function() {
            $(this).find(".trash-location").show();
        });
        
        $(document).on("mouseleave", ".location-list-item", function () {
            $(this).find(".trash-location").hide();
        });
    }
    
    $(document).on("click", ".trash-location", function() {
        var locationID = $(this).data("location-id");
        
        //first callback is success case, second is fail case
        deleteLocation(locationID, function() {
            location.reload();
        }, function(res) {
            $.notify(res.responseText, 
                {
                    globalPosition: "top left",
                    autoHide: true,
                    autoHideDelay: 3000
                }
            );
        });
    });
    
    //the user wants to see all the devices at a location
    $(".show-devices").click(function(e) {
        var locationID = $(this).data("location-id");
        $(this).hide(); //hide this link
        $(this).next().show();  //show the hide devices link
        
        //get all the devices for this location
        $.get('/getdevicesbylocation/' + locationID, function(data) {
            
            //Empty any previous device data
            $("#" + locationID + "-device-list").empty();
            
            $.each(data, function(i, val) {
                var html = "<li>" + val.Device_Name + "</li>"
                $("#" + locationID + "-device-list").append(html);
            });
            
            $("#" + locationID + "-device-list").show();
        });
    });
    
    //The user wants to hide the devices in a particular location
    $(".hide-devices").click(function() {
        var locationID = $(this).data("location-id");
        
        $("#" + locationID + "-device-list").hide(); //hide the device list
        
        $(this).prev().show();    //show link to show options
        $(this).hide();
    });
    
    //The user wants to see the rules for a particular location
    $(".show-rules").click(function(e) {
        var locationID = $(this).data("location-id");
        $(this).hide(); //hide this link
        $(this).next().show();  //show the hide rules link
        $.get("/getrulesbylocation/" + locationID, function(data) {
            
            //Empty any previous data
            $("#" + locationID + "-rule-list").empty();
            
            $.each(data, function(i, val) {
                //console.log(val);
                var ruleType, ruleRelation, timePrefix, time;
                
                
                if(val.Rule_Type == 0) {//passive rule
                    ruleType = "Passive";
                } 
                //Active rule
                else {
                    ruleType = "Active";
                }
                
                //Get the rule relation type
                switch(val.Rule_Relation) {
                    case 0:
                        ruleRelation = '>';
                        break;
                    case 1:
                        ruleRelation = '<';
                        break;
                    case 2:
                        ruleRelation = '&#61;';
                        break;
                    case 3:
                        ruleRelation = '&le;';
                        break;
                    case 4:
                        ruleRelation = '&ge;';
                        break;
                    default:
                        break;
                }
                
                //Time Prefix type
                switch(val.Time_Prefix) {
                    case 0:
                        timePrefix = 'BEFORE';
                        time = val.Time_Value
                        break;
                    case 1:
                        timePrefix = 'AFTER';
                        time = val.Time_Value
                        break;
                    case 2:
                        timePrefix = 'AT';
                        time = val.Time_Value
                        break;
                    case 3:
                        timePrefix = 'WITHIN';
                        var endTime = (parseInt(val.Time_Value, 10) + parseInt(val.Time_Modifier));
                        time = val.Time_Value + ' - ' + endTime;
                        break;
                    default:
                        break;
                }
                
                var html =  '<tr>' +
                                '<td>' +
                                    ruleType + 
                                '</td>' +
                                '<td>' +
                                    val.Sensor_Property_Name +
                                '</td>' +
                                '<td>' +
                                    ruleRelation +
                                '</td>' +
                                '<td>' +
                                    val.Goal_State +
                                '</td>' +
                                '<td>' +
                                    timePrefix +
                                '</td>' +
                                '<td>' +
                                    time +
                                '</td>' +
                            '</tr>';
                //Populate the table
                $("#" + locationID + "-rule-list").append(html);
                $("#" + locationID + "-rule-table").show(); //show the table
            });
        });
    });
    
    //The user wants to hide the rules for a particular location
    $(".hide-rules").click(function() {
        var locationID = $(this).data("location-id");
        
        $("#" + locationID + "-rule-table").hide(); //hide the rule table
        
        $(this).prev().show();    //show link to show options
        $(this).hide();
    });
    
    //Rest the location Modal to the starting state
    function resetLocationModal() {
        $(".location-name").val("");    //empty location name
        $(".location-type").hide();     //hide location type dropdown
        $("#save-location-btn").hide(); //hide the save button
        
        //close any prior success message
        $("#location-modal-alert").hide();
    };
    
    //Make a POST request to insert a new location
    function postLocation(data, callback) {
        if (typeof callback !== "function") {
            return;
        }
        $.ajax({
            type: "POST",
            url: "/locations",
            data: JSON.stringify(data),
            contentType : 'application/json',
            success: callback
        });
    };
    
    //Make a DELETE request to delete a location
    function deleteLocation(locationID, callback, errorCallback) {
        if(typeof callback !== "function") {
            return;
        }
        
        $.ajax({
            type: "DELETE",
            url: "/locations/" + locationID,
            contentType: 'application/json',
            error: errorCallback,
            success: callback
        })
    }
    
    //The user has launched the add location modal
    $(".add-location").click(function() {
        resetLocationModal() ;
    });
    
    //The user is typing in a location name
    $(".location-name").keyup(function() {
        $.get("/locationtype", function(data) {
            $(".location-type").empty();
            var html = '<option selected disabled value="">Location Type</option>';
            $.each(data, function(i, val) {
                html += '<option class="location-type-option" ' +
                        'value ="' + val.Location_Type_ID + '">' +
                            val.Location_Type_Name +
                        '</option>';
            });
            $(".location-type").append(html);
            $(".location-type").show();  
        });
    });
    
    //The user has selected a location type
    $(".location-type").change(function(){
        //show the save button
        $("#save-location-btn").show();
    });
    
    //The user wants to save a new location
    $("#save-location-btn").click(function() {
        
        //get the name of the location
        var locationName = $(".location-name").val();
        //get the location type id
        var locationType = $(".location-type").find("option:selected").val();
        
        var locationObj = {
            Location_Name: locationName,
            Location_Type_ID: locationType
        };
        
        //save the location to the data
        postLocation(locationObj, function(data) {
            $("#location-modal-alert").show();
        });
    });
    
    function init() {
        initTrashIcons();
    }
    
    init();
});