const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new device
module.exports.insert = (req, res) => {
    var deviceObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(deviceObj);
    for (var j=0; j < keys.length; j++) {
        deviceObj['$' + keys[j]] = deviceObj[keys[j]];
        delete deviceObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Table " +
            "(Device_Type_ID, Device_Name, " +
            "Icon_ID, " +
            "Location_ID, Class_Name_ID) VALUES ($Device_Type_ID, " +
            "$Device_Name, " +
            "$Icon_ID, $Location_ID, $Class_Name_ID)";

    db.run(sql, deviceObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all devices from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Table", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get a device by Device_ID
module.exports.getById = (req, res) => {
    var id = req.params.Device_ID;
    db.get("SELECT * FROM Device_Table WHERE Device_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a device by Device_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Device_ID;
    var body = _.pick(req.body, [
        'Device_Type_ID',
        'Device_Name',
        'Icon_ID',
        'Location_ID',
        'Class_Name_ID'
    ]);

    var sql = "UPDATE Device_Table SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Device_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a device by Device_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Device_ID;
    db.run("DELETE FROM Device_Table WHERE Device_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
