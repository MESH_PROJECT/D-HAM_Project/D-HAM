const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new locationv type
module.exports.insert = (req, res) => {
    var locationTypeObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(locationTypeObj);
    for (var j=0; j < keys.length; j++) {
        locationTypeObj['$' + keys[j]] = locationTypeObj[keys[j]];
        delete locationTypeObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Location_Types " +
            "(Location_Type_Name) " +
            "VALUES ($Location_Type_Name)";

    db.run(sql, locationTypeObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all location types from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Location_Types", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row);
        res.end();
    });
};

//get a location type by Location_Type_ID
module.exports.getById = (req, res) => {
    var id = req.params.Location_Type_ID;
    db.get("SELECT * FROM Location_Types WHERE Location_Type_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a location type by Location_Type_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Location_Type_ID;
    var body = _.pick(req.body, [
        'Location_Type_Name',
        'Location_Type_ID'
    ]);

    var sql = "UPDATE Location_Types SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ', ' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Location_Type_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a location type by id
module.exports.deleteById = (req, res) => {
    var id = req.params.Location_Type_ID;
    db.run("DELETE FROM Location_Types WHERE Location_Type_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
