const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new location
module.exports.insert = (req, res) => {
    var locationObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(locationObj);
    for (var j=0; j < keys.length; j++) {
        locationObj['$' + keys[j]] = locationObj[keys[j]];
        delete locationObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Locations " +
            "(Location_Name, Location_Type_ID) " +
            "VALUES ($Location_Name, $Location_Type_ID)";

    db.run(sql, locationObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all locations from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Locations", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row);
        res.end();
    });
};

//get a location by Location_ID
module.exports.getById = (req, res) => {
    var id = req.params.Location_ID;
    db.get("SELECT * FROM Locations WHERE Location_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a location by Location_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Location_ID;
    var body = _.pick(req.body, [
        'Location_Name',
        'Location_Type_ID'
    ]);

    var sql = "UPDATE Locations SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ', ' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Location_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

const getDevicesByLocationID = (req, res, callback) => {
    if(typeof callback !== "function") {
        console.log("check callback in getDevicesByLocationID");
        return;
    }
    
    var id = req.params.Location_ID;
    
    var sql = 'SELECT * FROM Device_Table as A ' +
                'JOIN Locations as B ON ' +
                'A.Location_ID = B.Location_ID ' +
                'WHERE B.Location_ID = ' + id;
    db.all(sql, callback);
}

//delete a location by location_ID
module.exports.deleteById = (req, res) => {
    getDevicesByLocationID(req, res, function(err, devices) {
        if(err) {
            console.log(err);
            return res.status(400).send();
        }
        
        if(devices.length !== 0) {
            return res.status(422).send("Delete Failed! Location contains devices");
        }
        
        var id = req.params.Location_ID;
        db.run("DELETE FROM Locations WHERE Location_ID = ?", id, function(err, row){
            if(err) {
                console.log(error);
                return res.status(400).send();
            }
            return res.status(200).send(row);
        });
    })

};

module.exports.devicesByLocationID = function (req, res) {
    getDevicesByLocationID(req, res, function(err, devices){
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }
        return res.status(200).send(devices);
    });
}
