const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new device
module.exports.insert = (req, res) => {
    var dTypeObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(dTypeObj);
    for (var j=0; j < keys.length; j++) {
        dTypeObj['$' + keys[j]] = dTypeObj[keys[j]];
        delete dTypeObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Type " +
            "(Device_Type_ID, Device_Type_Name) " +
            "VALUES ($Device_Type_ID, $Device_Type_Name)";
            
    db.run(sql, dTypeObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all devices from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Type", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get a device by Device_ID
module.exports.getById = (req, res) => {
    var id = req.params.Device_Type_ID;
    db.get("SELECT * FROM Device_Type WHERE Device_Type_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a device by Device_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Device_Type_ID;
    var body = _.pick(req.body, [
        'Device_Type_Name'
    ]);

    var sql = "UPDATE Device_Type SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Device_Type_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a device by Device_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Device_Type_ID;
    db.run("DELETE FROM Device_Type WHERE Device_Type_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
