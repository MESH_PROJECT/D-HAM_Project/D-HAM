const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new rule
module.exports.insert = (req, res) => {
    var ruleObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(ruleObj);
    for (var j=0; j < keys.length; j++) {
        ruleObj['$' + keys[j]] = ruleObj[keys[j]];
        delete ruleObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Rule_Table " +
            "(Rule_Type, Location_ID, Sensor_Prop_ID, Rule_Relation, " +
            "Time_Prefix, Goal_State, Time_Value, Time_Modifier) " +
            "VALUES ($Rule_Type, $Location_ID, $Sensor_Prop_ID, " +
            "$Rule_Relation, $Time_Prefix, $Goal_State, $Time_Value, " +
            "$Time_Modifier)";

    db.run(sql, ruleObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send({Rule_ID: this.lastID})
        res.end();
    });
};

//get all rules from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Rule_Table", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get a rule by Rule_ID
module.exports.getById = (req, res) => {
    var id = req.params.Rule_ID;
    db.get("SELECT * FROM Rule_Table WHERE Rule_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a rule by Rule_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Rule_ID;
    var body = _.pick(req.body, [
        'Rule_Type',
        'Location_ID',
        'Sensor_Prop_ID',
        'Rule_Relation',
        'Time_Prefix',
        'Goal_State',
        'Time_Value',
        'Time_Modifier'
    ]);

    var sql = "UPDATE Rule_Table SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Rule_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a rule by Rule_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Rule_ID;
    db.run("DELETE FROM Rule_Table WHERE Rule_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
