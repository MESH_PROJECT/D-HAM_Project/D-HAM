const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new item
module.exports.insert = (req, res) => {
    var dActionsSPRObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(dActionsSPRObj);
    for (var j=0; j < keys.length; j++) {
        dActionsSPRObj['$' + keys[j]] = dActionsSPRObj[keys[j]];
        delete dActionsSPRObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Action_SP_Relation " + 
            "VALUES ($Index, $Action_ID, " +
            "$Sensor_Property_ID, $Delta)";
    db.run(sql, dActionsSPRObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all items from table
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Action_SP_Relation", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get by id
module.exports.getById = (req, res) => {
    var id = req.params.Index;
    db.get("SELECT * FROM Device_Action_SP_Relation WHERE \"Index\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update by id
module.exports.updateById = (req, res) => {
    var id = req.params.Index;
    var body = _.pick(req.body, [
        'Action_ID',
        'Sensor_Property_ID',
        'Delta'
    ]);

    var sql = "UPDATE Device_Action_SP_Relation SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE \"Index\" = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a device by Index
module.exports.deleteById = (req, res) => {
    var id = req.params.Index;
    db.run("DELETE FROM Device_Action_SP_Relation WHERE \"Index\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
