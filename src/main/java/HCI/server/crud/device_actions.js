const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new device
module.exports.insert = (req, res) => {
    var dActionsObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(dActionsObj);
    for (var j=0; j < keys.length; j++) {
        dActionsObj['$' + keys[j]] = dActionsObj[keys[j]];
        delete dActionsObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Actions " +
            "(Action_Name, " +
            "Action_KWH) VALUES (" +
            "$Action_Name, $Action_KWH)";

    db.run(sql, dActionsObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all devices from the data
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Actions", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get a device by Device_ID
module.exports.getById = (req, res) => {
    var id = req.params.Action_ID;
    db.get("SELECT * FROM Device_Actions WHERE Action_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update a device by Device_ID
module.exports.updateById = (req, res) => {
    var id = req.params.Action_ID;
    var body = _.pick(req.body, [
        'Action_Name',
        'Action_KWH'
    ]);

    var sql = "UPDATE Device_Actions SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE Action_ID = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a device by Device_ID
module.exports.deleteById = (req, res) => {
    var id = req.params.Action_ID;
    db.run("DELETE FROM Device_Actions WHERE Action_ID = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
