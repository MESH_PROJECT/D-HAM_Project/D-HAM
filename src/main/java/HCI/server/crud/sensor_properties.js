const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new item
module.exports.insert = (req, res) => {
    var sensorPropObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(sensorPropObj);
    for (var j=0; j < keys.length; j++) {
        sensorPropObj['$' + keys[j]] = sensorPropObj[keys[j]];
        delete sensorPropObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Sensor_Properties " + 
            "VALUES ($Sensor_Property_ID, " +
            "$Sensor_Property_Name)";
    db.run(sql, sensorPropObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

//get all items from table
module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Sensor_Properties", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

//get by id
module.exports.getById = (req, res) => {
    var id = req.params.Sensor_Property_ID;
    db.get("SELECT * FROM Sensor_Properties WHERE \"Sensor_Property_ID\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

//update by id
module.exports.updateById = (req, res) => {
    var id = req.params.Sensor_Property_ID;
    var body = _.pick(req.body, [
        'Sensor_Property_Name'
    ]);

    var sql = "UPDATE Sensor_Properties SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE \"Sensor_Property_ID\" = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

//delete a Sensor_Property by id
module.exports.deleteById = (req, res) => {
    var id = req.params.Index;
    db.run("DELETE FROM Sensor_Properties WHERE \"Sensor_Property_ID\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
