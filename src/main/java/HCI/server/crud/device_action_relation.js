const {db} = require('./../db/sqlite3');
const _ = require('lodash');

//Insert a new device
module.exports.insert = (req, res) => {
    var dActionsRelObj = req.body;

    //Change the keys to contain the '$' prefix
    var keys = Object.keys(dActionsRelObj);
    for (var j=0; j < keys.length; j++) {
        dActionsRelObj['$' + keys[j]] = dActionsRelObj[keys[j]];
        delete dActionsRelObj[keys[j]];
    }

    //The sql query to be run
    var sql = "INSERT INTO Device_Action_Relation " +
            "(Action_ID, " +
            "Device_ID) VALUES (" +
            "$Action_ID, $Device_ID)";

    db.run(sql, dActionsRelObj, function(err, row) {
        if (err){
            console.log(err);
            return res.status(400);
        }

        res.status(200).send(row)
        res.end();
    });
};

module.exports.getAll = (req, res) => {
    db.all("SELECT * FROM Device_Action_Relation", function(err, row){
        if (err){
            console.log(err);
            return res.status(400);
        }
        res.status(200).send(row)
        res.end();
    });
};

module.exports.getById = (req, res) => {
    var id = req.params.Index;
    db.get("SELECT * FROM Device_Action_Relation WHERE \"Index\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};

module.exports.updateById = (req, res) => {
    var id = req.params.Index;
    var body = _.pick(req.body, [
        'Device_ID',
        'Action_ID'    
    ]);

    var sql = "UPDATE Device_Action_Relation SET ";

    var keys = Object.keys(body);
    for (var j=0; j < keys.length; j++) {
        sql += keys[j] + " = $" + keys[j] + ", ";
        body['$' + keys[j]] = body[keys[j]];
        delete body[keys[j]];
    }

    //Remove final ',' and add a space
    sql = sql.substring(0, sql.length - 2) + " ";

    sql += "WHERE \"Index\" = " + id;

    // As an object with named parameters.
    db.run(sql, body, (err, rows) => {
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(rows);
    });
};

module.exports.deleteById = (req, res) => {
    var id = req.params.Index;
    db.run("DELETE FROM Device_Action_Relation WHERE \"Index\" = ?", id, function(err, row){
        if(err) {
            console.log(error);
            return res.status(404);
        }
        res.status(200).send(row);
        res.end();
    });
};
