const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const bodyParser = require('body-parser');
const hbs = require('hbs');
const {db} = require('./db/sqlite3');

var device = require('./crud/device_table');
var deviceType = require('./crud/device_type');
var deviceActions = require('./crud/device_actions');
var deviceActionRelation = require('./crud/device_action_relation');
var deviceActionSPRelation = require('./crud/device_action_sp_relation');
var icon = require('./crud/icon');
var rule = require('./crud/rule_table');
var locations = require('./crud/locations');
var locationTypes = require('./crud/location_type');
var sensorProperties = require('./crud/sensor_properties');

const publicPath = path.join(__dirname, '../public/');
const port = process.env.PORT || 3000;

//Start an Express application
var app = express();
var server = http.createServer(app);
var io = socketIO(server);

var NUM_TIMESTEPS = 12;
var NUM_HOURS = 24;
var TIME_STEP_SIZE = NUM_HOURS / NUM_TIMESTEPS;


io.on('connection', (socket) => {
    console.log('New user connected');
    socket.on('disconnect', () => {
        console.log("User disconnected");
    });

    //The user has turned on a device
    socket.on('device.action', (data) => {
        console.log(data);
        socket.broadcast.emit('device.action', data);
    });

    socket.on('device.getschedule', (data) => {
        socket.broadcast.emit('device.getschedule', data);
    });

    socket.on('device.schedule', (data) => {
        socket.broadcast.emit('device.schedule', data);
    });
});
hbs.registerPartials(path.join(__dirname, '../views/partials/'));

hbs.registerHelper('numToRelationSymbol', function(number) {
    var ruleRelation;
    //Get the rule relation type
    switch(number) {
        case 0:
            ruleRelation = '>';
            break;
        case 1:
            ruleRelation = '<';
            break;
        case 2:
            ruleRelation = '&#61;';
            break;
        case 3:
            ruleRelation = '&le;';
            break;
        case 4:
            ruleRelation = '&ge;';
            break;
        default:
            break;
    }

    return new hbs.SafeString(ruleRelation);
});

hbs.registerHelper('timePrefixToName', function(timePrefix) {
    var name;
    //Time Prefix type
    switch(timePrefix) {
        case 0:
            name = 'BEFORE';
            break;
        case 1:
            name = 'AFTER';
            break;
        case 2:
            name = 'AT';
            break;
        case 3:
            name = 'WITHIN';
            break;
        default:
            break;
    }

    return new hbs.SafeString(name);
});

function convertToUserTime(time) {
    var militaryTime = Math.round(time * TIME_STEP_SIZE);

    if(militaryTime >= 12) {
        if(militaryTime % 12 === 0) {
            return 12 + 'PM';
        }
        return militaryTime % 12 + 'PM';
    } else {
        if(militaryTime % 12 === 0) {
            return 12 + 'AM';
        }
        return militaryTime % 12 + 'AM';
    }
}

hbs.registerHelper('timeValToTimeInterval', function() {
    if(this.Time_Modifier) {
        return convertToUserTime(this.Time_Value) + ' - '
            + convertToUserTime(this.Time_Value + this.Time_Modifier);
    }

    return convertToUserTime(this.Time_Value);
});

app.set('view engine', 'hbs');
app.use(express.static(publicPath));
app.use(bodyParser.json());

app.get('/schedule', (req, res) => {
    res.render('schedule.hbs');
});

app.get('/rule', (req, res) => {
    var sql = "SELECT * FROM Rule_Table " +
                "JOIN Sensor_Properties ON Sensor_Property_ID = " +
                "Sensor_Prop_ID " +
                "JOIN Locations ON Rule_Table.Location_ID = " +
                "Locations.Location_ID " +
                "WHERE Rule_Type = 1";
    db.all(sql, function(err, rules){
        if (err){
            console.log(err);
            return;
        }

        res.render('rule.hbs', {
            ruleList: rules
        });
    });
});

app.get('/device', (req, res) => {
    var sql = "SELECT * FROM Device_Table as A"
        + " JOIN (SELECT * FROM Comm_Pro_Device_Relation as B"
        + " JOIN Communication_Protocols ON Comm_Pro_ID = Protocol_ID) as C"
        + " ON C.Device_ID = A.Device_ID"
        + " JOIN Locations ON A.Location_ID = Locations.Location_ID";
    db.all(sql, function(err, devices){
        if (err){
            console.log(err);
            return;
        }

        res.render('device.hbs', {
            deviceList: devices
        });
    });
});

app.get('/location', (req, res) => {
    var sql = "SELECT * FROM Locations";
    db.all(sql, function(err, locations){
        if (err){
            console.log(err);
            return;
        }

        res.render('location.hbs', {
            locations: locations
        });
    });
});

app.get('/', (req, res) => {
    res.render('home.hbs');
});

app.get('/location', (req, res) => {
    var sql = "SELECT * FROM Device_Table as A"
        + " JOIN (SELECT * FROM Comm_Pro_Device_Relation as B"
        + " JOIN Communication_Protocols ON Comm_Pro_ID = Protocol_ID) as C"
        + " ON C.Device_ID = A.Device_ID"
        + " JOIN Locations ON A.Location_ID = Locations.Location_ID";
    db.all(sql, function(err, devices){
        if (err){
            console.log(err);
            return;
        }

        res.render('device.hbs', {
            deviceList: devices
        });
    });
});

//Device_Table REST
app.post('/devices', device.insert);
app.get('/devices', device.getAll);
app.get('/devices/:Device_ID', device.getById);
app.patch('/devices/:Device_ID', device.updateById);
app.delete('/devices/:Device_ID', device.deleteById);

//Device_Type REST
app.post('/devicetypes', deviceType.insert);
app.get('/devicetypes', deviceType.getAll);
app.get('/devicetypes/:Device_Type_ID', deviceType.getById);
app.patch('/devicetypes/:Device_Type_ID', deviceType.updateById);
app.delete('/devicetypes/:Device_Type_ID', deviceType.deleteById);

//Device_Actions REST
app.post('/deviceactions', deviceActions.insert);
app.get('/deviceactions', deviceActions.getAll);
app.get('/deviceactions/:Action_ID', deviceActions.getById);
app.patch('/deviceactions/:Action_ID', deviceActions.updateById);
app.delete('/deviceactions/:Action_ID', deviceActions.deleteById);

//Device_Action_Relation REST
app.post('/deviceactionrel', deviceActionRelation.insert);
app.get('/deviceactionrel', deviceActionRelation.getAll);
app.get('/deviceactionrel/:Index', deviceActionRelation.getById);
app.patch('/deviceactionrel/:Index', deviceActionRelation.updateById);
app.delete('/deviceactionrel/:Index', deviceActionRelation.deleteById);

//Device_Action_SP_Relation REST
app.post('/dasprel', deviceActionSPRelation.insert);
app.get('/dasprel', deviceActionSPRelation.getAll);
app.get('/dasprel/:Index', deviceActionSPRelation.getById);
app.patch('/dasprel/:Index', deviceActionSPRelation.updateById);
app.delete('/dasprel/:Index', deviceActionSPRelation.deleteById);

//Device_Icon REST
app.post('/icons', icon.insert);
app.get('/icons', icon.getAll);
app.get('/icons/:Icon_ID', icon.getById);
app.patch('/icons/:Icon_ID', icon.updateById);
app.delete('/icons/:Icon_ID', icon.deleteById);

//Rule_Table REST
app.post('/rules', rule.insert);
app.get('/rules', rule.getAll);
app.get('/rules/:Rule_ID', rule.getById);
app.patch('/rules/:Rule_ID', rule.updateById);
app.delete('/rules/:Rule_ID', rule.deleteById);

//Locations REST
app.post('/locations', locations.insert);
app.get('/locations', locations.getAll);
app.get('/locations/:Location_ID', locations.getById);
app.patch('/locations/:Location_ID', locations.updateById);
app.delete('/locations/:Location_ID', locations.deleteById);

//Location_Type REST
app.post('/locationtype', locationTypes.insert);
app.get('/locationtype', locationTypes.getAll);
app.get('/locationtype/:Location_Type_ID', locationTypes.getById);
app.patch('/locationtype/:Location_Type_ID', locationTypes.updateById);
app.delete('/locationtype/:Location_Type_ID', locationTypes.deleteById);

//Sensor_Properties REST
app.post('/sensorprops', sensorProperties.insert);
app.get('/sensorprops', sensorProperties.getAll);
app.get('/sensorprops/:Sensor_Property_ID', sensorProperties.getById);
app.patch('/sensorprops/:Sensor_Property_ID', sensorProperties.updateById);
app.delete('/sensorprops/:Sensor_Property_ID', sensorProperties.deleteById);

//get all actions available to a device
app.get('/getdevactions/:Device_ID', (req, res) => {
    var id = req.params.Device_ID;

    var sql = 'SELECT * FROM Device_Action_Relation as DAR'
            + ' JOIN Device_Actions ON DAR.Action_ID = Device_Actions.Action_ID'
            + ' WHERE DAR.Device_ID = ' + id;

    db.all(sql, function(err, actions){
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        res.status(200).send(actions);

    });
});

//Get all the sensor properties that this device can affect
app.get('/devaffectedsensorprops/:Device_ID', (req, res) => {
    var id = req.params.Device_ID;
    var sql = 'SELECT * FROM Sensor_Properties'
        + ' JOIN (SELECT * FROM Device_Action_SP_Relation as A'
        + ' JOIN (SELECT * FROM Device_Action_Relation as DAR'
        + ' JOIN Device_Actions ON DAR.Action_ID = Device_Actions.Action_ID'
        + ' WHERE DAR.Device_ID = ' + id + ') as B'
        + ' WHERE A.Action_ID = B.Action_ID) as C'
        + ' ON Sensor_Properties.Sensor_Property_ID = C.Sensor_Property_ID';

    db.all(sql, function(err, sensorProps){
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        console.log(sensorProps);
        res.status(200).send(sensorProps);
    });
});

app.get('/sensorpropsbyloc/:Location_ID', (req, res) => {
    var id = req.params.Location_ID;

    var sql = 'SELECT * FROM Locations as A, Device_Table as B '
            + 'JOIN Device_Action_Relation as C ON '
            + 'C.Device_ID = B.Device_ID '
            + 'JOIN Device_Actions as D ON D.Action_ID = C.Action_ID '
            + 'JOIN Device_Action_SP_Relation as E ON '
            + 'E.Action_ID = D.Action_ID '
            + 'JOIN Sensor_Properties as F ON F.Sensor_Property_ID = '
            + 'E.Sensor_Property_ID '
            + 'WHERE A.Location_ID = ' + id + ' AND A.Location_ID = '
            + 'B.Location_ID GROUP BY F.Sensor_Property_ID';
    db.all(sql, function(err, sensorProps){
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }

        console.log(sensorProps);
        res.status(200).send(sensorProps);
    });
});

app.get('/getrulesbylocation/:Location_ID', (req, res) => {
    var id = req.params.Location_ID;

    var sql = 'SELECT * FROM Rule_Table as A ' +
                'JOIN Locations as B ON ' +
                'B.Location_ID = A.Location_ID ' +
                'AND A.Location_ID = ' + id + ' ' +
                'JOIN Sensor_Properties as C ON ' +
                'C.Sensor_Property_ID = A.Sensor_Prop_ID';
    db.all(sql, function(err, rules){
        if(err) {
            console.log(err);
            return res.status(400).send({});
        }
        res.status(200).send(rules);
    });
});

app.get('/getdevicesbylocation/:Location_ID', locations.devicesByLocationID);

server.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

module.exports = {server}
