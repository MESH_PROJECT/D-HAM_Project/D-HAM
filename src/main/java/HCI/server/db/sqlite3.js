var sqlite3 = require('sqlite3').verbose();
const yargs = require('yargs');

const argv = yargs.argv;    //set the program arguments

if(!argv.db) {  //Make sure DB variable is set
    return console.log("You must provide a path to the DB file!");
}

//Connect to Sqlite data
var db = new sqlite3.Database(argv.db);

module.exports = {db};
