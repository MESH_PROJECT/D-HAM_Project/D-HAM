package factories;

import actions.DeviceAction;
import sensorproperties.SensorProperty;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;


/**
 *
 * @author Jeremiah Smith
 * @creationDate 12/15/2016
 * @lastModifiedDate 12/16/2016
 * @inputs HashMap<String, String> with three values
 * Action_ID, ID
 * Action_Name, Name
 * Action_KWH - This is the KW used for one hour of continued use.
 * @outputs DeviceAction Object
 *
 */
public class ActionFactory {

    public static DeviceAction constructDeviceAction(HashMap<String, String> action, ArrayList<SensorProperty> affectedProps) {
        Database database = new Database();
        HashMap<SensorProperty, Double> actionDelta = new HashMap<>();
        int actionID = Integer.parseInt(action.get("Action_ID"));
        String actionName = action.get("Action_Name");
        double actionKWH = Double.parseDouble(action.get("Action_KWH"));
        for (HashMap<String, String> hashMap : database.selectActionSPRelations(actionID)) {
            System.out.println(hashMap.toString());
            actionDelta.put(SensorPropFactory.constructSensorProperty(database.selectSensorProperty(Integer.parseInt(hashMap.get("Sensor_Property_ID")))), Double.parseDouble(hashMap.get("Delta")));
        }
        return new DeviceAction(actionID, actionName, actionKWH, affectedProps, actionDelta);
    }
}
