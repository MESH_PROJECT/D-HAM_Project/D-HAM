package factories;

import sensorproperties.SensorProperty;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah on 12/14/2016.
 */
public class SensorPropertyListFactory {

    public static ArrayList<SensorProperty> constructSensorPropList(ArrayList<HashMap<String, String>> sensorPropertiesHM) {
        ArrayList<SensorProperty> sensorPropList = new ArrayList<>();

        for(HashMap<String, String> sensorHM: sensorPropertiesHM) {
            sensorPropList.add(SensorPropFactory.constructSensorProperty(sensorHM));
        }

        return sensorPropList;
    }
}
