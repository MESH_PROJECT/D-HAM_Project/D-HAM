package factories;

import locations.Location;
import locations.LocationType;
import utilities.Database;

import java.util.HashMap;

/**
 *
 * @author Jeremiah Smith
 * @creationDate 12/14/2016
 * @lastModifiedDate 12/16/2016
 * @inputs HashMap<String, String> with three values
 * Location_ID, ID
 * Location_Name, Name
 * Location_Type_ID - This is the KW used for one hour of continued use.
 * @outputs Location Object
 *
 */
public class LocationFactory {



    public static Location constructLocation(HashMap<String, String> locationMap) {

        return new Location(Integer.parseInt(locationMap.get("Location_ID")), locationMap.get("Location_Name"), LocationType.assignLocationType(Integer.parseInt(locationMap.get("Location_Type_ID"))));
    }
}
