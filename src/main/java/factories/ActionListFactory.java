package factories;

import actions.DeviceAction;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jeremiah on 12/14/2016.
 */
public class ActionListFactory {
    private static Database database = new Database();
    public static ArrayList<DeviceAction> constructActionList(ArrayList<HashMap<String, String>> actions) {
        ArrayList<DeviceAction> actionList = new ArrayList<>();

        for(HashMap<String, String> hm : actions) {
            actionList.add(ActionFactory.constructDeviceAction(hm, SensorPropertyListFactory.constructSensorPropList(database.selectAffectedSensorProperties(Integer.parseInt(hm.get("Action_ID"))))));
        }


        return actionList;
    }
}
