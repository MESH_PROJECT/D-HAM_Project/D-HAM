package factories;

import actions.ActionList;
import actions.DeviceAction;
import communication.CommunicationProtocol;
import devices.*;
import locations.Location;
import sensorproperties.SensorProperty;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;

//todo modify comment header to include class information
/**
 * Created by jeremiah on 12/13/16.
 */
public class DeviceFactory {

    private static Database database = new Database();

    public static Device constructDevice(HashMap<String, String> dbDeviceValues, ArrayList<DeviceAction> actions) {

        String deviceName = dbDeviceValues.get("Device_Name");
        int deviceID = Integer.parseInt(dbDeviceValues.get("Device_ID"));
        int iconID = Integer.parseInt(dbDeviceValues.get("Icon_ID"));
        Location location = LocationFactory.constructLocation(database.selectLocation(Integer.parseInt(dbDeviceValues.get("Location_ID"))));
        CommunicationProtocol communicationProtocol = CommProFactory.constructCommunicationProtocol(database.selectCommProtocol(deviceID));

        ClassType classType = ClassType.assignClassType(Integer.parseInt(dbDeviceValues.get("Class_Name_ID")));
        switch(classType) {
            case BINARY_SWITCH: {
                return new BinarySwitch(deviceName, deviceID, iconID, communicationProtocol, location, new ActionList(actions));

            }
            case THERMOSTAT:{
                //todo implement thermostat creation

                break;
            }
            case LIGHT_ACTUATOR: {
                //todo implement light actuator
                break;
            }
            case LUMINOSITY_SENSOR: {
                //todo construct sensor property correctly
                return new LuminositySensor(deviceID, 0, iconID, deviceName, location, new SensorProperty(1, "Luminosity"), communicationProtocol);
            }
            default:


        }
        //todo look at new return type inst
        return null;
    }

}
