package factories;


import actions.DeviceAction;
import devices.Actuator;
import devices.Sensor;
import models.PredictiveModel;

import java.util.ArrayList;

/**
 * Created by Jeremiah on 1/18/2017.
 */
public class PredictiveModelFactory {



    public static PredictiveModel constructPredictiveModel(Sensor sensor, ArrayList<Actuator> actuators) {
        ArrayList<Actuator> modelActuators = new ArrayList<>();

        for (Actuator a : actuators) {
            if (a.getLocation().equals(sensor.getLocation())) {
                for (DeviceAction da : a.getActionList().getActions()) {
                    if(da.getAffectedSensorProperties().containsSensorProperty(sensor.getAffectedProperty()))
                        modelActuators.add(a);
                }
            }
        }

        if(modelActuators.size() > 0) {
            return new PredictiveModel(sensor, modelActuators);
        }
        return null;
    }
}
