package factories;

import devices.Device;
import devices.DeviceList;
import utilities.Database;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jeremiah on 12/14/16.
 */
public class DeviceListFactory {
    private static Database database = new Database();

    public static DeviceList constructDeviceList() {
        ArrayList<Device> devices = new ArrayList<>();
        //request data information
        ArrayList<HashMap<String, String>> databaseDevices = database.selectDevices();

        //build devices
        for(HashMap<String, String> hm : databaseDevices) {
            //add to device list
            devices.add(DeviceFactory.constructDevice(hm, ActionListFactory.constructActionList(database.selectDeviceActions(Integer.parseInt(hm.get("Device_ID"))))));

        }

        return new DeviceList(devices);
    }
}
