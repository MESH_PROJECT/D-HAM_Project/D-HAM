package factories;

import locations.Location;
import models.PredictiveModel;
import rules.*;
import sensorproperties.SensorProperty;
import utilities.Database;

import java.util.HashMap;

/**
 * Created by jeremiah on 12/16/16.
 */
//todo finish schedule rule factory
public class RuleFactory {

    public static SchedulingRule constructScheduleRule(HashMap<String, String> ruleHM) {
        Database database = new Database();
        int goalState = Integer.parseInt(ruleHM.get("Goal_State"));
        Location location = LocationFactory.constructLocation(database.selectLocation(Integer.parseInt(ruleHM.get("Location_ID"))));
        int ruleID = Integer.parseInt(ruleHM.get("Rule_ID"));
        RuleRelation ruleRelation = RuleRelation.assignRuleRelation(Integer.parseInt(ruleHM.get("Rule_Relation")));
        RuleType ruleType = RuleType.assignRuleType(Integer.parseInt(ruleHM.get("Rule_Type")));
        SensorProperty affectedSensorProp = SensorPropFactory.constructSensorProperty(database.selectSensorProperty(Integer.parseInt(ruleHM.get("Sensor_Prop_ID"))));
        int startTime = Integer.parseInt(ruleHM.get("Time_Value"));
        int endTime = startTime + Integer.parseInt(ruleHM.get("Time_Modifier"));
        RuleTimePrefix timePrefix = RuleTimePrefix.assignRuleTimePrefix(Integer.parseInt(ruleHM.get("Time_Prefix")));
        PredictiveModel predictiveModel = PredictiveModel.valueOf(location, affectedSensorProp);
        RuleTimePredicate ruleTimePredicate;

        if ( ruleType == RuleType.PASSIVE) {
            ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
        }
        else {
            switch(timePrefix) {
                case BEFORE: {
                    ruleTimePredicate = RuleTimePredicate.DISJUNCTION;
                }
                case AFTER: {
                    ruleTimePredicate = RuleTimePredicate.DISJUNCTION;
                }
                case WITHIN: {
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                }
                case AT: {
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                }
                default: {
                    ruleTimePredicate = RuleTimePredicate.CONJUNCTION;
                }
            }
        }

        return new SchedulingRule(ruleID, affectedSensorProp, location, ruleRelation, timePrefix, ruleType, goalState, startTime, endTime, ruleTimePredicate, predictiveModel);
    }
}
