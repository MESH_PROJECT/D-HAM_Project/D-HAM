package communication;

/**
 * Created by jeremiah on 12/2/16.
 */
public class CommunicationProtocol {

    private int protocolID;
    private String protocolName;
    private String protocolCommID;

    public CommunicationProtocol(int protocolID, String protocolName, String protocolCommID) {
        this.protocolID = protocolID;
        this.protocolName = protocolName;
        this.protocolCommID = protocolCommID;
    }

    public int getProtocolID() {
        return protocolID;
    }

    public void setProtocolID(int protocolID) {
        this.protocolID = protocolID;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    @Override
    public String toString() {
        return protocolName;
    }
}
