package devices;

/**
 * Created by Jerem on 12/15/2016.
 */
public enum ClassType {

    //todo add more class types
    BINARY_SWITCH(0),
    THERMOSTAT(1),
    LIGHT_ACTUATOR(2),
    LUMINOSITY_SENSOR(3);

    private int value;

    ClassType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ClassType assignClassType(int classType) {
        for (ClassType ct : ClassType.values()) {
            if(ct.getValue() == classType) {
                return ct;
            }
        }

        return null;
    }
}
