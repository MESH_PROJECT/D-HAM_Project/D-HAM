package devices;

import communication.CommunicationProtocol;
import locations.Location;
import java.io.PrintWriter;

/**
 * Created by alex on 12/13/16.
 * Modified by Jeremiah 01/25/2017
 */
public interface Device {

    void setID(int id);

    int getIconID();

    void setCommProtocol(CommunicationProtocol pro);

    String getName();

    int getID();

    CommunicationProtocol getCommProtocol();

    void printDevice(PrintWriter pWriter);

    Location getLocation();

    int getCpID();

    void setCpID(int cpID);

    int hashCode();

}
