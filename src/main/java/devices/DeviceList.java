package devices;

import java.util.ArrayList;

/**
 * Created by jeremiah on 11/11/16.
 */
public class DeviceList {

   private ArrayList<Device> devices;


    /**
     * Default constructor initializes actuator and sensor list to new empty lists
     */
    public DeviceList() {
        devices = new ArrayList<>();
    }

    /**
     * Constructor to create a new device list with the provided Actuator and Sensor ArrayLists
     * @param devices ArrayList of devices that implement the actuator interface
     */
    public DeviceList(ArrayList<Device> devices) {
        this.devices = devices;
    }

    public ArrayList<Device> getDevices() {
        return devices;
    }

    public void setDevices(ArrayList<Device> devices) {
        this.devices = devices;
    }

    //public methods

    public Device getDevice(int deviceID){
        for( Device d : devices) {
            if (d.getID() == deviceID) {
                return d;
            }
        }
        return null;
    }

    //todo check if needed
    public int getNumberOfActuators() {
        int actuatorCount = 0;

        for( Device d : devices) {
            if (d instanceof Actuator) {
                actuatorCount++;
            }
        }

        return actuatorCount;
    }

    //todo check if needed
    public int getNumberOfSensors() {
        int sensorCount = 0;

        for(Device d : devices)
            if(d instanceof Sensor) {
                sensorCount++;
            }

        return sensorCount;
    }
    /**
     * Method that will print the devices in the list to the console
     */
    public void printDevices() {
        this.printSeparator(100, "*");
        for(Device d: devices) {
            System.out.println(d.toString());
        }
        this.printSeparator(100, "*");

    }

    //private methods
    private void printSeparator(int numOfChars, String sepChar) {
        System.out.println();
        for(int i = 0; i < numOfChars; i++) {
            System.out.print(sepChar);
        }
        System.out.println();
    }


}
