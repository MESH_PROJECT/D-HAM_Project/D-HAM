package devices;


import sensorproperties.SensorProperty;

/**
 * Created by rread on 10/14/16.
 * Modified by Jeremiah 01/25/2017
 */
public interface Sensor extends Device {

    SensorProperty getAffectedProperty();
    int getState();
    void setState(int state);
    String toString();
}