package devices;

import actions.ActionList;
import actions.DeviceAction;
import communication.CommunicationProtocol;
import locations.Location;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by jeremiah on 11/11/16.
 */
public class BinarySwitch implements Actuator {

    private String name;
    private int deviceID;
    private String commID;
    private CommunicationProtocol commProtocol;
    private Location currentLocation;
    private ActionList actions;
    private int cpID;
    private int iconID;

    private static String token = "f9d7ea51-3cb2-4a65-b3bd-c044f6acd023";
    private static String address = "https://graph-na02-useast1.api.smartthings.com/api/smartapps/installations/975bf520-ed9f-4a0f-ae53-3406165190b1";

    public BinarySwitch(){

    }

    public BinarySwitch(String name, int deviceID, int iconID, CommunicationProtocol commProtocol, Location currentLocation, ActionList actions) {
        this.name = name;
        this.deviceID = deviceID;
        this.iconID = iconID;
        this.commProtocol = commProtocol;
        this.currentLocation = currentLocation;
        this.actions = actions;
    }


    @Override
    public Location getLocation() {
        return currentLocation;
    }


    @Override
    public CommunicationProtocol getCommProtocol(){ return commProtocol;}

    @Override
    public void printDevice(PrintWriter pWriter) {
        pWriter.write("ID" + this.deviceID);
    }

    @Override
    public ActionList getActionList() {
        return actions;
    }

    @Override
    public boolean activate(DeviceAction action) {
        for(DeviceAction da: actions.getActions()) {
            if(da.equals(action)) {
                //System.out.println("Activating Action " + da.toString());
                ProcessBuilder p = new ProcessBuilder("curl" ,
                        "-H" ,
                        "Authorization: Bearer "+token ,
                        "-X" ,
                        "PUT" ,
                        address+"/switches/"+ da.getActionName().toLowerCase());

                //System.out.println("Command: "+p.command().toString());

                //System.out.println(p.directory());

                //System.out.println(p.environment());
                try {
                    Process st = p.start();

                } catch (IOException e) {
                    e.printStackTrace();
                }

                return true;
            }
        }

        return false;
    }

    @Override
    public int getIconID() {
        return iconID;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getID() {
        return deviceID ;
    }


    @Override
    public boolean activate(int actionID) {
        return true;
    }

    @Override
    public boolean deActivate(int actionID) {
        return true;
    }


    @Override
    public void setID(int id){ deviceID = id;}

    @Override
    public void setCommProtocol(CommunicationProtocol pro){ commProtocol = pro; }

    @Override
    public String toString() {
        return "BinarySwitch{" +
                "name='" + name + '\'' +
                ", deviceID=" + deviceID +
                ", iconID=" + iconID +
                ", commID='" + commID + '\'' +
                ", commProtocol=" + commProtocol +
                ", currentLocation=" + currentLocation +
                ", actions=" + actions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinarySwitch)) return false;

        BinarySwitch that = (BinarySwitch) o;

        if (deviceID != that.deviceID) return false;
        if (cpID != that.cpID) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (commID != null ? !commID.equals(that.commID) : that.commID != null) return false;
        if (commProtocol != null ? !commProtocol.equals(that.commProtocol) : that.commProtocol != null) return false;
        if (currentLocation != null ? !currentLocation.equals(that.currentLocation) : that.currentLocation != null)
            return false;
        return actions != null ? actions.equals(that.actions) : that.actions == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + deviceID;
        result = 31 * result + (commID != null ? commID.hashCode() : 0);
        result = 31 * result + (commProtocol != null ? commProtocol.hashCode() : 0);
        result = 31 * result + (currentLocation != null ? currentLocation.hashCode() : 0);
        result = 31 * result + (actions != null ? actions.hashCode() : 0);
        result = 31 * result + cpID;
        return result;
    }
}
