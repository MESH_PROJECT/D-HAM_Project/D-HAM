package devices;


import java.util.ArrayList;

import actions.ActionList;
import actions.DeviceAction;
import communication.CommunicationProtocol;
import locations.Location;
import sensorproperties.SensorProperty;

import java.io.PrintWriter;

/**
 * Created by ryanread on 11/18/16.
 *
 * This class will be designed to implement z-wave to control a light bulb
 * currently using linearlinc bulbz.
 *
 */
public class LightActuator implements Actuator{

    private String name;
    private int device_id;
    private ArrayList<SensorProperty> senseProperties;
    private CommunicationProtocol communicationProtocol;
    private String communication_id;
    private ActionList actions;
    private Location location;
    private int cpID;
    private int iconID;

    public LightActuator(){

    }

    public LightActuator(int id, String n, String commID, CommunicationProtocol protocol){
        device_id = id;
        name = n;
        communicationProtocol = protocol;
        communication_id=commID;
    }

    @Override
    public void setID(int id){
        this.device_id = id;
    }


    @Override
    public int getIconID() {
        return iconID;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro) {

    }

    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public int getID(){
        return this.device_id;
    }


    @Override
    public CommunicationProtocol getCommProtocol(){
        return this.communicationProtocol;
    }

    @Override
    public void printDevice(PrintWriter pwriter){
        String x;

        x = "Name: " + this.name +"\n"
                + "ID: "+ this.device_id + "\n"+
                "CommProtocol: " + this.communicationProtocol + "\n"+
                "CommID: " + this.communication_id + "\n";

        pwriter.print(x);
        pwriter.flush();

    }

    @Override
    public Location getLocation(){
        return this.location;
    }

    @Override
    public boolean activate(int actionID){
        //on full
        //if action ID is full
        if(actionID == 1){

        }

        //on dim;

        //off full

        return false;
    }




    @Override
    public ActionList getActionList(){
        return actions;
    }

    @Override
    public boolean activate(DeviceAction action) {
        return false;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public boolean deActivate(int actionID){
        //off full

        return true;
    }
}
