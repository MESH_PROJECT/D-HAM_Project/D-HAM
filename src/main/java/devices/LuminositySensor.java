package devices;

import actions.ActionList;
import actions.DeviceAction;
import communication.CommunicationProtocol;
import locations.Location;
import sensorproperties.SensorPropList;
import sensorproperties.SensorProperty;

import java.io.PrintWriter;

/**
 * Created by Jeremiah on 1/24/2017.
 */
public class LuminositySensor implements Sensor {

    private int deviceID;
    private int iconID;
    private int cpID;
    private int currentState;
    private String deviceName;
    private Location location;
    private SensorProperty affectedSensorProperty;
    private CommunicationProtocol communicationProtocol;

    public LuminositySensor(int deviceID, int currentState, int iconID, String deviceName, Location location, SensorProperty affectedSensorProperty, CommunicationProtocol communicationProtocol) {
        this.deviceID = deviceID;
        this.iconID = iconID;
        this.currentState = currentState;
        this.deviceName = deviceName;
        this.location = location;
        this.affectedSensorProperty = affectedSensorProperty;
        this.communicationProtocol = communicationProtocol;

    }

    @Override
    public void setState(int state) {
        this.currentState = state;
    }

    @Override
    public SensorProperty getAffectedProperty() {
        return affectedSensorProperty;
    }


    @Override
    public int getState() {
        return currentState;
    }


    @Override
    public void setID(int id) {
        this.deviceID = id;
    }

    @Override
    public int getIconID() {
        return 0;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro) {
        this.communicationProtocol = pro;
    }

    @Override
    public String getName() {
        return deviceName;
    }

    @Override
    public int getID() {
        return deviceID;
    }

    @Override
    public CommunicationProtocol getCommProtocol() {
        return communicationProtocol;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {

    }

    @Override
    public Location getLocation() {
        return location;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LuminositySensor)) return false;

        LuminositySensor that = (LuminositySensor) o;

        if (deviceID != that.deviceID) return false;
        if (cpID != that.cpID) return false;
        if (currentState != that.currentState) return false;
        if (deviceName != null ? !deviceName.equals(that.deviceName) : that.deviceName != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        if (affectedSensorProperty != null ? !affectedSensorProperty.equals(that.affectedSensorProperty) : that.affectedSensorProperty != null)
            return false;
        return communicationProtocol != null ? communicationProtocol.equals(that.communicationProtocol) : that.communicationProtocol == null;
    }

    @Override
    public int hashCode() {
        int result = deviceID;
        result = 31 * result + cpID;
        result = 31 * result + (deviceName != null ? deviceName.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (affectedSensorProperty != null ? affectedSensorProperty.hashCode() : 0);
        result = 31 * result + (communicationProtocol != null ? communicationProtocol.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LuminositySensor{" +
                "deviceID=" + deviceID +
                ", iconID=" + iconID +
                ", cpID=" + cpID +
                ", currentState=" + currentState +
                ", deviceName='" + deviceName + '\'' +
                ", location=" + location +
                ", affectedSensorProperty=" + affectedSensorProperty +
                ", communicationProtocol=" + communicationProtocol +
                '}';
    }
}
