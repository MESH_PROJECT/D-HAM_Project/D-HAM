package devices;
import actions.ActionList;
import actions.DeviceAction;
import communication.CommunicationProtocol;
import locations.Location;

import java.io.*;
import java.net.*;

/**
 * Created by rread on 10/14/16.
 */

public class RoombaActuator implements Actuator {

    private Socket socket; //need to implement consistent communication protocols for all devices
    //private Socket data;
    private PrintWriter out;
    private int cpID;
    BufferedReader in;
    int roomba_id;
    String comm_id;
    CommunicationProtocol comm_proto;
    String name;

    public RoombaActuator(){
        //No argd constructor
    }

    public RoombaActuator(int port, int dataport){
        this.roomba_id=0;//TODO: double check roomba id
        this.comm_id="192.168.1.146";
        this.comm_proto = new CommunicationProtocol(4, "WI-FI","192.168.1.146");
        this.name = "roomba"; //TODO: can we handle multiple roombas?

        int x =this.connect(port, dataport);
        if(x==1) {
            System.out.println("Roomba initialized");
        }
        else{
            System.out.println("Issues connecting roomba.");
        }
    }
    public int connect(int port, int dataPort){
        try {
            ServerSocket sock = new ServerSocket(port);
            //ServerSocket dat = new ServerSocket(dataPort);
            this.socket = sock.accept();
            //this.data = dat.accept();


            this.out = new PrintWriter(this.socket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            System.out.println("Socket Connected");

        } catch (IOException e) {
            return 0;
        }
        return 1;
    }

    @Override
    public ActionList getActionList() {
        return null;
    }

    @Override
    public boolean activate(DeviceAction action) {
        return false;
    }

    @Override
    public int getCpID() {
        return cpID;
    }

    @Override
    public void setCpID(int cpID) {
        this.cpID = cpID;
    }

    @Override
    public CommunicationProtocol getCommProtocol() {
        return this.comm_proto;
    }

    @Override
    public void printDevice(PrintWriter pWriter) {

    }

    @Override
    public int getIconID() {
        return 0;
    }

    @Override
    public void setCommProtocol(CommunicationProtocol pro) {

    }


    @Override
    public void setID(int id){
        this.roomba_id = id;
    }

    @Override
    public int getID(){
        return this.roomba_id;
    }

    @Override
    public Location getLocation() {
        return null;
    }

    @Override
    public boolean activate(int actionID){
        //need to incorporate different modes
        //clean
        try {
            if (actionID == 1) {
                //clean mode
                this.out.write("clean");
                this.out.flush();
                this.in.readLine();
            }
            //max
            else if (actionID == 2) {
                //max mode
                this.out.write("max");
                this.out.flush();
                this.in.readLine();
            }
            //seek_dock
            else if (actionID == 3) {
                this.out.write("seek_dock");
                this.out.flush();
                this.in.readLine(); //block until roomba reaches dock
            }
            else if (actionID == 4) {
                this.out.write("get_charge");
                this.out.flush();
                String x = this.in.readLine();
                int charge = Integer.parseInt(x);
                System.out.println("charge is " + charge);
                //charge in charge
            }
            else { //charge
                //charge or idle error?
                this.out.write("charge");
                this.out.flush();
                this.in.readLine(); //block until roomba signals

                //it has reached the dock
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean deActivate(int actionID) {
        this.out.print("seek_dock");
        return true; //need to update to ensure finished
    }


    @Override
    public String getName(){
        return "roomba";
    }

    //prompts roomba for state and returns current state
    public String getState() {
        String state;
        this.out.write("state");
        this.out.flush();

        //read in a STRING
        try {
            //reads in the state
            state = this.in.readLine();
            return state;
        } catch (IOException e) {
            System.out.println("Error reading from Roomba.");
            return "";
        }
    }

    //for testing.


    /*public static void main(String[] args){

        String s;

        RoombaActuator robo = new RoombaActuator(3141, 4444);

        robo.activate(1);//activate clean
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        s = robo.getState();
        System.out.println(s); //returned state should be clean


        robo.activate(2);// activate max
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        s = robo.getState();
        System.out.println(s);//printed state should be max

        robo.activate(3);//delay charge
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        s = robo.getState();
        System.out.println(s);//printed state should be delay charge

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }*/

}
