package devices;

import actions.ActionList;
import actions.DeviceAction;

/**
 * Created by jeremiah on 10/14/16.
 */
public interface Actuator extends Device{


    @Deprecated
    boolean activate(int actionID);

    @Deprecated
    boolean deActivate(int actionID);

    String toString();

    ActionList getActionList();

    boolean activate(DeviceAction action);



}
