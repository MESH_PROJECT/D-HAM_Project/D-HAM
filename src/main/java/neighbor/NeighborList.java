package neighbor;

import utilities.LoadProjectProperties;
import java.util.ArrayList;

/**
 * Created by jeremiah on 6/24/16.
 */
public class NeighborList {

    private final LoadProjectProperties LPP = LoadProjectProperties.getInstance();
    private final int TIMESTEPS;

    private ArrayList<Neighbor> neighbors;

    public NeighborList() {

        TIMESTEPS = LPP.getTimeSteps();
        neighbors = new ArrayList<>();
    }

    public NeighborList(ArrayList<Neighbor> neighbors) {
        this.neighbors = neighbors;
        TIMESTEPS = LPP.getTimeSteps();
    }

    //accessor methods
    public ArrayList<Neighbor> getNeighbors() { return neighbors;}

    public int getNeighborCount() { return neighbors.size(); }

    public double [] getTotalAggregatedPower() {

        double [] totalAggregatedPower = new double[TIMESTEPS];
        for (int i = 0; i < totalAggregatedPower.length; i++) {
            totalAggregatedPower[i] = 0.0f;
        }

        for (Neighbor n: neighbors) {
            double [] temp = n.getMgmInfoList().getSpecificEP(n.getCurrentCycle());

            if(temp != null) {
                for (int i = 0; i < temp.length; i++) {
                    totalAggregatedPower[i] += temp[i];
                }
            }

        }
        return totalAggregatedPower;
    }

    public void printNeighbors () {
        for (Neighbor n : neighbors) {
            n.printInfo();
        }
    }

    public boolean cycleCheck(int cycleNumberToCheck) {
        for (Neighbor n : neighbors) {
            if (n.getCurrentCycle() < cycleNumberToCheck) {
                return false;
            }
        }

        return true;
    }

    public boolean mgmInfoReceived(int cycleNumber) {

        for (Neighbor n : neighbors) {
            if(n.getMgmInfoList().hasReceivedMGMInfo(cycleNumber) == false) {
                return false;
            }
        }
        return true;
    }

    public void addNeighbor(Neighbor neighbor) {
        this.neighbors.add(neighbor);
    }


}
