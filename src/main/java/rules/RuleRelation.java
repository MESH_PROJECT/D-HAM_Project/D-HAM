package rules;

/**
 * Created by jeremiah on 12/2/16.
 */
public enum RuleRelation {

    EQUAL(0),
    NOT_EQUAL(1),
    LESS_THAN(2),
    GREATER_THAN(3),
    LESS_THAN_EQUAL(4),
    GREATER_THAN_EQUAL(5);

    private int value;

    RuleRelation(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RuleRelation assignRuleRelation(int ruleRelation) {
       for(RuleRelation rr : RuleRelation.values()) {
           if(rr.getValue() ==  ruleRelation) {
               return rr;
           }
       }

       return null;
    }

}
