package rules;

import models.PredictiveModel;
import sensorproperties.SensorProperty;
import locations.Location;

/**
 * Created by jeremiah on 12/2/16.
 */
public class SchedulingRule {

    private int ruleID; //id from the data
    private SensorProperty affectedProperty;
    private Location location;
    private RuleRelation ruleRelation;
    private RuleTimePrefix ruleTimePrefix;
    private RuleType ruleType;
    private int goalState;
    private int startTime;
    private int endTime;
    private RuleTimePredicate ruleTimePredicate;
    private PredictiveModel predictiveModel;

    public SchedulingRule(int ruleID, SensorProperty affectedProperty, Location location, RuleRelation ruleRelation, RuleTimePrefix ruleTimePrefix, RuleType ruleType, int goalState, int startTime, int endTime, RuleTimePredicate ruleTimePredicate, PredictiveModel predictiveModel) {
        this.ruleID = ruleID;
        this.affectedProperty = affectedProperty;
        this.location = location;
        this.ruleRelation = ruleRelation;
        this.ruleTimePrefix = ruleTimePrefix;
        this.ruleType = ruleType;
        this.goalState = goalState;
        this.startTime = startTime;
        this.endTime = endTime;
        this.ruleTimePredicate = ruleTimePredicate;
        this.predictiveModel = predictiveModel;
    }

    public int getRuleID() {
        return ruleID;
    }

    public void setRuleID(int ruleID) {
        this.ruleID = ruleID;
    }

    public SensorProperty getAffectedProperty() {
        return affectedProperty;
    }

    public void setAffectedProperty(SensorProperty affectedProperty) {
        this.affectedProperty = affectedProperty;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public RuleRelation getRuleRelation() {
        return ruleRelation;
    }

    public void setRuleRelation(RuleRelation ruleRelation) {
        this.ruleRelation = ruleRelation;
    }

    public RuleTimePrefix getRuleTimePrefix() {
        return ruleTimePrefix;
    }

    public void setRuleTimePrefix(RuleTimePrefix ruleTimePrefix) {
        this.ruleTimePrefix = ruleTimePrefix;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public int getGoalState() {
        return goalState;
    }

    public void setGoalState(int goalState) {
        this.goalState = goalState;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public RuleTimePredicate getRuleTimePredicate() {
        return ruleTimePredicate;
    }

    public void setRuleTimePredicate(RuleTimePredicate ruleTimePredicate) {
        this.ruleTimePredicate = ruleTimePredicate;
    }

    public PredictiveModel getPredictiveModel() {
        return predictiveModel;
    }

    public void setPredictiveModel(PredictiveModel predictiveModel) {
        this.predictiveModel = predictiveModel;
    }

    @Override
    public String toString() {
        return "SchedulingRule{" +
                "ruleID=" + ruleID +
                ", affectedProperty=" + affectedProperty +
                ", location=" + location +
                ", ruleRelation=" + ruleRelation +
                ", ruleTimePrefix=" + ruleTimePrefix +
                ", ruleType=" + ruleType +
                ", goalState=" + goalState +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
