package rules;

/**
 * Created by Jerem on 1/15/2017.
 */
public enum RuleTimePredicate {
    CONJUNCTION(0),
    DISJUNCTION(1);

    private int value;

    RuleTimePredicate(int value) {this.value = value;}

    public int getValue() {return value;}

    public static RuleTimePredicate assignRuleTimePredicate(int ruleTimePredicate) {

        for (RuleTimePredicate rtp : RuleTimePredicate.values()) {
            if (rtp.getValue() == ruleTimePredicate) {
                return rtp;
            }

        }
        return null;
    }
}
