package rules;

/**
 * Created by jeremiah on 12/2/16.
 */
public enum RuleType {


    PASSIVE(0),
    ACTIVE(1);

    private int value;

    RuleType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RuleType assignRuleType(int ruleType) {
        for(RuleType rt: RuleType.values()) {
            if( rt.getValue() == ruleType) {
                return rt;
            }
        }

        return null;
    }
}
