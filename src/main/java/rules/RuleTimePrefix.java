package rules;

/**
 * Created by jeremiah on 12/2/16.
 */
public enum RuleTimePrefix {

    BEFORE(0),
    AFTER(1),
    AT(2),
    WITHIN(3);

    private int value;

    RuleTimePrefix(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static RuleTimePrefix assignRuleTimePrefix(int ruleTimePrefix) {

        for(RuleTimePrefix rtp : RuleTimePrefix.values()) {
            if (rtp.getValue() == ruleTimePrefix) {
                return rtp;
            }
        }

        return null;
    }
}
