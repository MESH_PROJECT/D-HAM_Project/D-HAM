package solver;

import schedule.Schedule;

/**
 * Created by Jeremiah on 1/16/2017.
 */
public interface SolverInterface {

    Schedule getSchedule(double[] neighborPower);
    Schedule getFirstSchedule();
    Schedule getBaseLineSchedule(double[] neighborPower);
}
